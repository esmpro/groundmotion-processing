#!/usr/bin/env python3

import json
import numpy as np
from copy import deepcopy
from sys import argv as sys_argv
from sys import exit as sys_exit
import argparse
import os.path

from lib.format_utils import dict_fmt
from lib.asdf_utils import valid_true_false
from lib.asdf_utils import valid_asdf_r
from lib.asdf_file import ASDFFile
from lib.quality_check import WaveformQualityCheck
from lib.waveforms import WaveformCuttingAuto, WaveformCutting
from lib.utilities import NpEncoder, PS_auto
from lib.dictionary import JSON

class ArgumentParserError(Exception): pass
class CustomArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise ArgumentParserError(message)


def preprocessing(opts_str, cur=None):
    
    """
    Run preprocessing steps given the input data and a default configuration file.
    This method analyzes the unprocessed waveforms providing information 
    about the data quality and the settings for automatic processing.

    Args:
        input_file (str):
            path to the file with the unprocessed waveforms.
        configuration_file (str):
            path to the configuration file (or None). Default is default.json.
        copy-data (bool):
            Keep a copy of output json files (or None). Default is False.
        verbose (bool):
            prints summary and other details (or None). Default is True.

    Returns:
        two json files, the former with the results of the quality class, 
        the latter with processing workflow and settings
    """
    synopsis = 'Return processing parameters and quality check metadata.'
    usage = "example: %prog --input_file /path/to/data/example.h5"
    p = argparse.ArgumentParser(description=synopsis + '\n' + usage)

    p.add_argument("--input", "--input_file", action="store", dest="ifile", type=valid_asdf_r, help="ASDF volume containing the raw data to be analyzed")
    
    p.add_argument("--config", "--configuration_file", action="store", dest="iconfig",  default='input/defaults.json', help="configuration file in JSON format containing placeholders for metadata defined by preprocessing.py and default parameters.")

    p.add_argument("--copy", "--copy-data", action="store", dest="copy", type=str, choices=['True', 'False', 'Guess'], default="False", help="Keep a copy of json files with processing parameters and quality check metadata. 'Guess' will keep copies only for waveforms classified as quality 'A' (Best) or 'B' (Good). Default: False")

    p.add_argument("--v", "--verbose", action="store", dest="verb", type=valid_true_false, default='True', help="Verbose mode (prints summary and other details). Default: True")

    opts_str_lst = opts_str.replace("'", "").replace('=',' ').split(' ')
    
    try: opts = p.parse_args(opts_str_lst) 
    except ArgumentParserError as ap_err: return dict_fmt(exit_status=2, exit_message=str(ap_err))    
    
    errors = []
    warnings = {}
    classes = {}
    parameters = {}
 
    path = os.path.dirname(opts.ifile[0]) 
    if opts.verb: print ("path: ", path)
    
    filename_h5 = os.path.basename(opts.ifile[0])
    if opts.verb: print ("filename_h5: ", filename_h5)
    
    filename_json = opts.iconfig.split('/')[-1]
    path_json = os.path.basename(opts.iconfig[0])
    if opts.verb: print ("filename_json: ", filename_json)
    
    #Load the input JSON as dictionary and set filename.
    try:
        load_json = JSON(path, filename_json)
        dictn = load_json.loadJSON()
    except Exception as e:
        if opts.verb: print('load json', print(str(e)))
    #From filename create the ASDF file.
    try:
        asdf_file = ASDFFile(opts.ifile[0])
    except Exception as e:
        if opts.verb: print('create the ASDF file', print(str(e)))
    
    eve_depth = asdf_file.get_depth()
    
    origin_time = asdf_file.get_origin_time()
    
    epi_dist = asdf_file.get_epi_dist()
    
    magnitude = asdf_file.get_magnitude()
    
    # Extract dataset: Waveform + Spectra + Auxiliary Data.
    try:
        ds = asdf_file.get_dataset()
    except Exception as e:
        if opts.verb: print('get_dataset', str(e))
        errors.append(str(e))

    # Extract stream for a specific station and event.    
    try:
        wfs_stream = asdf_file.get_stream(asdf_file.get_station_ids()[0], asdf_file.get_event_ids()[0])
    except Exception as e:
        if opts.verb: print('get_list_of_streams', str(e))
        errors.append(str(e))

    # Check 3 components
    try:
        check_3comp = asdf_file.check_3c(wfs_stream)
        warnings = load_json.merge_two_dicts(warnings,check_3comp)
    except Exception as e:
        warnings = load_json.merge_two_dicts(warnings,{'the_number_of_traces_is_equal_to_three': None,'empty_component': None,'zero_component': None})
        if opts.verb: print('check 3 components', str(e))
        errors.append(str(e))
        
    # For WaveformCut apply trigger.
    try:
        wfs_trigger = WaveformCuttingAuto()
        trigger = wfs_trigger.trigger(wfs_stream[0])
    except Exception as e:
        if opts.verb: print('WaveformCuttingAuto', str(e))
        warnings = load_json.merge_two_dicts(warnings,{'trigger_detected': False})
        errors.append(str(e))
    
    try:
        if len(trigger>=0): warnings = load_json.merge_two_dicts(warnings,{'trigger_detected': True})
        else: warnings = load_json.merge_two_dicts(warnings,{'trigger_detected': False})
    except Exception as e:
        if opts.verb: print('Trigger', str(e))
        errors.append(str(e))

    # Find the target event.
    try:
        target = wfs_trigger.targetevent(wfs_stream, eve_depth,origin_time,epi_dist,trigger, cur=cur)
        warnings = load_json.merge_two_dicts(warnings,target[1])
    except Exception as e:
        if opts.verb: print('EventTarget', str(e))
        warnings = load_json.merge_two_dicts(warnings,{'multiple_events': None, 'unconsistent_picking': None})
        errors.append(str(e))

    # Extract tag acc_cv (raw data).
    try:
        stream = asdf_file.extract_info_tag(ds,asdf_file.get_station_ids()[0],'acc_cv')
    except Exception as e:
        if opts.verb: print('get_stream', str(e))
        errors.append(str(e))

    #Waveform Cut.
    # set starttime and endtime
    try:
        wfs_cut, message = wfs_trigger.cut(stream,epi_dist,target[0])
        parameters = load_json.merge_two_dicts(parameters,wfs_cut)
        warnings = load_json.merge_two_dicts(warnings,message)
    except Exception as e:
        if opts.verb: print('EventCut', str(e))
        parameters = load_json.merge_two_dicts(parameters, {'start_time': None, 'end_time': None})
        errors.append(str(e))

    # apply cut
    try:
        stream_orig = deepcopy(stream)
    except Exception as e:
        if opts.verb: print('deep copy straem', str(e))
        errors.append(str(e))

    try:
        streamcut = WaveformCutting(parameters['start_time'], parameters['end_time'])
        stream_cut = streamcut.apply(stream)
    except Exception as e:
        if opts.verb: print('ApplyCut', str(e))
        errors.append(str(e))
    ############################ Quality Index from SNR in Time Domain (TD) #############################

    try:
        # Picking p-s from Automatic Picking
        warning_msg={'picking_detected':True}
        st=stream_cut.copy()
        st.trim(starttime = wfs_cut['start_time'], endtime=wfs_cut['end_time'])
        st.detrend(type='demean').detrend(type='linear')
        st.filter('bandpass', freqmin=2.0, freqmax=8.0, corners=2,zerophase=True)
        
        p_pick, s_pick = PS_auto(st)
        if p_pick > s_pick or p_pick < 0:
            p_pick=None
            s_pick=None
            warning_msg={'picking_detected':False}
        warnings = load_json.merge_two_dicts(warnings,warning_msg)
    except:
        warning_msg={'picking_detected':False}
        warnings = load_json.merge_two_dicts(warnings,warning_msg)
        p_pick=None
        s_pick=None
    # define time windows for signal and noise with Automatic Picking
    try:
        windows = WaveformQualityCheck(None,None,None,None,wfs_cut['start_time'],wfs_cut['end_time'])
        sn_windows = windows.signal_noise_windows_TD(stream_cut, p_pick, s_pick)
        parameters = load_json.merge_two_dicts(parameters,sn_windows)

    except Exception as e:
        if opts.verb: print('SN windows', str(e))
        parameters = load_json.merge_two_dicts(parameters,{'signal_window':[None, None],'noise_window':[None, None],'ps_auto':{'P':None,'S':None}})
        errors.append(str(e))

    try:
        # provide streams for signal and noise
        sn_stream = windows.signal_noise_stream_TD(stream_cut,sn_windows['noise_window'],sn_windows['signal_window'])
    except Exception as e:
        if opts.verb: print('SN stream', str(e))
        errors.append(str(e))

    try:
        # compute Quality Index based on SNR in TD.
        quality_index = windows.signal_to_noise_TD(sn_stream['noise_window_stream'],sn_stream['signal_window_stream'])
        parameters = load_json.merge_two_dicts(parameters,quality_index)
    except Exception as e:
        if opts.verb: print('QI' ,str(e))
        parameters = load_json.merge_two_dicts(parameters,{'quality_index':{'average_value':None, 'component': {'U': None, 'V': None, 'W': None}},'rms':{'signal':{'component':{'U': None, 'V': None, 'W': None}},'noise':{'component':{'U': None, 'V': None, 'W': None}}}})
        errors.append(str(e))

    #################################### Additional Checks ################################################

    try:
        # check if PGA of one horizontal component is more than twice the other - ACC
        horizontal_pga_check = windows.horizontal_pgX_check(stream_cut, flag=0)
        warnings = load_json.merge_two_dicts(warnings,horizontal_pga_check)
    except Exception as e:
        if opts.verb: print('HORIZONTAL PGA CHECK', str(e))
        warnings = load_json.merge_two_dicts(warnings,{'unconsistent_horizontal_pga': None})
        errors.append(str(e))
        
    try:
        # check if PGA of the vertical component is more than twice the others -ACC
        vertical_pga_check = windows.vertical_pgX_check(stream_cut, flag=0)
        warnings = load_json.merge_two_dicts(warnings,vertical_pga_check)
    except Exception as e:
        if opts.verb: print('VERTICAL PGA CHECK', str(e))
        warnings = load_json.merge_two_dicts(warnings,{'unconsistent_vertical_pga': None})
        errors.append(str(e))

    try:
        # check if the RMS on signal for one horizontal component is at least twice the other -ACC
        amplitude_check = windows.horizontal_amplitude_check(quality_index['rms']['signal']['component']['U'],quality_index['rms']['signal']['component']['V'])
        warnings = load_json.merge_two_dicts(warnings,amplitude_check)
    except Exception as e:
        if opts.verb: print('HORIZONTAL AMPLITUDE CHECK', str(e))
        warnings = load_json.merge_two_dicts(warnings,{'unconsistent_horizontal_amplitude': None})
        errors.append(str(e))

    try:
        # check if the RMS on signal for the vertical component is at least twice the others -ACC
        amplitude_check = windows.vertical_amplitude_check(quality_index['rms']['signal']['component']['U'],quality_index['rms']['signal']['component']['V'],quality_index['rms']['signal']['component']['W'])
        warnings = load_json.merge_two_dicts(warnings,amplitude_check)
    except Exception as e:
        if opts.verb: print('VERTICAL AMPLITUDE CHECK', str(e))
        warnings = load_json.merge_two_dicts(warnings,{'unconsistent_vertical_amplitude': None})
        errors.append(str(e))

    try:
        # check PGA > 2g for one of the 3 components -ACC
        pga_check = windows.pga_check(stream_cut)
        warnings = load_json.merge_two_dicts(warnings,pga_check)

    except Exception as e:
        if opts.verb: print('PGA CHECK > 2g', str(e))
        warnings = load_json.merge_two_dicts(warnings,{'extreme_pga_values': None})
        errors.append(str(e))

    # check on cutoff frequency from SNR in FD
    try:
        # compute signal and noise windows for SNR in frequency domain(FD)
        sn = windows.signal_noise_windows_FD(stream_orig,wfs_cut['start_time'],sn_windows['ps_auto']['P'])
    except Exception as e:
        if opts.verb: print('sn duration', str(e))
        errors.append(str(e))

    try:
        #compute smoothed (Konno Ohmachi smoothing) FFT on both signal and noise streams
        fourier_smooth = windows.signal_noise_smooth_fft(sn['signal']['amplitude'],sn['noise']['amplitude'])
    except Exception as e:
        if opts.verb: print('fourier smooth', str(e))
        errors.append(str(e))

    try:
        # compute lc and Hc
        Nc = stream_orig[0].stats.sampling_rate/2
        warning_lc_hc,output_lc_hc = windows.compute_lc_hc(sn['noise']['duration'],sn['signal']['duration'],[fourier_smooth['noise']['component']['U']['amplitude'],fourier_smooth['noise']['component']['V']['amplitude'],fourier_smooth['noise']['component']['W']['amplitude']],\
                                                          [fourier_smooth['signal']['component']['U']['amplitude'],fourier_smooth['signal']['component']['V']['amplitude'],fourier_smooth['signal']['component']['W']['amplitude']],fourier_smooth['noise']['component']['U']['frequency'],Nc,magnitude)
        warnings = load_json.merge_two_dicts(warnings,warning_lc_hc)	
        parameters = load_json.merge_two_dicts(parameters,output_lc_hc)
    except Exception as e:
        if opts.verb: print('lc hc', str(e))
        warnings = load_json.merge_two_dicts(warnings,{'hc frequecies based on signal to noise ratio of the fft': None,'hc frequencies based on fixed threshold': None,'bad lc frequency':None, 'bad hc frequency':None})
        parameters = load_json.merge_two_dicts(parameters,{'lc':{'component':{'U':None,'V':None,'W':None}}, 'hc':{'component':{'U':None,'V':None,'W':None}}})
        errors.append(str(e))
        
    ########################################## Final Quality Class and corresponding Reason ############################

    try:
        windows = WaveformQualityCheck(None,None,None,None,None,None)
        classes = windows.waveform_class(parameters['quality_index']['average_value'],warnings,parameters)
        classes.update(classes)
    except Exception as e:
        if opts.verb: print('wfs classification', str(e))
        classes.update({'A':False,'B':False,'C':False,'D':True,'reason':'UnknownError'})
        errors.append(str(e))

    if opts.verb:
        print('SUMMARY:')
        print()
        print('warnings: ',warnings)
        print('classes: ',classes)
        print('parameters: ',parameters)
        print('errors:',errors)
        print()
        print('- - - -')

########################################################################################################################
    
    base_input_path = os.path.dirname(opts.ifile[0]) + '/'
    
    if opts.verb: print(base_input_path)

    for key, value in classes.items():  
        if value is True:
            classe = key
        
    message = []        
    for messagge, value in warnings.items():  
        if value is True:
            message.append(messagge)
    quality_dict = {'quality_class': classe, 'quality_reason': classes['reason'],'quality_features':message}
    
    if opts.copy == 'True' or (opts.copy == 'Guess' and quality_dict['quality_class'] in ['A', 'B']):
        JSON_name = 'quality_check'
        quality_json_file = opts.ifile[0].replace('.h5', '.' + JSON_name ) + '.json'
        with open(quality_json_file, 'w') as fp:
            json.dump(quality_dict, fp, indent=4,sort_keys=False)

    parameters['start_time']=str(parameters['start_time'])
    parameters['end_time']=str(parameters['end_time'])
    
    if opts.copy == 'True' or (opts.copy == 'Guess' and quality_dict['quality_class'] in ['A', 'B']):
        JSON_name = 'parameters'
        parameters_json_file =  opts.ifile[0].replace('.h5', '.' + JSON_name ) + '.json'
        with open(parameters_json_file, 'w') as fp:
            json.dump(parameters, fp, indent=4,sort_keys=False, cls=NpEncoder)

    # Create the input for automatic processing with PAO11
    settings = dictn.get('settings')
    data = dictn.get('data')
    settings['WaveformCut']['StartTime']=str(parameters['start_time'])
    settings['WaveformCut']['EndTime']=str(parameters['end_time'])
    settings['FourierSpectrumFilter']['HighCut']=np.array([parameters['hc']['component']['U'],parameters['hc']['component']['V'],parameters['hc']['component']['W']])
    settings['FourierSpectrumFilter']['LowCut']=[parameters['lc']['component']['U'],parameters['lc']['component']['V'],parameters['lc']['component']['W']]
    settings['WaveformDetrend']['T1Samples'] = None
    settings['WaveformDetrend']['T2Samples'] = None
    settings['WaveformDetrend']['T3Samples'] = None
    workflow = dictn.get('workflow')
    workflow['modality'] = 'A'
    workflow['method'] = 'P'

    dictn['settings'] = settings
    dictn['workflow'] = workflow
    dictn['data'] = data
    ##: to save json with processing info and data

    # AP
    dictn_ap = deepcopy(dictn)
    
    if opts.copy == 'True' or (opts.copy == 'Guess' and quality_dict['quality_class'] in ['A', 'B']):
        JSON_name = 'AP'
        ap_json_file = opts.ifile[0].replace('.h5', '_' + JSON_name ) + '.json'
        with open(ap_json_file, 'w') as fp:
            json.dump(dictn, fp, indent=4,sort_keys=False, cls=NpEncoder)

    # AB
    workflow['method'] = 'B'
    settings['WaveformDetrend']['T1Samples'] = 5
    settings['WaveformDetrend']['T2Samples'] = 20
    settings['WaveformDetrend']['T3Samples'] = 20
    settings['WaveformTaper']['side']['left'] = 5
    settings['WaveformTaper']['side']['right'] = None
    settings['FourierSpectrumFilter']['LowCut'] = [None, None, None]
    dictn['settings'] = settings
    dictn['workflow'] = workflow
    dictn['data'] = data
    dictn_ab = deepcopy(dictn)

    if opts.copy == 'True' or (opts.copy == 'Guess' and quality_dict['quality_class'] in ['A', 'B']):
        JSON_name = 'AB'
        ab_json_file = opts.ifile[0].replace('.h5', '_' + JSON_name ) + '.json'
        with open(ab_json_file, 'w') as fp:
            json.dump(dictn, fp, indent=4,sort_keys=False, cls=NpEncoder)
    
    
    out_dict = {}
    out_dict = quality_dict
    out_dict["parameters"]  = parameters

    out_json = json.dumps(out_dict, cls=NpEncoder)

    if len(errors) == 0 or quality_dict['quality_reason'] != None:

        out_dict["input_ap"] = dictn_ap
        out_dict["input_ab"] = dictn_ab

        return dict_fmt(exit_status=0, exit_message='', out_string=out_json, out_object=json.loads(out_json), out_format='json')
        
    else:
        return dict_fmt(exit_status=6, exit_message=json.dumps(errors), out_string=out_json, out_object=json.loads(out_json), out_format='json')


def main():
    
    ret_code = preprocessing(' '.join(sys_argv[1:]))
    #if ret_code['exit_status'] == 0 and ret_code['out_string'] : print(ret_code['out_string'])
    if ret_code['exit_message']: print(ret_code['exit_message'])
    sys_exit(ret_code['exit_status'])


if __name__ == "__main__": main()
