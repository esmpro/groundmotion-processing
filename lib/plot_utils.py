#!/usr/bin/env python3
from math import trunc as math_trunc
from obspy.core import UTCDateTime
import numpy as np

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from lib.format_utils import return_decimals
from lib.format_utils import fromUTCDateTime2time as fromUTCDateTime
from lib.math_utils import FFT_spectrum
from lib.utilities import PS_TravelTimes
from lib.asdf_file import ASDFFile


def S_plot(U_data_MP,V_data_MP,W_data_MP,opts,cur,transpflag):
    """
    Plot Response Spectra

    Args:
        U_data_MP(AuxiliaryDataContainer): first ground-motion component
        V_data_MP(AuxiliaryDataContainer): second ground-motion component
        W_data_MP(AuxiliaryDataContainer): third ground-motion component
        opts(Namespace)
        cur(NoneType)
        transpflag(bool): flag for transparency
    Returns:
        a figure
    """

    U_max_MP = max(U_data_MP.data[1])
    V_max_MP = max(V_data_MP.data[1])
    W_max_MP = max(W_data_MP.data[1])
    max_S = max(U_max_MP,V_max_MP,W_max_MP)
    pg_decimals = return_decimals(max_S/100.0)

    if opts.plottype == 'SA':
        tick_vect = [0, 0.5, 1, 1.5, 2]
        tick_lab_vect = ['0.0', '0.5', '1.0', '1.5', '2.0']
    elif opts.plottype == 'SD':
        tick_vect = [0, 2, 4, 6, 8, 10]
        tick_lab_vect = ['0', '2', '4', '6', '8', '10']    
    #~ initialize figure
    PRE_fig = plt.figure(figsize=(7.55, 5.65), dpi=100)
    #~ U_plot
    U_sub = 311; U_pos = 0.67; axis_position = 'top'
    S_subplot(PRE_fig,U_sub,U_pos,U_data_MP,pg_decimals,max_S,\
    U_max_MP,tick_vect,tick_lab_vect,axis_position,opts,None)
    #~ V_plot
    V_sub = 312; V_pos = 0.39; axis_position = 'none'
    S_subplot(PRE_fig,V_sub,V_pos,V_data_MP,pg_decimals,max_S,\
    V_max_MP,tick_vect,tick_lab_vect,axis_position,opts,None)
    #~ W_plot
    W_sub = 313; W_pos = 0.11; axis_position = 'bottom'
    S_subplot(PRE_fig,W_sub,W_pos,W_data_MP,pg_decimals,max_S,\
    W_max_MP,tick_vect,tick_lab_vect,axis_position,opts,None)
    #~ saving figure
    PRE_fig.savefig(opts.outfile + opts.plottype, dpi=100, transparent=transpflag)

def WF_plot(U_data_CV,V_data_CV,W_data_CV,U_data_MP,V_data_MP,W_data_MP,opts,time_offset, cur, processingtype,transpflag,proctype,origindepth, origintime, distance):
    """
    Plot Waveforms (plottype in postprocessing.py: "ACC", "VEL" or "DISP")

    Args:
        U_data_CV(StreamObject): first ground-motion component, unprocessed
        V_data_CV(StreamObject): second ground-motion component, unprocessed
        W_data_CV(StreamObject): third ground-motion component, unprocessed
        U_data_MP(StreamObject): first ground-motion component, processed
        V_data_MP(StreamObject): second ground-motion component, processed
        W_data_MP(StreamObject): third ground-motion component, processed
        opts(Namespace)
        time_offset(int): temporal offset for waveforms plot
        cur(NoneType)
        processingtype(str): type of processing. The choices are: "AB", "MB", "AP", "MP" or "MC".
        transpflag(bool): flag for transparency
        proctype(str): type of processing in lower case.
        origindepth(float): event depth
        origintime(UTCDateTime): event origin time
        distance(str): epicentral distance
    Returns:
        a figure
    """
    if opts.processingtype == 'AB' or opts.processingtype == 'MB': tag_met = 'eBASCO'
    elif opts.processingtype == 'AP' or opts.processingtype == 'MP': tag_met = 'PAO11'
    else: tag_met = 'CUSTOM'
    line_color = {'CV':'r','MP':'k','AP':'k','AB':'b','MB':'b','MC':'y'}
    line_width = {'CV':[0.8,0],'MP':[0,0.8],'AP':[0,0.8],'AB':[0,0.8],'MB':[0,0.8],'MC':[0,0.8],'NONE':[0,0]}
    max_starttime_CV = \
    max(U_data_CV[0].stats['starttime'],V_data_CV[0].stats['starttime'],W_data_CV[0].stats['starttime'])
    min_endtime_CV = \
    min(U_data_CV[0].stats['endtime'],V_data_CV[0].stats['endtime'],W_data_CV[0].stats['endtime'])
    max_starttime = \
    max(U_data_MP[0].stats['starttime'],V_data_MP[0].stats['starttime'],W_data_MP[0].stats['starttime'])
    min_endtime = \
    min(U_data_MP[0].stats['endtime'],V_data_MP[0].stats['endtime'],W_data_MP[0].stats['endtime'])
    
    U_max_abs_CV = U_data_CV[0].data[np.argmax(abs(U_data_CV[0].data))]
    V_max_abs_CV = V_data_CV[0].data[np.argmax(abs(V_data_CV[0].data))]
    W_max_abs_CV = W_data_CV[0].data[np.argmax(abs(W_data_CV[0].data))]
    U_max_abs_MP = U_data_MP[0].data[np.argmax(abs(U_data_MP[0].data))]
    V_max_abs_MP = V_data_MP[0].data[np.argmax(abs(V_data_MP[0].data))]
    W_max_abs_MP = W_data_MP[0].data[np.argmax(abs(W_data_MP[0].data))]
    max_abs_pga = max(abs(U_max_abs_MP),abs(V_max_abs_MP),abs(W_max_abs_MP))
    if opts.plottype == 'ACC':
        max_abs_pga = max(max_abs_pga,abs(U_max_abs_CV),abs(V_max_abs_CV),abs(W_max_abs_CV))
    pg_decimals = return_decimals(max_abs_pga/100.0)
    
    time_step = round((min_endtime - max_starttime)/5)
    step_0 = time_offset + 0.0*time_step
    step_1 = time_offset + 1.0*time_step
    step_2 = time_offset + 2.0*time_step
    step_3 = time_offset + 3.0*time_step
    step_4 = time_offset + 4.0*time_step
    step_5 = time_offset + 5.0*time_step
    tick_vect = [step_0, step_1, step_2, step_3, step_4, step_5]
    tick_lab_0 = max_starttime - max_starttime_CV + 0.0*time_step
    tick_lab_1 = max_starttime - max_starttime_CV + 1.0*time_step
    tick_lab_2 = max_starttime - max_starttime_CV + 2.0*time_step
    tick_lab_3 = max_starttime - max_starttime_CV + 3.0*time_step
    tick_lab_4 = max_starttime - max_starttime_CV + 4.0*time_step
    tick_lab_5 = max_starttime - max_starttime_CV + 5.0*time_step
    top_labels = ("%.0f" % tick_lab_0,"%.0f" % tick_lab_1,"%.0f" % tick_lab_2,"%.0f" % tick_lab_3,"%.0f" % tick_lab_4,"%.0f" % tick_lab_5)
    time_tick_lab_0 = fromUTCDateTime(max_starttime + 0.0*time_step)
    time_tick_lab_1 = fromUTCDateTime(max_starttime + 1.0*time_step)
    time_tick_lab_2 = fromUTCDateTime(max_starttime + 2.0*time_step)
    time_tick_lab_3 = fromUTCDateTime(max_starttime + 3.0*time_step)
    time_tick_lab_4 = fromUTCDateTime(max_starttime + 4.0*time_step)
    time_tick_lab_5 = fromUTCDateTime(max_starttime + 5.0*time_step)
    bottom_labels = (time_tick_lab_0,time_tick_lab_1,time_tick_lab_2,time_tick_lab_3,time_tick_lab_4,time_tick_lab_5)
    center_labels = ('','','','','','')
    
    p_s_times = PS_TravelTimes(origindepth, origintime, distance)

    if p_s_times:
        time_P = UTCDateTime(p_s_times[0])- max_starttime + time_offset
        time_S = UTCDateTime(p_s_times[1])- max_starttime + time_offset
    else: 
        time_P = None
        time_S = None

    #~ initialize figure
    PRE_fig = plt.figure(figsize=(7.55, 5.65), dpi=100)
    #~ U_plot
    U_sub = 311; U_pos = 0.67; axis_position = 'top'
    WF_subplot(PRE_fig,U_sub,U_pos,U_data_CV,U_data_MP,pg_decimals,max_abs_pga,\
    U_max_abs_CV,U_max_abs_MP,max_starttime - time_offset,min_endtime + time_offset,\
    tick_vect,top_labels,axis_position,time_P,time_S,opts,processingtype,proctype)
    #~ V_plot
    V_sub = 312; V_pos = 0.39; axis_position = 'none'
    WF_subplot(PRE_fig,V_sub,V_pos,V_data_CV,V_data_MP,pg_decimals,max_abs_pga,\
    V_max_abs_CV,V_max_abs_MP,max_starttime - time_offset,min_endtime + time_offset,\
    tick_vect,center_labels,axis_position,time_P,time_S,opts,processingtype,proctype)
    #~ W_plot
    W_sub = 313; W_pos = 0.11; axis_position = 'bottom'
    WF_subplot(PRE_fig,W_sub,W_pos,W_data_CV,W_data_MP,pg_decimals,max_abs_pga,\
    W_max_abs_CV,W_max_abs_MP,max_starttime - time_offset,min_endtime + time_offset,\
    tick_vect,bottom_labels,axis_position,time_P,time_S,opts,processingtype,proctype)
    #~ annotations
    if opts.txtflag == True and opts.rawflag == True:
        if opts.plottype == 'ACC': plt.figtext(0.94,0.915,'Unprocessed', fontsize=10, horizontalalignment='right', verticalalignment="bottom", color='r', figure=PRE_fig)
        plt.figtext(0.94,0.885,tag_met, fontsize=10, horizontalalignment='right', verticalalignment="bottom", color=line_color[opts.processingtype], figure=PRE_fig)
        
    #~ saving figure
    PRE_fig.savefig(opts.outfile + opts.plottype, dpi=100, transparent=transpflag)
   
def FFT_plot(U_data_CV,V_data_CV,W_data_CV,U_data_MP,V_data_MP,W_data_MP,U_aux_MP,V_aux_MP,W_aux_MP,opts):
    """
    Plot Fourier Spectra

    Args:
        U_data_CV(StreamObject): first ground-motion component, unprocessed
        V_data_CV(StreamObject): second ground-motion component, unprocessed
        W_data_CV(StreamObject): third ground-motion component, unprocessed
        U_data_MP(StreamObject): first ground-motion component, processed
        V_data_MP(StreamObject): second ground-motion component, processed
        W_data_MP(StreamObject): third ground-motion component, processed
        U_aux_MP(dict): auxiliary data of the first component
        V_aux_MP(dict): auxiliary data of the second component
        W_aux_MP(dict): auxiliary data of the third component
        opts(Namespace)
    Returns:
        a figure
    """
    if opts.processingtype == 'AB' or opts.processingtype == 'MB': tag_met = 'eBASCO'
    elif opts.processingtype == 'AP' or opts.processingtype == 'MP': tag_met = 'PAO11'
    else: tag_met = 'CUSTOM'
    
    line_color = {'CV':'r','MP':'k','AP':'k','AB':'b','MB':'b','MC':'y'}
    line_width = {'CV':[0.8,0],'MP':[0,0.8],'AP':[0,0.8],'AB':[0,0.8],'MB':[0,0.8],'MC':[0,0.8],'NONE':[0,0]}
       
    U_fft_CV_list = FFT_spectrum(U_data_CV)
    V_fft_CV_list = FFT_spectrum(V_data_CV)
    W_fft_CV_list = FFT_spectrum(W_data_CV)
    U_fft_MP_list = FFT_spectrum(U_data_MP)
    V_fft_MP_list = FFT_spectrum(V_data_MP)
    W_fft_MP_list = FFT_spectrum(W_data_MP)
    U_freq_CV = U_fft_CV_list[0]; U_fft_CV = U_fft_CV_list[1]
    V_freq_CV = V_fft_CV_list[0]; V_fft_CV = V_fft_CV_list[1]
    W_freq_CV = W_fft_CV_list[0]; W_fft_CV = W_fft_CV_list[1]
    U_freq_MP = U_fft_MP_list[0]; U_fft_MP = U_fft_MP_list[1]
    V_freq_MP = V_fft_MP_list[0]; V_fft_MP = V_fft_MP_list[1]
    W_freq_MP = W_fft_MP_list[0]; W_fft_MP = W_fft_MP_list[1]

    max_U_fft_CV = max(U_fft_CV); max_V_fft_CV = max(V_fft_CV); max_W_fft_CV = max(W_fft_CV)
    max_U_fft_MP = max(U_fft_MP); max_V_fft_MP = max(V_fft_MP); max_W_fft_MP = max(W_fft_MP)
    max_fft = max(max_U_fft_CV,max_V_fft_CV,max_W_fft_CV,max_U_fft_MP,max_V_fft_MP,max_W_fft_MP)
    
    #~ initialize figure
    PRE_fig = plt.figure(figsize=(7.55, 5.65), dpi=100)
    ypos1 = 0.67; ypos2 = 0.39; ypos3 = 0.11
    #~ U_plot
    U_sub = 311; U_xpos = 0.1; U_ypos = ypos1; axis_position = 'top'
    FFT_subplot(PRE_fig,U_sub,U_xpos,U_ypos,U_freq_CV,U_fft_CV,U_freq_MP,U_fft_MP,U_data_CV,max_fft,U_aux_MP['low_cut_frequency_hz'],U_aux_MP['high_cut_frequency_hz'],axis_position,opts)
    #~ V_plot
    V_sub = 312; V_xpos = 0.1; V_ypos = ypos2; axis_position = 'none'
    FFT_subplot(PRE_fig,V_sub,V_xpos,V_ypos,V_freq_CV,V_fft_CV,V_freq_MP,V_fft_MP,V_data_CV,max_fft,V_aux_MP['low_cut_frequency_hz'],V_aux_MP['high_cut_frequency_hz'],axis_position,opts)
    #~ W_plot
    W_sub = 313; W_xpos = 0.1; W_ypos = ypos3; axis_position = 'bottom'
    FFT_subplot(PRE_fig,W_sub,W_xpos,W_ypos,W_freq_CV,W_fft_CV,W_freq_MP,W_fft_MP,W_data_CV,max_fft,W_aux_MP['low_cut_frequency_hz'],W_aux_MP['high_cut_frequency_hz'],axis_position,opts)
    #~ annotations
    if opts.txtflag == True and opts.rawflag == True:

        plt.figtext(0.8,0.9,'Unprocessed', fontsize=10, verticalalignment="bottom", color='r', figure=PRE_fig)
        plt.figtext(0.8,0.87,tag_met, fontsize=10, verticalalignment="bottom", color=line_color[opts.processingtype], figure=PRE_fig)
    if opts.txtflag == True and opts.rawflag == False:   
        plt.figtext(0.8,0.87,tag_met, fontsize=10, verticalalignment="bottom", color=line_color[opts.processingtype], figure=PRE_fig)
    #~ saving figure
    PRE_fig.savefig(opts.outfile + 'FFT', dpi=100, transparent=opts.transpflag)
    
def PRE_plot(U_data_CV,V_data_CV,W_data_CV,outfile,transpflag):
    """
    Plot Unprocessed waveforms.

    Args:
        U_data_CV(StreamObject): first ground-motion component, unprocessed
        V_data_CV(StreamObject): second ground-motion component, unprocessed
        W_data_CV(StreamObject): third ground-motion component, unprocessed
        outfile(str): name of output file
        transpflag(bool): flag for transparency
    Returns:
        a figure
    """
    min_starttime = \
    min(U_data_CV[0].stats['starttime'],V_data_CV[0].stats['starttime'],W_data_CV[0].stats['starttime'])
    max_endtime = \
    max(U_data_CV[0].stats['endtime'],V_data_CV[0].stats['endtime'],W_data_CV[0].stats['endtime'])
    U_max_abs = U_data_CV[0].data[np.argmax(abs(U_data_CV[0].data))]
    V_max_abs = V_data_CV[0].data[np.argmax(abs(V_data_CV[0].data))]
    W_max_abs = W_data_CV[0].data[np.argmax(abs(W_data_CV[0].data))]
    max_abs_pga = max(abs(U_max_abs),abs(V_max_abs),abs(W_max_abs))
    pg_decimals = return_decimals(max_abs_pga/100.0)
    
    #~ initialize figure
    PRE_fig = plt.figure(figsize=(3.7, 2.1), dpi=100)
    #~ U_plot
    U_sub = 311; U_pos = 0.6666
    PRE_subplot(PRE_fig,U_sub,U_pos,U_data_CV,pg_decimals,max_abs_pga,\
    U_max_abs,min_starttime,max_endtime)
    #~ V_plot
    V_sub = 312; V_pos = 0.3333
    PRE_subplot(PRE_fig,V_sub,V_pos,V_data_CV,pg_decimals,max_abs_pga,\
    V_max_abs,min_starttime,max_endtime)
    #~ W_plot
    W_sub = 313; W_pos = 0.0000
    PRE_subplot(PRE_fig,W_sub,W_pos,W_data_CV,pg_decimals,max_abs_pga,\
    W_max_abs,min_starttime,max_endtime)
    #~ saving figure
    PRE_fig.savefig(outfile, dpi=100, transparent=transpflag)

#~ subplot functions
def S_subplot(PRE_fig,X_sub,X_pos,X_data_MP,pg_decimals,max_S,\
    X_max_MP,tick_vect,tick_lab_vect,axis_position,opts,U_data_GMM):
    """
    Subplot of S_plot (Plot of Response Spectra)

    Args:
        PRE_fig(Figure): S_plot Figure
        X_sub(int): position of the subplot described by three integers (nrows, ncols, index)
        X_pos(float): position of subplot from 0 to 1
        X_data_MP(AuxiliaryDataContainer): Auxiliary data of the X ground-motion component
        pg_decimals(str): decimals of peak values
        max_S(float): max value recorded on the 3 components
        X_max_MP(float): max value recorded on the X component
        tick_vect(list): ticks on the x axis (periods)
        tick_lab_vect(list): labels for the ticks on the x axis (periods)
        axis_position(str): position of subplot may be "top", "none, "bottom"
        opts(Namespace)
        U_data_GMM(NoneType): prepared to include the predicted spectral amplitude from a ground motion model (GMM)
    Returns:
        Subplot of S_plot
    """
    line_color = {'CV':'r','MP':'k','AP':'k','AB':'b','MB':'b','MC':'y'}
    line_width = [[0.8, 0],[0, 0.8],[0,0]]
    if opts.plottype == 'SA':
        PGX_str = 'PGA'; xlim_max = 2.0; PGX_key = 'pga_cm_s_2'
        y_lab_str = r'Acceleration response spectrum (5% damped) [cm/$\mathregular{s^2}$]'
    elif opts.plottype == 'SD': 
        PGX_str = 'PGD'; xlim_max = 10.0; PGX_key = 'pgd_cm'
        y_lab_str = 'Displacement response spectrum (5% damped) [cm]'
    
    X_ax = PRE_fig.add_subplot(X_sub)
    X_pga_MP = eval('"%.' + pg_decimals + 'f" % X_data_MP.parameters["' + PGX_key + '"]')
    X_str_MP = PGX_str + ' = ' + X_pga_MP
    
    X_plt_MP = X_ax.plot(X_data_MP.data[0],X_data_MP.data[1],color=line_color[opts.processingtype],linewidth=line_width[0][0])

    curr_component = X_data_MP.path.split('_')[1][2].upper()
    if opts.chflag == True:
        plt.figtext(0.15,0.265 + X_pos,curr_component,horizontalalignment='left', fontsize=12, verticalalignment="top", figure=PRE_fig, color='k')

    first_tick = 0
    second_tick = 2*max_S*1.1/7
    third_tick = 4*max_S*1.1/7
    fourth_tick = 6*max_S*1.1/7
    X_ax.set_yticks([first_tick, second_tick, third_tick, fourth_tick])
    X_ax.set_yticklabels((eval('"%.' + pg_decimals + 'f" % first_tick'),eval('"%.' + pg_decimals + 'f" % second_tick'),eval('"%.' + pg_decimals + 'f" % third_tick'),eval('"%.' + pg_decimals + 'f" % fourth_tick')),fontsize=10)

    X_ax.set_position([0.13,X_pos,0.83,0.28])
    X_ax.set_xlim([0,xlim_max])
    X_ax.set_ylim([0,max_S*1.2])
    X_ax.set_xticks(tick_vect)
    X_axis = X_ax.xaxis
    if axis_position == 'top':
        X_ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False) 
    elif axis_position == 'none':
        X_ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False) 
        X_ax.set_ylabel(y_lab_str,fontsize=10)
    elif axis_position == 'bottom':
        X_ax.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=True) 
        X_ax.set_xlabel('Period [s]',fontsize=10)
    X_ax.set_xticklabels(tick_lab_vect,fontsize=10)
    plt.grid(opts.gridflag)
    #X_ax.grid()
    if opts.axesflag == False: X_ax.axis('off')
    
    if opts.peakflag == True : plt.figtext(0.94,0.03 + X_pos,X_str_MP,horizontalalignment='right', fontsize=10, verticalalignment="top", figure=PRE_fig, color=line_color[opts.processingtype])   

       
def WF_subplot(PRE_fig,X_sub,X_pos,X_data_CV,X_data_MP,pg_decimals,max_abs_pga,\
X_max_abs_CV,X_max_abs_MP,max_starttime,min_endtime,tick_vect,tick_lab_vect,\
axis_position,time_P,time_S,opts,processingtype,proctype):
    """
    Subplot of WF_plot (Plot of waveforms)

    Args:
        PRE_fig(Figure): WF_plot Figure
        X_sub(int): position of the subplot described by three integers (nrows, ncols, index)
        X_pos(float): position of subplot from 0 to 1
        X_data_CV(StreamObject): stream of the X ground-motion component, unprocessed
        X_data_MP(StreamObject): stream of the X ground-motion component, processed
        pg_decimals(str): decimals of peak values
        max_abs_pga(float): max value recorded on the 3 components
        X_max_abs_CV: max value recorded on the X component (unprocessed)
        X_max_abs_MP: max value recorded on the X component (processed)
        max_starttime(UTCDateTime): maximum startime of the 3 components
        min_endtime(UTCDateTime): minumum endtime of the 3 components
        tick_vect(list): ticks on the x axis (periods)
        tick_lab_vect(list): labels for the ticks on the x axis (periods)
        axis_position(str): position of subplot may be "top", "none, "bottom"
        time_P(float): theoretical P wave arrival (Travel Times) in seconds
        time_S(float): theoretical S wave arrival (Travel Times) in seconds
        opts(Namespace)
        processingtype(str): type of processing. The choices are: "AB", "MB", "AP", "MP" or "MC".
        proctype(str): type of processing in lower case. 
    Returns:
        Subplot of WF_plot
    """
    if opts.plottype == 'ACC':
        PGX_str = 'PGA'; y_lab_str = r'Acceleration [cm/$\mathregular{s^2}$]'
    elif opts.plottype == 'VEL': PGX_str = 'PGV'; y_lab_str = 'Velocity [cm/s]'
    elif opts.plottype == 'DIS': PGX_str = 'PGD'; y_lab_str = 'Displacement [cm]'
    
    X_ax = PRE_fig.add_subplot(X_sub)
    X_pga_MP = eval('"%.' + pg_decimals + 'f" % X_max_abs_MP')
    X_str_MP = PGX_str + ' = ' + X_pga_MP

    line_color = {'CV':'r','MP':'k','AP':'k','AB':'b','MB':'b','MC':'y'}
    line_width = {'CV':[0.8,0],'MP':[0,0.8],'AP':[0,0.8],'AB':[0,0.8],'MB':[0,0.8],'MC':[0,0.8],'NONE':[0,0]}

    X_pga_CV = eval('"%.' + pg_decimals + 'f" % X_max_abs_CV')
    X_str_CV = PGX_str + ' = ' + X_pga_CV
                     
    if opts.plottype == 'ACC' and opts.rawflag == True: 
        tag = 'CV' 
        X_data_CV.trim(starttime=max_starttime, endtime=min_endtime, pad=True, fill_value=None)
        X_plt_CV = X_ax.plot(np.arange(0,X_data_CV[0].stats['npts'])*X_data_CV[0].stats['delta'],X_data_CV[0].data,color='r',linewidth=line_width[tag][0])
        X_data_MP.trim(starttime=max_starttime, endtime=min_endtime, pad=True, fill_value=None)
        X_plt_MP = X_ax.plot(np.arange(0,X_data_MP[0].stats['npts'])*X_data_MP[0].stats['delta'],X_data_MP[0].data,color=line_color[opts.processingtype],linewidth=line_width[opts.processingtype.upper()][1])                    
    if opts.plottype == 'ACC' and opts.rawflag == False: 
        tag = opts.processingtype.upper() 
        X_data_MP.trim(starttime=max_starttime, endtime=min_endtime, pad=True, fill_value=None)
        X_plt_MP = X_ax.plot(np.arange(0,X_data_MP[0].stats['npts'])*X_data_MP[0].stats['delta'],X_data_MP[0].data,color=line_color[opts.processingtype],linewidth=line_width[tag][1])  
    if opts.plottype == 'VEL' or  opts.plottype == 'DIS':
        tag = opts.processingtype.upper()
        X_data_MP.trim(starttime=max_starttime, endtime=min_endtime, pad=True, fill_value=None)
        X_plt_MP = X_ax.plot(np.arange(0,X_data_MP[0].stats['npts'])*X_data_MP[0].stats['delta'],X_data_MP[0].data,color=line_color[opts.processingtype],linewidth=line_width[tag][1]) 
    
    first_tick = -2*max_abs_pga/3
    second_tick = 0
    third_tick = 2*max_abs_pga/3
    X_ax.set_yticks([first_tick, second_tick, third_tick])
    X_ax.set_yticklabels((eval('"%.' + pg_decimals + 'f" % first_tick'),eval('"%.' + pg_decimals + 'f" % second_tick'),eval('"%.' + pg_decimals + 'f" % third_tick')),fontsize=10)    
    X_ax.set_position([0.13,X_pos,0.83,0.28])
    X_ax.set_xlim([0,min_endtime - max_starttime])
    X_ax.set_ylim([-max_abs_pga*1.1,max_abs_pga*1.1])
    X_ax.set_xticks(tick_vect)
    X_axis = X_ax.xaxis
    if axis_position == 'top':
        X_axis.set_ticks_position('top')
    elif axis_position == 'none':
        X_ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False) 
        X_ax.set_ylabel(y_lab_str,fontsize=10)
    elif axis_position == 'bottom':
        X_ax.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=True) 
        X_ax.set_xlabel('Time [hh:mm:ss]',fontsize=10)
    X_ax.set_xticklabels(tick_lab_vect,fontsize=10)
    
    plt.grid(opts.gridflag)
    
    if opts.axesflag == False: X_ax.axis('off')
    if opts.chflag == True:
        plt.figtext(0.15,0.265 + X_pos,X_data_CV[0].stats['channel'][-1],horizontalalignment='left', fontsize=12, verticalalignment="top", figure=PRE_fig, color='k')
    if opts.peakflag == True:
        if  opts.rawflag == True:
            if opts.plottype == 'ACC': plt.figtext(0.94,0.06 + X_pos,X_str_CV,horizontalalignment='right', fontsize=10, verticalalignment="top", figure=PRE_fig, color=line_color['CV'])
            plt.figtext(0.94,0.03 + X_pos,X_str_MP,horizontalalignment='right', fontsize=10, verticalalignment="top", figure=PRE_fig, color=line_color[opts.processingtype])
        if  opts.rawflag == False: plt.figtext(0.94,0.03 + X_pos,X_str_MP,horizontalalignment='right', fontsize=10, verticalalignment="top", figure=PRE_fig, color=line_color[opts.processingtype])
    if opts.psflag == True:
        PS_height = 5*max_abs_pga/6
        X_P_aux = X_ax.axvline(x=time_P,color='#0893cf',ymin=0.1,ymax=0.9)
        X_S_aux = X_ax.axvline(x=time_S,color='#891075',ymin=0.1,ymax=0.9)
        X_ax.text(x=time_P,y=PS_height,s=' P',color='#0893cf', verticalalignment="bottom", fontsize=10)
        X_ax.text(x=time_S,y=PS_height,s=' S',color='#891075', verticalalignment="bottom", fontsize=10)
    
def FFT_subplot(PRE_fig,X_sub,X_xpos,X_ypos,X_freq_CV,X_fft_CV,X_freq_MP,X_fft_MP,X_data,X_max_fft,lcf_aux,hcf_aux,axis_position,opts):
    """
    Subplot of FFT_plot (Plot of Fourier Spectra)

    Args:
        PRE_fig(Figure): FFT_plot Figure
        X_sub(int): position of the subplot described by three integers (nrows, ncols, index)
        X_xpos(float): position of subplot from 0 to 1 (in the x direction)
        X_ypos(float): position of subplot from 0 to 1 (in the y direction)
        X_freq_CV(ndarray): frequencies of Fourier Spectrum (unprocessed waveforms)
        X_fft_CV(ndarray): amplitudes of Fourier Spectrum (unprocessed waveforms)
        X_freq_MP(ndarray): frequencies of Fourier Spectrum (processed waveforms)
        X_fft_MP: amplitudes of Fourier Spectrum (processed waveforms)
        X_data(StreamObject)
        X_max_fft(float): max FFT value of the 3 components
        lcf_aux(float): low-cut frequency
        hcf_aux(int): high-cut frequency
        axis_position(str): position of subplot may be "top", "none, "bottom"
        opts(Namespace)
    Returns:
        Subplot of FFT_plot
    """
    line_color = {'CV':'r','MP':'k','AP':'k','AB':'b','MB':'b','MC':'y'}
    line_width = {'CV':[0.8,0],'MP':[0,0.8],'AP':[0,0.8],'AB':[0,0.8],'MB':[0,0.8],'MC':[0,0.8],'NONE':[0,0]}
    

    max_yaxis = X_max_fft*1.2
    min_yaxis = max_yaxis*0.0001
    X_ax = PRE_fig.add_subplot(X_sub)
    if opts.rawflag == True:
        tag = 'CV'
        X_plt_CV = X_ax.plot(X_freq_CV,X_fft_CV,color=line_color[tag],linewidth=line_width[tag][0])
    tag = opts.processingtype	    
    X_plt_MP = X_ax.plot(X_freq_MP,X_fft_MP,line_color[tag],linewidth=line_width[tag][1])
        
    if opts.cornerfreq == True:
        X_lcf_aux = X_ax.axvline(x=lcf_aux,color='g')
        X_hcf_aux = X_ax.axvline(x=hcf_aux,color='g')
    X_ax.set_xscale('log')
    X_ax.set_yscale('log')
    X_ax.set_xlim([0.01,100])
    X_ax.set_ylim([min_yaxis,max_yaxis])
    X_ax.set_xticks([0.01, 0.1, 1, 10, 100])
    if axis_position == 'top':
        X_ax.tick_params(axis='x', which='both', bottom=False, top=True, labelbottom=False) 
    elif axis_position == 'none':
        X_ax.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False) 
        X_ax.set_ylabel('fourier',fontsize=10)
        X_ax.set_ylabel(r'Fourier spectrum [cm/s]',fontsize=9, labelpad=4)
    elif axis_position == 'bottom':
        X_ax.tick_params(axis='x', which='both', bottom=True, top=False, labelbottom=True) 
        X_ax.set_xlabel('Period [s]',fontsize=10)
        X_ax.set_xlabel('Frequency [Hz]',fontsize=9, labelpad=1)
        
    X_ax.set_xticklabels(('0.01', '0.1', '1', '10', '100'),fontsize=8)
    plt.grid(opts.gridflag)
    if opts.chflag == True:
        plt.figtext(0.03 + X_xpos,0.25 + X_ypos,X_data[0].stats['channel'][-1],horizontalalignment='left', fontsize=11, verticalalignment="top", figure=PRE_fig)
    X_ax.set_position([X_xpos,X_ypos,0.83,0.28])
    
    
    #------------------------
    first_tick = float(10**(math_trunc(np.log10(max_yaxis))))
    second_tick = first_tick/10.0
    third_tick = first_tick/100.0
    fourth_tick = first_tick/1000.0
    X_ax.set_yticks([fourth_tick, third_tick, second_tick, first_tick])
    first_decimal_val = return_decimals(first_tick)
    second_decimal_val = return_decimals(second_tick)
    third_decimal_val = return_decimals(third_tick)
    fourth_decimal_val = return_decimals(fourth_tick)
    first_tick_label = eval('"%.' + first_decimal_val + 'f" % first_tick')
    second_tick_label = eval('"%.' + second_decimal_val + 'f" % second_tick')
    third_tick_label = eval('"%.' + third_decimal_val + 'f" % third_tick')
    fourth_tick_label = eval('"%.' + fourth_decimal_val + 'f" % fourth_tick')
    X_ax.set_yticklabels((fourth_tick_label, third_tick_label, second_tick_label, first_tick_label),fontsize=8)
    #------------------------

def PRE_subplot(PRE_fig,X_sub,X_pos,X_data_CV,pg_decimals,max_abs_pga,\
X_max_abs,min_starttime,max_endtime):
    """
    Subplot of PRE_plot (Plot of unprocessed waveforms)

    Args:
        PRE_fig(Figure): PRE_plot Figure
        X_sub(int): position of the subplot described by three integers (nrows, ncols, index)
        X_pos(float): position of subplot from 0 to 1
        X_data_CV(StreamObject): stream of the X ground-motion component, unprocessed
        pg_decimals(str): decimals of peak values
        max_abs_pga(float): max value recorded on the 3 components
        X_max_abs(float): max value recorded on the X component (unprocessed)
        min_starttime(UTCDateTime): minimum startime of the 3 components
        max_endtime(UTCDateTime): maximum endtime of the 3 components
    Returns:
        Subplot of PRE_plot
    """
    X_ax = PRE_fig.add_subplot(X_sub)
    X_dur = round((X_data_CV[0].stats['npts']-1)*X_data_CV[0].stats['delta'],0)
    X_pga = eval('"%.' + pg_decimals + 'f" % X_max_abs')
    X_str = r'DUR: ' + str(int(X_dur)) + ' s - PGA: ' + X_pga + ' cm/$\mathregular{s^2}$'
    X_data_CV.trim(starttime=min_starttime, endtime=max_endtime, pad=True, fill_value=None)
    X_plt = X_ax.plot(np.arange(0,X_data_CV[0].stats['npts'])*X_data_CV[0].stats['delta'],X_data_CV[0].data,color='r',linewidth=0.8)
    X_ax.set_position([0,X_pos,1,0.3333])
    plt.figtext(0.97,0.3 + X_pos,X_str,horizontalalignment='right', fontsize=10, verticalalignment="top", figure=PRE_fig, color='#717171')
    plt.figtext(0.03,0.3 + X_pos,X_data_CV[0].stats['channel'][-1],horizontalalignment='left', fontsize=10, verticalalignment="top", figure=PRE_fig, color='#717171')
    X_ax.set_xlim([0,X_dur])
    X_ax.set_ylim([-max_abs_pga*1.05,max_abs_pga*1.05])
    X_ax.axis('off')
