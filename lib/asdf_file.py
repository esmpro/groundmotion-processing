#!/usr/bin/env python3
import pyasdf
import os
from obspy.core import Stream
from datetime import datetime
import numpy as np
from lib.utilities import ArraytoTrace
from lib.format_utils import fromUTCDateTime
from lib.dictionary import JSON

class ASDFFile:
    """ Read and retrieve information from ASDF Files."""

    def __init__(self, pathname):
        try:
            self.pathname = pathname
            self.ds = pyasdf.ASDFDataSet(pathname, mode='r')

        except IOError as e:
            print(e.message)

    def get_event_ids(self):
        """ Get event IDs """
        return [str(event.resource_id).split('=')[1] for event in self.ds.events]

    def get_resource_id(self):
        """ Get resource IDs """
        return [str(event.resource_id) for event in self.ds.events]
    
    def get_events (self):
        """ Get events """
        return self.ds.events
    
    def get_depth (self):
        """ Get event depth """
        return self.get_events().events[0].origins[0].depth
    
    def get_origin_time (self):
        """ Get event origin time """
        return self.get_events().events[0].origins[0].time

    def get_epi_dist (self):
        """ Get epicentral distance """
        station = self.get_station_ids()[0].replace('.','_')
        event = self.get_event_ids()[0]
        tag = self.get_tag(self.get_station_ids()[0],event)
        return self.get_dataset().auxiliary_data.Headers[station][tag[0]].parameters['epicentral_distance_km']
    
    def get_channel_code(self):
        """ Get channel code """
        station = self.get_station_ids()[0].replace('.','_')
        return self.ds.waveforms[station].StationXML.get_contents()['channels']
    
    def get_network_code(self):
        """ Get network code """
        return self.get_channel_code()[0].split('.')[0]
    
    def get_station_code(self):
        """ Get station code """
        return self.get_channel_code()[0].split('.')[1]

    def get_location_code(self):
        """ Get location code """
        return self.get_channel_code()[0].split('.')[2]
                   
    def get_station_ids(self):
        """ Get station IDs """
        return list(self.ds.waveforms.list())
    
    def get_pathname(self):
        """ Get pathname """
        return self.pathname

    def get_dataset(self):
        """ Extract dataset: Waveform + Spectra + Auxiliary Data. """
        return self.ds

    def get_magnitude(self):
        """ Get magnitude """
        return self.ds.events[0].magnitudes[0].mag

    def get_tag(self, station, event):
        """ 
        Get waveform tags
            Args:
                station(str): network and station code. Es. 'IT.CLO'
                event(str): event code.

            Returns:
                list of waveform tags.
        """
        asdf_file = ASDFFile(self.pathname)
        if station in asdf_file.get_station_ids(): # and event in asdf_file.get_event_ids():
            event = event.lower().replace('-', '_')
            tags = list(filter(lambda x: (event + '_acc_cv' in x), self.ds.waveforms[station].get_waveform_tags()))
        else:
            raise AssertionError("ERROR: station " + station + " or event " + event + " not found")
        return tags

    def get_header(self, station, event):
        """ 
        Get header
            Args:
                station(str): network and station code. Es. 'IT.CLO'
                event(str): event code.

            Returns:
                list of waveform headers.
        """
        return [self.ds.auxiliary_data.Headers[station.replace('.', '_')][self.get_tag(station, event)[0]].parameters,
                self.ds.auxiliary_data.Headers[station.replace('.', '_')][self.get_tag(station, event)[1]].parameters,
                self.ds.auxiliary_data.Headers[station.replace('.', '_')][self.get_tag(station, event)[2]].parameters]

    def get_data_shape(self, station, event):
        """ 
        Get dataset
            Args:
                station(str): network and station code. Es. 'IT.CLO'
                event(str): event code.

            Returns:
                list of Dataset Objects.
        """
        return [self.ds.auxiliary_data.Headers[station.replace('.', '_')][self.get_tag(station, event)[0]].data,
                self.ds.auxiliary_data.Headers[station.replace('.', '_')][self.get_tag(station, event)[1]].data,
                self.ds.auxiliary_data.Headers[station.replace('.', '_')][self.get_tag(station, event)[2]].data]
                
    def update_header(self, headers, dictn, seconds_to_cut):
        """ 
        Update header
            Args:
                headers(list): list of headers.
                dictn(dict): dictionary with processing info.
                seconds_to_cut(tuple): startime and endtime in seconds.

            Returns:
                list of updated headers.
        """
        for index in range(len(headers)):
            # trigger type
            headers[index]['late_normal_triggered'] = dictn.get("workflow")["trigger"]
            # filter corners
            if dictn.get("settings")["FourierSpectrumFilter"]["LowCut"][index] is not None:
                headers[index]['low_cut_frequency_hz'] = round(dictn.get("settings")["FourierSpectrumFilter"]["LowCut"][index],2)
            headers[index]['high_cut_frequency_hz'] = round(dictn.get("settings")["FourierSpectrumFilter"]["HighCut"][index],2)
            # filter order
            headers[index]['filter_order'] = dictn.get("settings")["FourierSpectrumFilter"]["order"]
            # filter type
            headers[index]['filter_type'] = 'BUTTERWORTH'
            # DATA_TIMESTAMP
            headers[index]['time_stamp'] = fromUTCDateTime(datetime.now())
            # processing method          
            if dictn.get("workflow")['modality'] == 'A' and dictn.get("workflow")['method'] == 'P':
                headers[index]['processing'] = "automatic (Paolucci et al., 2011)"
            elif dictn.get("workflow")['modality'] == 'M' and dictn.get("workflow")['method'] == 'P':
                headers[index]['processing'] = "manual (Paolucci et al., 2011)"
            elif dictn.get("workflow")['modality'] == 'A' and dictn.get("workflow")['method'] == 'B':
                headers[index]['processing'] = "automatic (Schiappapietra et al., 2020)"
            elif dictn.get("workflow")['modality'] == 'M' and dictn.get("workflow")['method'] == 'B':
                headers[index]['processing'] = "manual (Schiappapietra et al., 2020)"

            # remove MEAN REMOVED from cv waveforms
            if "MEAN REMOVED" in str(headers[index]['user1']):
                headers[index]['user1'] = ''
            # second to cut
            headers[index]['seconds_to_cut_from_beginning'] = round(seconds_to_cut[0][index],1)
            headers[index]['seconds_to_cut_from_end'] = round(seconds_to_cut[1][index],1)
            
            # taper
            headers[index]['taper_at_beginning'] = int(round(dictn.get('settings')['WaveformTaper']['side']['left'],2))
            if dictn.get('settings')['WaveformTaper']['side']['right'] is None:
                headers[index]['taper_at_end'] = ''
            else:
                headers[index]['taper_at_end'] = int(round(dictn.get('settings')['WaveformTaper']['side']['right'],2))
            
            if dictn.get('settings')['WaveformTaper']['EndLT'] is None:
                headers[index]['taper_at_end_late_triggered_vel_dis'] = ''
            else:
                headers[index]['taper_at_end_late_triggered_vel_dis'] = int(round(dictn.get('settings')['WaveformTaper']['EndLT'],1))
            headers[index]['baseline_correction'] = 'BASELINE REMOVED'
            # quality class
            #print(self.pathname)
            if os.path.exists(os.path.abspath(self.pathname).replace('.h5','.quality_check.json')):
                base_path = os.path.dirname(self.pathname)
                filename = os.path.basename(os.path.abspath(self.pathname).replace('.h5','.quality_check.json'))
                load_json =  JSON(base_path, filename)
                quality_dict = load_json.loadJSON()
                # ~ print(quality_dict['quality_class'])
                # ~ print(quality_dict['quality_reason'])
                # ~ print (quality_dict['quality_features'])
                # ~ print(headers[index]['quality_class'])
                # ~ print(headers[index]['quality_features'])
                # ~ print(headers[index]['quality_reason'])
                headers[index]['quality_class'] = quality_dict['quality_class']
                headers[index]['quality_reason'] = quality_dict['quality_reason']
                headers[index]['quality_features'] = quality_dict['quality_features']
            else:
                headers[index]['quality_class'] = ''
                headers[index]['quality_reason'] = ''
                headers[index]['quality_features'] = ''
        return headers

    def update_stationXML(self, station, ds):
        """ 
        Update station XML
            Args:
                station(str): network and station code. Es. 'IT.CLO'
                ds(ASDFDataSet): ASDF dataset object.

            Returns:
                the ASDFDataSet with the updated stationXML and stationXML itself.
        """
        stationXML = self.get_dataset().waveforms[station.replace('.', '_')].StationXML
        ds.add_stationxml(stationXML)
        return ds, stationXML

    def update_UncorrectedWaveform(self, tags, dictn, param, header, ds, string):
        """ 
        Update ASDFDataSet with Uncorrected Waveforms
        Args:
            tags(list): list of waveform tags
            dictn(dict): dictionary with processing info
            param(list): list of Stream Objects
            header(list): list of headers
            ds(ASDFDataSet): empty dataset
            string(str): substring in tags es. "_acc_cv"
        
        Returns:
            updated ASDFDataSet and list of tags.
        """
        _tags = tags
        dat = self.get_data_shape(self.get_station_ids()[0], self.get_event_ids()[0])
        for index in range(len(tags)):
            ds.add_waveforms(param[index][0], tag=_tags[index], event_id=self.get_event_ids()[0])
        
        pre_tag_path = param[0][0].stats.network + "_" + param[0][0].stats.station + "/"
        for index in range(len(header)):
            
            ds.add_auxiliary_data(data=dat[index], data_type="Headers", path=pre_tag_path + _tags[index],
                                  parameters=header[index])
        
        return ds, _tags

    def update_CorrectedWaveform(self, tags, dictn, param, headers, ds, string):
        """ 
        Update ASDFDataSet with Corrected Waveforms 
        Args:
            tags(list): list of waveform tags
            dictn(dict): dictionary with processing info
            param(list): list of Stream Objects
            header(list): list of headers
            ds(ASDFDataSet): empty dataset
            string(str): substring in tags es. "_acc_"
        
        Returns:
            updated ASDFDataSet and list of tags.
        """
        _tags = []
        
        for tag in tags:
            _tags.append(tag.replace('_acc_cv',
                                     string + dictn.get("workflow")["modality"].lower() + dictn.get("workflow")[
                                         "method"].lower()))
        dat = self.get_data_shape(self.get_station_ids()[0], self.get_event_ids()[0])
        for index in range(len(tags)):
            ds.add_waveforms(param[index], tag=_tags[index], event_id=self.get_event_ids()[0])
        pre_tag_path = self.get_station_ids()[0].replace('.','_') + "/"
        for index in range(len(headers)):
            ds.add_auxiliary_data(data=dat[index], data_type="Headers", path=pre_tag_path + _tags[index],
                                  parameters=headers[index])
        return ds, _tags
    
    def pre_tag (self,param):
        """
        Returns a pre_tag
        Args:
            param(StreamObject)
        Returns: string with pre_tag. es. "IT_CLO/"
        """
        amplitudes = []
        for index in range(len(param)):
            amplitudes.append(param[index].data)
        traces = ArraytoTrace(param, amplitudes)
        pre_tag_path = traces[0].stats.network + "_" + traces[0].stats.station + "/"
        return pre_tag_path
                
    def update_Spectra(self, tags, ds, X, Y, datatype, param, measunit,flag):
        """ 
        Update ASDFDataSet with Response Spectra 
        Args:
            tags(list): list of waveform tags
            ds(ASDFDataSet): ASDF dataset
            X(ndarray): periods
            Y(list): spectral ordinates
            datatype(str): es. "Spectra"
            param(StreamObject): stream
            measunit(str): unit of measure
            flag(int)
        
        Returns:
            updated ASDFDataSet with Response Spectra.
        """
        pre_tag_path = self.pre_tag(param)
        
        if flag == 1:

            for index in range(len(tags)):
                ds.add_auxiliary_data(data=np.vstack((X, Y[index])), data_type=datatype,
                                    path=pre_tag_path + tags[index], parameters={'damping': 0.05,
                                                                                    measunit: param[index].data[
                                                                                        np.argmax(
                                                                                            abs(param[index].data))]})
        else:
            for index in range(len(tags)):
                ds.add_auxiliary_data(data=np.vstack((X[index], Y[index])), data_type=datatype,
                                    path=pre_tag_path + tags[index], parameters = {'measunit': 'cm_s'})
        return ds
        
    def update_ASDF(self, dictn, seconds_to_cut, outfile, inpfile, acceleration, velocity, displacement, periods, Sa,
                    Sd, frequency, FFTamplitude):
        """ 
        Update ASDF with waveforms, spectra, and auxiliary data
        Args:
            dictn(dict): dictionary with processing information.
            seconds_to_cut(typle): startime and endtime in seconds.
            outfile(str): path to output h5 file.
            inpfile(str): path to input h5 file.
            acceleration(StreamObject): acceleration stream
            velocity(StreamObject): velocity stream
            displacement(StreamObject): displacement stream
            periods(ndarray): periods
            Sa(list): spectral ordinates
        Returns:
            updated ASDFDataSet.
        """
        #: update header
        header = self.get_header(self.get_station_ids()[0], self.get_event_ids()[0])
        #: add CV Waveforms
        tags = self.get_tag(self.get_station_ids()[0], self.get_event_ids()[0])
        ds_out = pyasdf.ASDFDataSet(outfile, mode='w')
        
        headers = self.update_header(header, dictn, seconds_to_cut)        
        ds_inp = pyasdf.ASDFDataSet(inpfile, mode='r')
        stream = self.get_stream(self.get_station_ids()[0], self.get_event_ids()[0])
        ds_out, tags = self.update_UncorrectedWaveform(tags, dictn, stream, header, ds_out, '_acc_cv')
        ##: Add event
        ds_out.add_quakeml(ds_inp.events)
        ##: Add StationXML
        station = self.get_station_ids()[0]        
        ds_out, stationXML = self.update_stationXML(station, ds_out)
        ds_out, acc_tags = self.update_CorrectedWaveform(tags, dictn, acceleration, headers, ds_out, '_acc_')
        for index in range(len(headers)):
            acc_tags[index].replace('_cv','_' + (dictn.get('workflow')['modality'] + dictn.get('workflow')['method']).lower())
        #: velocity
        for index in range(len(headers)):
            headers[index] ['units']= "cm/s"
        ds_out, vel_tags = self.update_CorrectedWaveform(tags, dictn, velocity, headers, ds_out, '_vel_')
        #: displacement
        for index in range(len(headers)):
            headers[index] ['units']= "cm"        
        ds_out, dis_tags = self.update_CorrectedWaveform(tags, dictn, displacement, headers, ds_out, '_dis_')
        #: Sa
        ds_out = self.update_Spectra(acc_tags, ds_out, periods, Sa, "Spectra", acceleration, 'pga_cm_s_2',1)
        #: Sd
        ds_out = self.update_Spectra(dis_tags, ds_out, periods, Sd, "Spectra", displacement, 'pgd_cm',1)
         #: FFT
        fft_tags = [acc_tags[0].replace('_acc_','_fft_'),acc_tags[1].replace('_acc_','_fft_'),acc_tags[2].replace('_acc_','_fft_')]
        ds_out = self.update_Spectra(fft_tags, ds_out, frequency, FFTamplitude, "Spectra", acceleration, 'pga_cm_s_2',2)       
        return ds_out

    def get_stats(self, station, event):
        """ 
        Get stats from station and event 
        Args:
            station(str): network and station code. Es. 'IT.CLO'
            event(str): event code.

        Returns:
            list of metadata. 
        """
        tags = self.get_tag(station, event)
        stats = [self.ds.waveforms[station][tags[0]][0].stats, self.ds.waveforms[station][tags[1]][0].stats,
                 self.ds.waveforms[station][tags[2]][0].stats]
        return stats

    def get_starttime(self, station, event):
        """ 
        Get startime from station and event 
        Args:
            station(str): network and station code. Es. 'IT.CLO'
            event(str): event code.

        Returns:
            list of startime. 
        """
        stats = self.get_stats(self.get_station_ids()[0], event)
        startt = []
        for index in range(len(stats)):
            startt.append(stats[index].starttime)
        return startt

    def get_endtime(self, station, event):
        """ 
        Get endtime from station and event 
        Args:
            station(str): network and station code. Es. 'IT.CLO'
            event(str): event code.

        Returns:
            list of endtime. 
        """
        stats = self.get_stats(self.get_station_ids()[0], event)
        endt = []
        for index in range(len(stats)):
            endt.append(stats[index].endtime)
        return endt

    def get_trace(self, station, event):
        """ 
        Get trace from station and event
        Args:
            station(str): network and station code. Es. 'IT.CLO'
            event(str): event code.

        Returns:
            collection of traces. 
        """
        tags = self.get_tag(station, event)
        return [self.ds.waveforms[station][tags[0]][0], self.ds.waveforms[station][tags[1]][0],
                self.ds.waveforms[station][tags[2]][0]]

    def get_stream(self, station, event):
        """ 
        Extract stream for a specific station and event. 
        Args:
            station(str): network and station code. Es. 'IT.CLO'
            event(str): event code.

        Returns:
            collection of streams. 
        """
        tags = self.get_tag(station, event)
        stream = []
        for i in range(len(tags)):
            stream.append(self.ds.waveforms[station][tags[i]])
        return stream
            
    def get_instrumental_frequency(self, station, event):
        """ 
        Get instrumental frequency
        Args:
            station(str): network and station code. Es. 'IT.CLO'
            event(str): event code.

        Returns:
            list of instrumental frequencies. 
        """
        return [self.get_header(station, event)[0]['instrumental_frequency_hz'],
                self.get_header(station, event)[1]['instrumental_frequency_hz'],
                self.get_header(station, event)[2]['instrumental_frequency_hz']]

    def get_instrumental_damping(self, station, event):
        """ 
        Get instrumental damping 
        Args:
            station(str): network and station code. Es. 'IT.CLO'
            event(str): event code.

        Returns:
            list of instrumental damping.
        """
        return [self.get_header(station, event)[0]['instrumental_damping'],
                self.get_header(station, event)[1]['instrumental_damping'],
                self.get_header(station, event)[2]['instrumental_damping']]

    def get_sampling_interval(self, station, event):
        """ 
        Get sampling interval
        Args:
            station(str): network and station code. Es. 'IT.CLO'
            event(str): event code.

        Returns:
            list of sampling intervals.
        """
        return [self.ds.waveforms[station.replace('.', '_')][self.get_tag(station, event)[0]][0].stats.delta,
                self.ds.waveforms[station.replace('.', '_')][self.get_tag(station, event)[1]][0].stats.delta,
                self.ds.waveforms[station.replace('.', '_')][self.get_tag(station, event)[2]][0].stats.delta]

    def is_digital(self, station, event):
        """ 
        Check if the instrument is Digital or Analog 
        Args:
            station(str): network and station code. Es. 'IT.CLO'
            event(str): event code.

        Returns:
            True, False, None.
        """
        result = True
        if self.get_header(station, event)[0]['instrument_analog_digital'] == 'A' and \
                self.get_header(station, event)[1]['instrument_analog_digital'] == 'A' and \
                self.get_header(station, event)[2]['instrument_analog_digital'] == 'A':
            return False
        if self.get_header(station, event)[0]['instrument_analog_digital'] == '' and self.get_header(station, event)[1][
            'instrument_analog_digital'] == '' and self.get_header(station, event)[2][
            'instrument_analog_digital'] == '':
            return None
        return result

    def is_normal_triggered(self, station, event):
        """ 
        Check if late or normal triggered
        Args:
            station(str): network and station code. Es. 'IT.CLO'
            event(str): event code.

        Returns:
            True, False, None.
        """
        result = True
        if self.get_header(station, event)[0]['late_normal_triggered'] == 'LT' and self.get_header(station, event)[1][
            'late_normal_triggered'] == 'LT' and self.get_header(station, event)[2]['late_normal_triggered'] == 'LT':
            return False
        elif self.get_header(station, event)[0]['late_normal_triggered'] == '' and self.get_header(station, event)[1][
            'late_normal_triggered'] == '' and self.get_header(station, event)[2]['late_normal_triggered'] == '':
            return None
        return result

    def is_valid(self):
        """ Check if it is a valid ASDF file """
        try:
            for station in self.get_station_ids():
                for event in self.get_event_ids():
                    self.get_tag(station, event)
            return True
        except AssertionError as e:
            print('is_valid: Exception', e)
            return False

    def UTC2PAT(self, starttime, endtime):
        """ 
        Return startime and endtime
        Args:
            starttime(UTCDateTime): startime
            endtime(UTCDateTime): endtime

        Returns:
            two lists for startime and endtime, respectively.
        """
        pattern = "%4d-%02d-%02dT%02d:%02d:%02d"
        st = [];
        et = []
        for index in range(len(starttime)):
            st.append(pattern % (starttime[index].year, starttime[0].month, starttime[index].day, starttime[index].hour,
                                 starttime[index].minute, starttime[index].second))
            et.append(pattern % (
            endtime[index].year, endtime[index].month, endtime[index].day, endtime[index].hour, endtime[index].minute,
            endtime[index].second))
        return st, et

    def extract_info_tag(self,ds,curr_sta,flag):
        """ 
        Extract stream from dataset for a specific station and flag. 
        Args:
            ds(ASDFDataSet): ASDF dataset
            curr_sta(str): current network and station codes. es. "IV.APRC"
            flag(str): es. "acc_cv"

        Returns:
            stream object.
        """
        for station in ds.waveforms:
            extract_tag=[tag for tag in sorted(station.get_waveform_tags()) if tag[-6:]== flag]
            stream = Stream(traces=[ds.waveforms[curr_sta][extract_tag[0]][0],\
                            ds.waveforms[curr_sta][extract_tag[1]][0],\
                            ds.waveforms[curr_sta][extract_tag[2]][0]])
        return stream

    def check_3c(self, stream):
        """ 
        Check the number of traces 
        Args:
            stream(StreamObject)
        Returns:
            dictionary with warnings
        """
        warning_msg = {'the_number_of_traces_is_equal_to_three': False,'empty_component': False,'zero_component': False}
        if len(stream) == 3:
            warning_msg['the_number_of_traces_is_equal_to_three'] = True
            if stream[0][0].stats.npts == 0 or stream[1][0].stats.npts == 0 or stream[2][0].stats.npts == 0:
                warning_msg['empty_component'] = True
            if sum(stream[0][0].data) == 0 or sum(stream[1][0].data) == 0 or sum(stream[2][0].data) == 0:
                warning_msg['zero_component'] = True
        return warning_msg
        
    def get_dictionary(self, starttime_corr, endtime_corr, workflow):
        """ 
        Return a dictionary
        Args:
            starttime_corr(str): startime
            entime_corr(str): endtime
            workflow(str): type or processing
        Returns:
            dictionary
        """
        station = self.get_station_ids()[0]
        event = self.get_event_ids()[0]
        stats = self.get_stats(self.get_station_ids()[0], event)
        components = stats[0].channel[2] + stats[1].channel[2] + stats[2].channel[2]
        tags = self.get_tag(self.get_station_ids()[0],event)
        location = tags[0].split('_')[0]
        starttime = self.get_starttime(self.get_station_ids()[0], event)
        endtime = self.get_endtime(self.get_station_ids()[0], event)
        starttime, endtime = self.UTC2PAT(starttime, endtime)
        dictionary = {
            "components": str(components.lower()),
            "station_paths": {
                "waveforms": str('waveforms/' + station),
                "headers": str('AuxiliaryData/Headers/' + station.replace('.', '_'))
            },
            "datasets": {
                "unprocessed_acceleration": {
                    "u": str(station + '.' + location + '.' + stats[0].channel + '__' + starttime[0] + '__' + endtime[
                        0] + '__' + tags[0]),
                    "v": str(station + '.' + location + '.' + stats[1].channel + '__' + starttime[1] + '__' + endtime[
                        1] + '__' + tags[1]),
                    "w": str(station + '.' + location + '.' + stats[2].channel + '__' + starttime[2] + '__' + endtime[
                        2] + '__' + tags[2])
                },
                "acceleration": {
                    "u": str(station + '.' + location + '.' + stats[0].channel + '__' + starttime_corr + '__' +
                             endtime_corr + '__' + tags[0].replace('_acc_cv', '_acc_' + workflow.lower())),
                    "v": str(station + '.' + location + '.' + stats[1].channel + '__' + starttime_corr + '__' +
                             endtime_corr + '__' + tags[1].replace('_acc_cv', '_acc_' + workflow.lower())),
                    "w": str(station + '.' + location + '.' + stats[2].channel + '__' + starttime_corr + '__' +
                             endtime_corr + '__' + tags[2].replace('_acc_cv', '_acc_' + workflow.lower()))
                },
                "velocity": {
                    "u": str(station + '.' + location + '.' + stats[0].channel + '__' + starttime_corr + '__' +
                             endtime_corr + '__' + tags[0].replace('_acc_cv', '_vel_' + workflow.lower())),
                    "v": str(station + '.' + location + '.' + stats[1].channel + '__' + starttime_corr + '__' +
                             endtime_corr + '__' + tags[1].replace('_acc_cv', '_vel_' + workflow.lower())),
                    "w": str(station + '.' + location + '.' + stats[2].channel + '__' + starttime_corr + '__' +
                             endtime_corr + '__' + tags[2].replace('_acc_cv', '_vel_' + workflow.lower()))
                },
                "displacement": {
                    "u": str(station + '.' + location + '.' + stats[0].channel + '__' + starttime_corr + '__' +
                             endtime_corr + '__' + tags[0].replace('_acc_cv', '_dis_' + workflow.lower())),
                    "v": str(station + '.' + location + '.' + stats[1].channel + '__' + starttime_corr + '__' +
                             endtime_corr + '__' + tags[1].replace('_acc_cv', '_dis_' + workflow.lower())),
                    "w": str(station + '.' + location + '.' + stats[2].channel + '__' + starttime_corr + '__' +
                             endtime_corr + '__' + tags[2].replace('_acc_cv', '_dis_' + workflow.lower()))
                }
            }
        }
        return dictionary

#if __name__ == "__main__":
#  asdf_file = ASDFFile(pathname)

