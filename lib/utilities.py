import os
import json
from obspy.core import Stream,Trace
from scipy import optimize,stats
import numpy as np 
from copy import deepcopy 
from obspy.signal.invsim import cosine_taper as cosTaper 
from obspy.signal.filter import lowpass 
from obspy.signal.trigger import ar_pick
import pathlib

from lib.dictionary import JSON


def ProcessingOutput(base_output_path,filename,dictn, errors, ptype, asdf_file, seconds_to_cut, _acceleration,_velocity, _displacement, periods, Sa, Sd, frequency, FFTamplitude):
    """
    Data processing 

    Args:
        base_output_path(str): path to output directory
        filename(str): input filename of h5 file
        dictn(dict): dictionary with the processing workflow
        errors(dict): dictionary with errors
        ptype(str): type of processing es. "AP"
        asdf_file(ASDFFile): ASDF container
        seconds_to_cut(tuple): startime and endtime in seconds for waveform trimming
        _acceleration(StreamObject): acceleration stream
        _velocity(StreamObject): velocity stream
        _displacement(StreamObject): displacement stream
        periods(ndarray): periods of response spectra
        Sa(list): response spectra in acceleration
        Sd(list): response spectra in displacement
        frequency(list): frequencies of Fourier Spectra
        FFTamplitude(list): Fourier spectra of the 3 components
    Returns:
        a tuple. If the processing succeeds it contains the filename of the JSON file with processing info and "None". 
        If the processing does not succeed it contains the filename of h5 file and errors.
    """
 
    if len(errors) == 0:
        filename_h5_pro = '/h5/' + filename.split('/')[-1].replace('.h5', '_' + ptype + '.h5' )
        filename_json_pro =  '/json/' + filename.split('/')[-1].replace('.h5','_' + 'settings_' + ptype + '.json')
        #: to save ASDF with waveforms, spectra, and auxiliary data       
        try:
            asdf_file.update_ASDF(dictn, seconds_to_cut, base_output_path + '/' + filename_h5_pro, filename, _acceleration,_velocity, _displacement, periods, Sa, Sd, frequency, FFTamplitude)
        except Exception as e:
            print('UpdatingASDF', str(e))
            errors.update({'UpdatingASDF', str(e)})
       
       #: to save json with processing info
        try:
            dict_new = {}
            dict_new.update(dictn['settings'])
            dict_new.update(dictn['workflow'])
            update_dictionary =  JSON(base_output_path, filename_json_pro)
            update_dictionary.makeJSON(dict_new,2)
        except Exception as e:
            print('makeJSON settings', str(e))
            errors.update({'makeJSON settings' : str(e)})    
        return filename_json_pro, None
    else:
        #: to save json with error messages
        try:
            update_dictionary = JSON(base_output_path, '/json/' + 'error_message')
            update_dictionary.makeJSON(errors,2)
        except Exception as e:
            print('makeJSON errors', str(e))
            errors.update({'makeJSON errors', str(e)})
            
        return filename_h5_pro, errors


def create_input_dir(input_path):
    """
    Check if the input directory exists
    Args:
        file_path(str): path to a specific directory
    Retruns:
        create the input directory if it does not exist
    """
    directory = os.path.dirname(input_path)
    if not os.path.exists(directory):
        os.makedirs(directory)


def create_output_dirs(output_path):
    """
    Create the output directories
    Args:
        output_path(str): output path
    Retruns:
        create all the output directory and subdirectories
    """
    pathlib.Path(output_path).mkdir(parents=True, exist_ok=True)
    pathlib.Path(output_path + "/fig").mkdir(parents=True, exist_ok=True)
    pathlib.Path(output_path + "/h5").mkdir(parents=True, exist_ok=True)
    pathlib.Path(output_path + "/json").mkdir(parents=True, exist_ok=True)


def PS_TravelTimes(origindepth, origintime, distance):
    """
    Provide theoretical P and S wave arrivals based on Travel Times

    Args:
        origindepth(float): event depth
        origintime(UTCDateTime): event origin time
        distance(str): epicentral distance
    Returns:
        P and S wave arrivals in UTCDatetime
    """
    if float(origindepth) > 30:
        arrivaltimeS = (float(origindepth)-30.0)/4.5 + ((30.0**2.0 + float(distance)**2.0)**0.5)/3.2
        arrivaltimeP = (float(origindepth)-30.0)/8.0 + ((30.0**2.0 + float(distance)**2.0)**0.5)/5.7
    else:
        arrivaltimeS = ((float(origindepth)**2.0 + float(distance)**2.0)**0.5)/3.2
        arrivaltimeP = ((float(origindepth)**2.0 + float(distance)**2.0)**0.5)/5.7

    timeP = origintime + arrivaltimeP
    timeS = origintime + arrivaltimeS
    
    return timeP, timeS


def SignalToNoiseWindows(stream,NW1,NW2,SW1,SW2,p_pick,s_pick):
    """
    Finds the signal and noise windows for SNR in time domain(TD).

    Args:
        NW1(NoneType): user defined startime of noise window
        NW2(NoneType): user defined endtime of noise window
        SW1(NoneType): user defined startime of signal window
        SW2(NoneType): user defined endtime of signal window
        p_pick(float): P-wave arrival derived from automatic picking
        s_pick(float): S-wave arrival derived from automatic picking
    Returns:
        the startime and endtime of the signal and noise windows
    """
    if NW1 is not None and NW2 is not None and SW1 is not None and SW2 is not None:
        pass
    else:
        t0 = stream[0].stats['starttime']
        tend = stream[0].stats['endtime']
        noise_window = [tend-t0-4, tend-t0]\
            if (p_pick-4)<0 else [p_pick-4, p_pick]
        NW1 = noise_window[0]
        NW2 = noise_window[1]
        SW1 = s_pick
        SW2 = SW1 + (NW2 - NW1)
    
    return SW1,SW2,NW1,NW2


def SNR(NW_traces,SW_traces):
    """
    Compute the quality index from SNR in time domain.

        Args:
            noise(StreamObject): noise stream
            signal(StreamObject): signal stream
        Returns:
            a number for the quality index.
    """
    E_SW_RMS = np.sqrt(np.sum(np.square(SW_traces[0].data))/SW_traces[0].stats.npts)
    N_SW_RMS = np.sqrt(np.sum(np.square(SW_traces[1].data))/SW_traces[1].stats.npts)
    Z_SW_RMS = np.sqrt(np.sum(np.square(SW_traces[2].data))/SW_traces[2].stats.npts)
    E_NW_RMS = np.sqrt(np.sum(np.square(NW_traces[0].data))/NW_traces[0].stats.npts)
    N_NW_RMS = np.sqrt(np.sum(np.square(NW_traces[1].data))/NW_traces[1].stats.npts)
    Z_NW_RMS = np.sqrt(np.sum(np.square(NW_traces[2].data))/NW_traces[2].stats.npts)
    SNR_E = 10*np.log10(E_SW_RMS/E_NW_RMS)
    SNR_N = 10*np.log10(N_SW_RMS/N_NW_RMS)
    SNR_Z = 10*np.log10(Z_SW_RMS/Z_NW_RMS)

    return np.mean([SNR_E, SNR_N, SNR_Z])


def PS_auto(stream):
    """
    Compute the automatic picking
    Args:
        stream(StreamObject): stream to analyze
    Returns:
        P and S wave arrivals in seconds from waveform startime
    """
    df = stream[0].stats.sampling_rate
    p_pick,s_pick = ar_pick(stream[2].data, stream[1].data, stream[0].data, df,\
                     1.0, 20.0, 2.0, 0.5, 2.0, 1.0, 2, 8, 0.4, 0.4)
    return p_pick,s_pick


def ArraytoTrace(stream,amplitudes):
    """
    Convert ndarray to Trace

    Args:
        stream(StreamObject)
        amplitudes(list)
    Returns:
        list of traces
    """
    trace1 = Trace(amplitudes[0],stream[0].stats)
    trace2 = Trace(amplitudes[1],stream[1].stats)
    trace3 = Trace(amplitudes[2],stream[2].stats)
    trace = [trace1,trace2,trace3]
    return trace


def SecondToCut(starttime,endtime,wfs_stream):
    """
    Provide startime and endtime for waveform trimming in seconds from the startime of the unprocessed waveform
    Args:
        starttime(UTCDateTime)
        endtime(UTCDateTime)
        wfs_stream(StreamObject)
    Returns:
        two lists, one for startime the other for endtime. Both are in seconds from the startime of the unprocessed waveform
    """
    try:
        sec_from_beginning = [starttime-wfs_stream[0][0].stats.starttime,starttime-wfs_stream[1][0].stats.starttime,starttime-wfs_stream[2][0].stats.starttime]
        sec_from_end = [wfs_stream[0][0].stats.endtime-endtime,wfs_stream[1][0].stats.endtime-endtime,wfs_stream[2][0].stats.endtime-endtime]
    except Exception:
        pass
    try:
        sec_from_beginning = [starttime-wfs_stream[0].stats.starttime,starttime-wfs_stream[1].stats.starttime,starttime-wfs_stream[2].stats.starttime]
        sec_from_end = [wfs_stream[0].stats.endtime-endtime,wfs_stream[1].stats.endtime-endtime,wfs_stream[2].stats.endtime-endtime]
    except Exception:
        pass
    return sec_from_beginning,sec_from_end    


def WaveformNpts(wfs_stream):
    """ 
    Provide the Number of sample points
    Args:
        wfs_stream(StreamObject)
    Returns:
        list with the Number of sample points for each component
    """
    return [wfs_stream[0][0].stats.npts,wfs_stream[1][0].stats.npts,wfs_stream[2][0].stats.npts]    


def WaveformsDelta(wfs_stream):
    """ 
    Provide the Sample distance in seconds in PAO11
    Args:
        wfs_stream(StreamObject)
    Returns:
        list with the Sample distance in seconds for each component
    """
    return [wfs_stream[0][0].stats.delta,wfs_stream[1][0].stats.delta,wfs_stream[2][0].stats.delta]


def WaveformsDelta2(wfs_stream):
    """ 
    Provide the Sample distance in seconds in eBASCO
    Args:
        wfs_stream(StreamObject)
    Returns:
        list with the Sample distance in seconds for each component
    """
    return [wfs_stream[0].stats.delta,wfs_stream[1].stats.delta,wfs_stream[2].stats.delta]                


def IdentifyNormalLate(stream):
    """ 
    Identify if Normal or Late Triggered record
    Args:
        stream(StreamObject)
    Returns:
        list with the ratio between Arias Intensity at 5% (i1) and 95% (i2), i1, i2, and significant duration
    """
    I1=0.05; I2=0.95; late_trigger_threshold = 0.05
    output1 = WFPartition(stream[0].data,stream[0].stats.delta,I1, I2)
    output2 = WFPartition(stream[1].data,stream[1].stats.delta,I1, I2)
    D1_E = output1[0]; D2_E = output1[1]; D3_E = output1[2]
    D1_N = output2[0]; D2_N = output2[1]; D3_N = output2[2]
    D1 = min(D1_N, D1_E)
    if D1 == D1_N: D2 = D2_N; D3 = D3_N
    elif D1 == D1_E: D2 = D2_E; D3 = D3_E
    r1 = float(D1)/float(D2)
    return r1,D1,D2,D3


def WFPartition(w_f, d_t, i1, i2):
    """ 
    Compute the significant duration from the Normalized Arias Intensity between i1 and i2
    Args:
        w_f(ndarray): waveform
        d_t(float): sample distance in seconds
        i1(float): first sample of the Normalized Arias Intensity es. 0.05
        i2(float): second sample of the Normalized Arias Intensity es. 0.95
    Returns:
        time in seconds at i1 and i2, besides the temporal duration between these two points
    """
    N = len(w_f)
    Ia = np.zeros(N)
    for i in range(0,N-1):
        Ia[i+1] = Ia[i] + w_f[i+1]**2
    Ia = Ia/Ia[-1]
    idx1 = (abs(Ia-i1)).argmin()
    idx2 = (abs(Ia-i2)).argmin()
    return idx1*d_t,(idx2)*d_t,(idx2-idx1)*d_t


def wf_Partition(stream, d_t, i1, i2):
    """ 
    Compute the significant duration from the Normalized Arias Intensity between i1 and i2
    Args:
        stream(StreamObject): stream to be analyzed
        d_t(float): sample distance in seconds
        i1(float): first sample of the Normalized Arias Intensity es. 0.05
        i2(float): second sample of the Normalized Arias Intensity es. 0.95
    Returns:
        ndarray with time in seconds at i1 and i2, besides the temporal duration between these two points
    """
    out = np.empty((0,4), float)
    for w_f in stream:
        t0 = w_f.stats['starttime']
        #print(t0)
        N = len(w_f)
        Ia = np.zeros(N)
        for i in range(0,N-1):
            Ia[i+1] = Ia[i] + w_f[i+1]**2
        Ia = Ia/Ia[-1]
        idx1 = (abs(Ia-i1)).argmin()
        idx2 = (abs(Ia-i2)).argmin()
        out = np.append(out,np.array([[t0+idx1*d_t,t0+(idx2)*d_t,(idx2-idx1)*d_t,Ia]], dtype=object), axis=0)
    return out   


def SampleCorrectionPoints (stream,samplenum,prctg1,prctg2):
    """ 
    Sample the time between prctg1 and prctg2 of the Normalized Arias Intensity
    Args:
        stream(StreamObject): stream to be analyzed
        samplenum(int): number of samples
        prctg1(float): first point of the Normalized Arias Intensity es. 0.05
        prctg2(float): second point of the Normalized Arias Intensity es. 0.95
    Returns:
        list with the samples
    """
    T_sample = []
    for i in range(len(stream)):
        T_range = WFPartition(stream[i].data, stream[i].stats.delta, prctg1, prctg2)
        T_sample.append(np.logspace(np.log10(T_range[0]),np.log10(T_range[1]),samplenum, endpoint=False, base=10.0))
    return T_sample


def FindNearest(array, value):
    """
    Find the element of the array closest to the input value

    Args:
        array(ndarray)
        value(float)
    Returns:
        the index of the array element closest to the input value
    """
    array = np.asarray(array)
    idx = (abs(array - value)).argmin()
    return idx


def time(stream):
    """
    Return the temporal axis of the time series

    Args:
        stream(StreamObject)
    Returns:
        a list with the temporal axis of the 3 components
    """
    delta = [stream[0].stats.delta,stream[1].stats.delta,stream[2].stats.delta]   
    return [np.arange(0,len(stream[0])*delta[0],delta[0]),np.arange(0,len(stream[1])*delta[1],delta[1]),np.arange(0,len(stream[2])*delta[2],delta[2])]


def PreEvent(stream,T1arr):
    """
    Define the "pre-event" (in velocity) between the first sample T0 and T1

    Args:
        stream(StreamObject)
        T1arr(list): output of SampleCorrectionPoints
    Returns:
        lists with time and list with velocity values
    """
    times = time(stream)
    _time_pre = []
    _vel_pre = []
    for i in range(len(stream)):
        time_pre = []
        vel_pre = []
        for T1 in T1arr[i]:
            index = FindNearest(times[i], T1)
            time_pre.append(times[i][0:index])
            vel_pre.append(stream[i][0:index])
        _time_pre.append(np.array(time_pre, dtype=object))
        _vel_pre.append(np.array(vel_pre, dtype=object))
    return _time_pre,_vel_pre


def FindSolutions(acceleration_corrected,delta):
    """
    List of possible solutions for eBASCO method

    Args:
        acceleration_corrected(list): list with corrected acceleration
        delta(list): list with the sample distance in seconds
    Returns:
        lists with corrected velocity and displacement
    """
    vel_corr_good = []
    disp_corr_good = []
    for i in range(len(acceleration_corrected)):
        output1,output2 = AcceptSolution(acceleration_corrected[i],delta[i])
        vel_corr_good.append(output1)
        disp_corr_good.append(output2)
    return vel_corr_good,disp_corr_good


def func_pre_fit(x, a):       
    return a*x


def DetrendPreEvent(PRE_TIME,PRE_VEL):
    """
    Detrend pre event in eBASCO method

    Args:
        PRE_TIME(list): list with time
        PRE_VEL(list): list with velocity values
    Returns:
        ndarray with detrended pre event
    """
    _PRE_EVE_VEL_DET = []
    _LAST_VAL_FIT = []
    _FIT = []
    for i in range(len(PRE_TIME)):  
        PRE_EVE_VEL_DET = []
        LAST_VAL_FIT = []
        FIT = [] 
        for ind in range(len(PRE_VEL[i])):
            popt1,pcov1 = optimize.curve_fit(func_pre_fit,PRE_TIME[i][ind],PRE_VEL[i][ind])  
            PRE_EVE_VEL_DET.append(PRE_VEL[i][ind]- func_pre_fit(PRE_TIME[i][ind],popt1))
            LAST_VAL_FIT.append(func_pre_fit(PRE_TIME[i][ind],popt1)[-1])
            FIT.append(func_pre_fit(PRE_TIME[i][ind],popt1))
        _PRE_EVE_VEL_DET.append(np.array(PRE_EVE_VEL_DET, dtype=object))
        _LAST_VAL_FIT.append(np.array(LAST_VAL_FIT))
        _FIT.append(np.array(FIT, dtype=object))
    return np.array(_PRE_EVE_VEL_DET),np.array(_LAST_VAL_FIT),np.array(_FIT)


def func_post_fit(x, b, c):       
    return b*x +c


def DetrendPostEvent(post_time,post_vel):
    """
    Detrend post event in eBASCO method

    Args:
        post_time(list): list with time
         post_vel(list): list with velocity values
    Returns:
        list with detrended post event and fit
    """
    post_eve_vel_det = []
    fit = []
    for i in range(len(post_time)):
        popt,pcov = optimize.curve_fit(func_post_fit,post_time[i],post_vel[i])  
        post_eve_vel_det.append(post_vel[i]- func_post_fit(post_time[i],popt[0],popt[1]))  
        fit.append(func_post_fit(post_time[i],popt[0],popt[1]))
    return post_eve_vel_det,fit


def EndPoint (stream):
    """
    Search the end points for each component in eBASCO method

    Args:
        stream(StreamObject)
    Returns:
        list of times for each last sample of each component
    """
    delta = [stream[0].stats.delta,stream[1].stats.delta,stream[2].stats.delta]
    time_1 = np.arange(0,len(stream[0])*delta[0],delta[0]) 
    time_2 = np.arange(0,len(stream[1])*delta[1],delta[1])
    time_3 = np.arange(0,len(stream[2])*delta[2],delta[2])
    end_point = [time_1[-1], time_2[-1], time_3 [-1]]
    return end_point


def T2Sample(t3_samples,end_points,ind,opts_t2):
    """
    Sample T2 time from T3 and the end of the signal (eBASCO method)

    Args:
        t3_samples(list): samples of T3 points
        end_points(list): samples of end points
        ind(int): index of possible solution
        opts_t2(int): 
    Returns:
        ndarray that Sample T2 time for each component
    """
    T2_SAMP_E = []
    T2_SAMP_N = []
    T2_SAMP_Z = []
    
    T2_SAMP_E=np.logspace(np.log10(t3_samples[0][ind]),np.log10(end_points[0]), opts_t2, endpoint=False, base=10.0)
    T2_SAMP_N=np.logspace(np.log10(t3_samples[1][ind]),np.log10(end_points[1]), opts_t2, endpoint=False, base=10.0)       
    T2_SAMP_Z=np.logspace(np.log10(t3_samples[2][ind]),np.log10(end_points[2]), opts_t2, endpoint=False, base=10.0)
    return T2_SAMP_E, T2_SAMP_N, T2_SAMP_Z


def PostEve(stream,T2arr,end_points,index_T2):
    """
    Define the "post event" between T2 and the end of the signal on velocity (eBASCO method)

    Args:
        stream(StreamObject):
        T2arr(tuple):
        end_points(list): list of end points
        index_T2(int): index of T2
    Returns:
        lists with time and list with velocity values
    """
    times = time(stream)
    post_ev_time = []
    post_ev_vel = []
    for i in range(len(stream)):
        post_ev_time.append(times[i][FindNearest(times[i], T2arr[i][index_T2]):FindNearest(times[i], end_points[i])])
        post_ev_vel.append(stream[i][FindNearest(times[i], T2arr[i][index_T2]):FindNearest(times[i], end_points[i])])
    return post_ev_time, post_ev_vel


def StrongEve(stream,T1,T2,ind_1,ind_2):
    """
    Define the "strong-event" between T1 and T2 on velocity (eBASCO method)

    Args:
        stream(StreamObject):
        T1(list):
        T2(list): 
        ind_1(int):
        ind_2(int):
    Returns:
        lists with time and list with velocity values
    """ 
    times = time(stream)
    strong_ev_time = []
    strong_ev_vel = []
    for i in range(len(stream)):
        strong_ev_time.append(times[i][FindNearest(times[i], T1[i][ind_1]):FindNearest(times[i], T2[i][ind_2])])
        strong_ev_vel.append(stream[i][FindNearest(times[i], T1[i][ind_1]):FindNearest(times[i], T2[i][ind_2])])
    return strong_ev_time, strong_ev_vel


def AM(post_eve_lin_fit,last_value_lin_fit,T1_sample,T2_samples,index_T1,index_T2):
    """
    Calculate the slope Am of the line between V(T1) and V(T2); V(T1) 
    and V(T2) are the last point of the detrending line in the "pre-event" 
    and the first point of the detrending line in the "post-event", respectively
    (eBASCO method)

    Args:
        post_eve_lin_fit(list):
        last_value_lin_fit(ndarray):
        T1_sample(list): 
        T2_samples(tuple):
        index_T1(int):
        index_T2(int):
    Returns:
        a list with the slope Am
    """ 
    Am = []
    for i in range(len(post_eve_lin_fit)):
        Am.append(float((post_eve_lin_fit[i][0]-last_value_lin_fit[i][index_T1])/(T2_samples[i][index_T2]-T1_sample[i][index_T1])))
    return Am


def OrdinateIntercept(last_value_lin_fit,index_T1,Am,t1_samples):
    """    
    Calculate the ordinate intercept of the line between V(T1) and V(T2)
    
    Args:
        last_value_lin_fit(ndarray):
        index_T1(int):
        Am(list):
        t1_samples(list):
    Returns:
        a list with the the ordinate intercept
    """
    q = []
    for i in range(len(Am)):
        q.append(last_value_lin_fit[i][index_T1] - Am[i]*t1_samples[i][index_T1])
    return q


def DetrendStrongEvent(strong_vel,strong_eve_line):
    """
    Detrend strong event in eBASCO method

    Args:
        strong_vel(list): 
        strong_eve_line(list): 
    Returns:
        list with detrended strong event velocity
    """
    strong_eve_vel_det = []
    for i in range(len(strong_vel)):
        strong_eve_vel_det.append(strong_vel[i]- strong_eve_line[i])
    return strong_eve_vel_det


def concatenate(pre_eve_time,index_T1,strong_phase_time,post_eve_time,pre_eve_vel_det,strong_phase_vel_det,post_eve_vel_det):
    """
    Concatenate the trilinear detrended velocity trace (time windows start-T1, T1-T2, T3-Tend) in eBASCO method

    Args:
        pre_eve_time(list): 
        index_T1(int):
        strong_phase_time(list):
        post_eve_time(list):
        pre_eve_vel_det(ndarray):
        strong_phase_vel_det(list):
        post_eve_vel_det(list):
    Returns:
        lists with time and list with velocity values
    """
    time=[]; vel_corr=[]
    for i in range (len(pre_eve_time)):
        time.append(np.concatenate((pre_eve_time[i][index_T1],strong_phase_time[i],post_eve_time[i][1:]),axis=0))
        vel_corr.append(np.concatenate((pre_eve_vel_det[i][index_T1],strong_phase_vel_det[i],post_eve_vel_det[i][1:]),axis=0))       
    return time,vel_corr


def eBASCOvelocity(stream,t1_sample,t2_sample_num,t3_sample,end_point,last_value_lin_fit,pre_eve_time,pre_eve_vel_det):
    """
    Compute the corrected velocity for eBASCO method

    Args:
        stream(StreamObject):
        t1_sample(list):
        t2_sample_num(int):
        t3_sample(list):
        end_point(list):
        last_value_lin_fit(ndarray):
        pre_eve_time(list): 
        pre_eve_vel_det(ndarray):
    Returns:
        time windows start-T1, T1-T2, T3-Tend, besides time and corrected velocity
    """    
    T1_1 = []; T2_1 = []; T3_1 = []; T1_2 = []; T2_2 = []; T3_2 = []; T1_3 = []; T2_3 = []; T3_3 = []; time = []; vel_corr = []
    for index_T1 in range(len(t1_sample[0])): 
        for index_T3 in range(len(t3_sample[0])):
                                            
            #: sample T2 time from each T3 and the end of the signal
            t2_sample = T2Sample(t3_sample,end_point,index_T3,t2_sample_num)
            for index_T2 in range(len(t2_sample[0])):                             
                #: define the "post event" between T2 and the end of the signal (velocity)
                [post_eve_time, post_eve_vel] = PostEve(stream,t2_sample,end_point,index_T2) 
                
                #: linear de-trend between T2 and the end of the signal (velocity)
                [post_eve_vel_det, post_eve_lin_fit] = DetrendPostEvent (post_eve_time,post_eve_vel)
                
                #: define the "strong-event" between T1 and T2 (velocity)                   
                [strong_phase_time, strong_phase_vel] = StrongEve(stream,t1_sample,t2_sample,index_T1,index_T2)
                #print(post_eve_lin_fit[0])
                
                #: calculate the slope Am of the line between V(T1) and V(T2); V(T1) and V(T2) are the last point of the detrending line in the "pre-event" and the first point of the detrending line in the "post-event", respectively
                Am = AM(post_eve_lin_fit,last_value_lin_fit,t1_sample,t2_sample,index_T1,index_T2)
                
                #: calculate the ordinate intercept of the line between V(T1) and V(T2)
                q = OrdinateIntercept(last_value_lin_fit,index_T1,Am,t1_sample)
                        
                def retta(x,m,q):
                        return m*x + q
                
                #: define the detrending line between V(T1) and V(T2)                        
                strong_phase_line = [retta(strong_phase_time[0],Am[0],q[0]), retta(strong_phase_time[1],Am[1],q[1]), retta(strong_phase_time[2],Am[2],q[2])]
                
                #: linear de-trend in the "strong-event" 
                strong_phase_vel_det = DetrendStrongEvent(strong_phase_vel,strong_phase_line)
                
                # concatenate the trilinear detrended velocity trace (time windows start-T1, T1-T2, T3-Tend)
                _time,_vel_corr = concatenate(pre_eve_time,index_T1,strong_phase_time,post_eve_time,pre_eve_vel_det,strong_phase_vel_det,post_eve_vel_det)
                time.append(_time)
                vel_corr.append(_vel_corr)
                
                T1_1.append(t1_sample[0][index_T1]); T1_2.append(t1_sample[1][index_T1]); T1_3.append(t1_sample[2][index_T1])
                T2_1.append(t2_sample[0][index_T2]); T2_2.append(t2_sample[1][index_T2]); T2_3.append(t2_sample[2][index_T2])
                T3_1.append(t3_sample[0][index_T3]); T3_2.append(t3_sample[1][index_T3]); T3_3.append(t3_sample[2][index_T3])
                
        T1 = [T1_1,T1_2,T1_3]; T2 = [T2_1,T2_2,T2_3]; T3 = [T3_1,T3_2,T3_3]       
    return T1, T2, T3, time, vel_corr


def AccCorr (uncorrected_acceleration,vel_corr,time,t1,t2,t3,eps):
    """
    Correct the acceleration for eBASCO method

    Args:
        uncorrected_acceleration(StreamObject): stream with uncorrected acceleration
        vel_corr(list): corrected velocity 
        time(list):
        t1(list):
        t2(list):
        t3(list):
        eps(float): 
    Returns:
        corrected acceleration
    """ 
    acceleration_corrected = []; acceleration_bad = []; t3_ok = []; index_ok = []; time_ok = []
    delta = [uncorrected_acceleration[0].stats.delta,uncorrected_acceleration[1].stats.delta,uncorrected_acceleration[2].stats.delta]
    for j in range(len(uncorrected_acceleration)):
        _acceleration_corrected = []; _t3_ok = []; _index_ok = []; _time_ok = []; _index_ok = []
        for i in range(len(vel_corr)):
            
            index_1 = FindNearest(time[i][j], t1[j][i])
            pga_uncorrected_t1 = uncorrected_acceleration[j].data[index_1]
            acceleration = np.gradient(vel_corr[i][j],delta[j])
            pga_corrected_t1 = acceleration[index_1]
            index_2 = FindNearest(time[i][j], t2[j][i])
            pga_uncorrected_t2 = uncorrected_acceleration[j].data[index_2]
            pga_corrected_t2 = acceleration[index_2]
            p_var_t1 = abs(pga_corrected_t1-pga_uncorrected_t1)/abs(pga_uncorrected_t1)
            p_var_t2 = abs(pga_corrected_t2-pga_uncorrected_t2)/abs(pga_uncorrected_t2)
            if p_var_t1 < eps and p_var_t2 < eps:
                _acceleration_corrected.append(acceleration)
                _t3_ok.append(t3[j][i])
                _index_ok.append(i)
                _time_ok.append(time[i][j])
        index_ok.append(_index_ok)
        acceleration_corrected.append(_acceleration_corrected)
        time_ok.append(_time_ok)
        t3_ok.append(_t3_ok)
        
    return acceleration_corrected,time_ok,t3_ok,index_ok,delta


def FindBestSolution(acceleration_corrected,time_ok,disp_corr_good,t3_ok,delta,taperleft,filterorder,HighCut,T1,T2,T3):
    """
    Identify the best solution througth the flatness indicator in eBASCO

    Args:
        acceleration_corrected(list): stream with uncorrected acceleration
        time_ok(list): corrected velocity 
        disp_corr_good(list):
        t3_ok(list):
        delta(list):
        taperleft(int):
        filterorder(int):
        HighCut(list):
        T1(list):
        T2(list):
        T3(list):
    Returns:
        different list for acceleration, velocity and displacement, besides list for T1, T2, and T3
    """
    # to calculate the flatness indicator
    # ----------------------------------#
    # r = linear correlation coefficient of the samples between T3 and the end of the signal --> -1<= r <=1   if |r|=1 good linear covariance
    # b = slope of the regression line between T3 and the end of the signal --> 0 when the [T3:end] signal is costant
    # sigma = variance of the mean value of the samples between T3 and the end of the signal --> sigma-->0 means that the samples are poor scattered around the mean value
    # f_value = |r|/(|b|*sigma) is the flatness indicator  f--> high value when the signal between T3 and the end of the signal approssimates a line parallel to the time axes
    #----------------------------------#
    
    st_1_dis=[-999]; st_2_dis=[-999]; st_3_dis=[-999]
    
    if len(acceleration_corrected[0])>0: f_value_1 = []; f_value_1 = fvalue(time_ok[0],disp_corr_good[0],t3_ok[0]); num_1 = np.argmax(f_value_1);[st_1_acc, st_1_vel, st_1_dis] = FinalTrace(acceleration_corrected[0],num_1,taperleft,delta[0],filterorder,HighCut[0]); print("f value 1st component: ",format(f_value_1[num_1],'.0f')); T1_1_good = T1[0][num_1]; T2_1_good = T2[0][num_1]; T3_1_good = T3[0][num_1]
    else: num_1 = -999
    
    if len(acceleration_corrected[1])>0: f_value_2 = []; f_value_2 = fvalue(time_ok[1],disp_corr_good[1],t3_ok[1]); num_2 = np.argmax(f_value_2);[st_2_acc, st_2_vel, st_2_dis] = FinalTrace(acceleration_corrected[1],num_2,taperleft,delta[1],filterorder,HighCut[1]); print("f value 2nd component: ",format(f_value_2[num_2],'.0f')); T1_2_good = T1[1][num_2]; T2_2_good = T2[1][num_2]; T3_2_good = T3[1][num_2]
    else: num_2 = -999
    
    if len(acceleration_corrected[2])>0: f_value_3 = []; f_value_3 = fvalue(time_ok[2],disp_corr_good[2],t3_ok[2]); num_3 = np.argmax(f_value_3);[st_3_acc, st_3_vel, st_3_dis] = FinalTrace(acceleration_corrected[2],num_3,taperleft,delta[2],filterorder,HighCut[2]); print("f value 3th component: ",format(f_value_3[num_3],'.0f')); T1_3_good = T1[2][num_3]; T2_3_good = T2[2][num_3]; T3_3_good = T3[2][num_3]
    else: num_3 = -999
    
    
    T1_good = [T1_1_good,T1_2_good,T1_3_good]
    T2_good = [T2_1_good,T2_2_good,T2_3_good]
    T3_good = [T3_1_good,T3_2_good,T3_3_good]
    st_acc = [st_1_acc,st_2_acc,st_3_acc]
    st_vel = [st_1_vel,st_2_vel,st_3_vel]
    st_dis = [st_1_dis,st_2_dis,st_3_dis]
    
    print("---------------------------------------------") 
    print("")

    if (num_1 == -999 and num_2==-999) and num_3==-999:
        raise Exception ("there are no acceptable solutions, please try to change default options!")
    else:
        #--------------------------------------------
        # READ THE PERMANET DISPLACEMENT
        #--------------------------------------------            
        print("")
        print("------------PERMANENT DISPLACEMENT--------------") 
        pd_1 = st_dis[0][-1]; pd_2 = st_dis[1][-1]; pd_3 = st_dis[2][-1]
        print("permanent displacement 1st comp [cm]: ",format(pd_1,'.2f'))
        print("permanent displacement 2nd comp [cm]: ",format(pd_2,'.2f'))
        print("permanent displacement 3th comp [cm]: ",format(pd_3,'.2f'))
        print("---------------------------------------------") 
        print("")
    return st_acc,st_vel,st_dis,T1_good,T2_good,T3_good


def ArrayToStream(array,stream):
    """
    Include the array data into stream object.

    Args:
        array(ndarray): 
        stream(StreamObject): 
    Returns:
        updated stream
    """
    head1=stream[0].stats
    head2=stream[1].stats
    head3=stream[2].stats
    head1.npts = len(array[0])
    head2.npts = len(array[1])
    head3.npts = len(array[2])
    trace1 = Trace(array[0],head1)
    trace2 = Trace(array[1],head2)
    trace3 = Trace(array[2],head3)
    stream = Stream(traces=[trace1,trace2,trace3])    
    return(stream)


def fvalue (time,amp,t3):
    """
    Compute the f_value, which is a flatness indicator for eBASCO processing: f_value = |r|/(|b|*sigma)

    Args:
        time(list):
        amp(list): 
        t3(list):
    Returns:
        a list of f values
    """
    f_value = [];
    for num in range(len(t3)):
        
        ind_T3 = FindNearest(time[num],t3[num])
        slope, intercept, r_value, p_value, std_err = stats.linregress(time[num][ind_T3:-1],amp[num][ind_T3:-1])
        sigma = std_err**2
        f_value.append(abs(r_value)/(abs(slope)*sigma))
    return f_value 


def double_integ(AMP,dt):
    """
    Create temporary arrays from integration

    Args:
        AMP(ndarray): acceleration
        dt(float): sample distance in seconds
    Returns:
        two array, one for velocity the other for displacement 
    """
    strms_AMP = deepcopy(AMP); # make a copy of AMP
    ACC_tmp = strms_AMP;
    VEL = np.cumsum(ACC_tmp)*dt
    VEL_copy = deepcopy(VEL);
    DIS = np.cumsum(VEL_copy)*dt
    return VEL,DIS


def AcceptSolution(acc_corr, dt):
    """
    Returns corrected velocity and displacement

    Args:
        acc_corr(list): corrected acceleration
        dt(float): sample distance in seconds
    Returns:
        a tuple with two lists, one for corrected velocity, the other for corrected displacement
    """
    vel_corr_good = []
    disp_corr_good = []
    strms_cp = deepcopy(acc_corr)
    for ind in range(len(acc_corr)):
        OUT = double_integ(strms_cp[ind],dt)
        vel_corr_good.append(OUT[0])
        disp_corr_good.append(OUT[1])
    return (vel_corr_good, disp_corr_good) 


def FinalTrace(ACC_CORR_X_good,num_X,t_a,std_delta_X,f_o,l_f):
    """
    Returns velocity and displacement from acceleration

    Args:
        ACC_CORR_X_good(list): 
        num_X(int):
        t_a(int): 
        std_delta_X(float): 
        f_o(int): 
        l_f(int): 
    Returns:
        different arrays for acceleration, velocity and displacement
    """
    # taper at the beginning of the signal (default 5%)
    strms_X = AsymTaper_beginning(ACC_CORR_X_good[num_X], len(ACC_CORR_X_good[num_X]), t_a)
        
    # Low-pass acausal Butterworth filter
    strms_X_fltrd = lowpass(strms_X,l_f, int(1/std_delta_X), corners=f_o, zerophase=True)
        
    # create temporary arrays for differentiation
    data_X_temporary = deepcopy(strms_X_fltrd)
        
    # VELOCITY
    #---------------------------------------------------------------

    data_X_temporary = np.cumsum(data_X_temporary)*std_delta_X

    # taper velocity
    data_X_temporary = AsymTaper_beginning(data_X_temporary, len(data_X_temporary) , t_a)
        
    # DISPLACEMENTS
    #---------------------------------------------------------------

    data_X_temporary = np.cumsum(data_X_temporary)*std_delta_X

    # taper displacement
    st_X_dis = AsymTaper_beginning(data_X_temporary, len(data_X_temporary) , t_a)

    # reconstruct Trace object
    #---------------------------

    # back to velocity
    (st_X_vel) = deepcopy(st_X_dis) # make a copy of streams 
    st_X_vel = np.gradient(st_X_vel,std_delta_X)

    # back to acceleration
    (st_X_acc) = deepcopy(st_X_vel) # make a copy of streams 
    st_X_acc = np.gradient(st_X_acc,std_delta_X)
    return st_X_acc,st_X_vel,st_X_dis


def AsymTaper_beginning(w_f, n_pts, t_a):
    """
    Taper at the beginning of the signal

    Args:
        w_f(ndarray): waveform amplitudes
        n_pts(int): number of points
        t_a(int): percentage of taper es. 5%
    Returns:
        array with tapered amplitudes
    """
    tap1 = cosTaper(n_pts, t_a*2*10**-2)
    tap = np.ones(n_pts)
    tap[:int(n_pts/2)] = tap1[:int(n_pts/2)]
    return w_f*tap


def AsymTaper(w_f, n_pts, t_a, t_z):
    """
    Taper at the beginning and at the end of the signal

    Args:
        w_f(ndarray): waveform amplitudes
        n_pts(int): number of points
        t_a(int): percentage of taper at the beginning of the trace es. 5%
        t_z(int): percentage of taper at the end of the trace  es. 5%
    Returns:
        array with tapered amplitudes
    """
    tap1 = cosTaper(n_pts, t_a*2*10**-2)
    tap2 = cosTaper(n_pts, t_z*2*10**-2)
    tap = np.ones(n_pts)
    tap[:int(n_pts/2)] = tap1[:int(n_pts/2)]
    tap[-n_pts+int(n_pts/2):] = tap2[-n_pts+int(n_pts/2):]
    return w_f*tap


def CornerFrequencies(M,recordtype,lcut,hcut):
    """
    Select corner frequencies for bandpass filter based on event magnitude

    Args:
        M(float): event magnitude
        recordtype(str): Analog A or Digital D
        lcut(list): list of low cut frequencies
        hcut(list): list of high cut frequencies
    Returns:
        two lists, one for low cut the other for high cut frequencies
    """
    if lcut == [None, None, None] or hcut == [None, None, None]:
        if M < 3.5: lcut=0.25
        elif M>=3.5 and M<4: lcut=[0.20,0.20,0.20]
        elif M>=4 and M<4.5: lcut=[0.20,0.20,0.20]
        elif M>=4.5 and M<5.5: lcut=[0.10,0.10,0.10]
        elif M>=5.5 and M<6.0: lcut=[0.07,0.07,0.07]
        elif M>=6.0 and M<6.5: lcut=[0.05,0.05,0.05]
        elif M>=6.5 and M<7.0: lcut=[0.04,0.04,0.04]
        elif M>=7.0 and M<8.0: lcut=[0.03,0.03,0.03]
        elif M>=8.0: lcut=[0.025,0.025,0.025]
        
        if recordtype == 'A': hcut = [25,25,25]
        else: hcut = [40,40,40]
        
    else:
        pass
        
    return lcut,hcut


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        if isinstance(obj, np.floating):
            return float(obj)
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return super(NpEncoder, self).default(obj)
