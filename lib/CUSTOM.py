#!/usr/bin/env python3
from obspy.core import UTCDateTime
import numpy as np
from copy import deepcopy
import time

from lib.utilities import SecondToCut, ProcessingOutput, create_output_dirs
from lib.waveforms import WaveformMultiplierFactor,WaveformCutting,WaveformDetrend,WaveformFilter, WaveformIntegrate,WaveformDifferentiate,WaveformTaper,WaveformCutting
from lib.spectra import WaveformSpectra,WaveformFFT
from lib.asdf_file import ASDFFile
 
#from lib.dictionary import JSON
#load_json = JSON('/PATH_INP', 'example_CUSTOM.json')
#dictn = load_json.loadJSON()
#filename = '/PATH_INP/example.h5'

base_output_path = './output/' + str(round(time.time()))
create_output_dirs(base_output_path)

def CUSTOMprocessing(dictn,filename):
	"""
	Compute the Custom Processing.

	Args:
	    dictn(dict):
		    dictionary with the settings for processing.
		filename(str):
		    path to file with the unprocessed waveforms.
	
	Returns:
	    a tuple. If the processing succeeds it contains the filename of the JSON file with processing info and None. 
		If the processing does not succeed it contains the filename of h5 file and errors.

	"""

	errors = {}

	ptype = dictn.get('workflow')['modality'] + dictn.get('workflow')['method']

	
	if dictn.get('workflow')['verbose']: print('number of steps for customized processing: ' + str(len(dictn.get('workflow')['steps'])))
	dictn.get('workflow')['method'] = 'C'

    # read the uncorrected accelerogram stored in ESM ASDF volumes; the filename are identified by a sequence of tags: \
    # net_code + station_code + location_code + event_id
	
	asdf_file = ASDFFile(filename)
	
	if asdf_file.is_valid() is True:
		pass   		

	try:
		wfs_stream = asdf_file.get_stream(asdf_file.get_station_ids()[0], asdf_file.get_event_ids()[0])
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('get_stream', str(e))
		errors.update({'get_stream' : str(e)})
	try:
		wfs_headers = asdf_file.get_header(asdf_file.get_station_ids()[0], asdf_file.get_event_ids()[0])
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('get_header', str(e))
		errors.update({'get_header' : str(e)})
	try:
		multiplier_factor = WaveformMultiplierFactor(1)
		wfs_stream = multiplier_factor.apply(wfs_stream)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('WaveformMultiplierFactor', str(e))
		errors.update({'WaveformMultiplierFactor' : str(e)})
	seconds_to_cut = [[0,0,0],[0,0,0]]
	if len(dictn.get('workflow')['steps'])>0:
		for step in dictn.get('workflow')['steps']:
			if dictn.get('workflow')['verbose']: print(step)
			if step == 'WaveformCutting':
				starttime = UTCDateTime(dictn.get('settings')['WaveformCut']['StartTime'])
				endtime = UTCDateTime(dictn.get('settings')['WaveformCut']['EndTime'])
				try:
					seconds_to_cut = SecondToCut(UTCDateTime(dictn.get('settings')['WaveformCut']['StartTime']),UTCDateTime(dictn.get('settings')['WaveformCut']['EndTime']),wfs_stream)
				except Exception as e:
					if dictn.get('workflow')['verbose']: print({'SecondToCut' : str(e)})
					errors.update({'SecondToCut' : str(e)})
				try:
					waveform_cut = WaveformCutting(starttime, endtime)			
					wfs_stream = waveform_cut.apply(wfs_stream)
					if dictn.get('workflow')['verbose']: print(wfs_stream)
					#wfs_cut = deepcopy(wfs_stream)
				except Exception as e:
					if dictn.get('workflow')['verbose']: print ('WaveformCutting',str(e))
					errors.update({'WaveformCutting' : str(e)})
			elif step == 'WaveformDetrend':
				try:
					waveform_detrend = WaveformDetrend(dictn.get('settings')['WaveformDetrend']['Type'])
					wfs_stream = waveform_detrend.apply(wfs_stream)
					#wfs_detrend = deepcopy(wfs_stream)
				except Exception as e:
					if dictn.get('workflow')['verbose']: print('WaveformDetrend', str(e))
					errors.update({'WaveformDetrend' : str(e)})

			elif step == 'WaveformIntegrate':
				try:
					waveform_integrate = WaveformIntegrate(dictn.get('settings')['WaveformIntegrate']['Method'])
					wfs_stream = waveform_integrate.apply(wfs_stream)
					#wfs_integrate = deepcopy(wfs_stream)
				except Exception as e:
					if dictn.get('workflow')['verbose']: print('WaveformIntegrate', str(e))
					errors.update({'WaveformIntegrate' : str(e)})
			elif step == 'WaveformDifferentiate':
				try:
					waveform_differentiate = WaveformDifferentiate(dictn.get('settings')['WaveformDifferentiate']['Method'])
					wfs_stream = waveform_differentiate.apply(wfs_stream)
					#wfs_differentiate = deepcopy(wfs_stream)
				except Exception as e:
					if dictn.get('workflow')['verbose']: print('waveform_differentiate', str(e))
					errors.update({'waveform_differentiate' : str(e)})
			elif step == 'WaveformTaper':
				try:
					waveform_tapering = WaveformTaper(dictn.get('settings')['WaveformTaper']['side']['right'],dictn.get('settings')['WaveformTaper']['side']['left'],dictn.get('settings')['WaveformTaper']['Type'])
					wfs_stream = waveform_tapering.apply(wfs_stream)
					#wfs_tapering = deepcopy(wfs_stream)
				except Exception as e:
					if dictn.get('workflow')['verbose']: print('WaveformTaper ', str(e))
					errors.update({'waveform_differentiate' : str(e)})
			elif step == 'WaveformFilter':
				try:
					waveform_filtering = WaveformFilter(dictn.get('settings')['FourierSpectrumFilter']['order'],dictn.get('settings')['FourierSpectrumFilter']['Type'],dictn.get('settings')['FourierSpectrumFilter']['LowCut'],dictn.get('settings')['FourierSpectrumFilter']['HighCut'])
					wfs_stream = waveform_filtering.apply(wfs_stream)
				except Exception as e:
					if dictn.get('workflow')['verbose']: print('WaveformFilter ', str(e))
					errors.update({'WaveformFilter' : str(e)})										
		_acceleration = deepcopy(wfs_stream)
		periods = np.array(dictn.get('data')['periods'])
		damping_val =  np.array(dictn.get('data')['damping'])
		if dictn.get('workflow')['postpro'] is True:
			try:
				waveform_fourier = WaveformFFT()
				frequency,FFTamplitude = waveform_fourier.apply(wfs_stream)
			except Exception as e:
				if dictn.get('workflow')['verbose']: print('WaveformFFT ', str(e))
				errors.update({'WaveformFFT' : str(e)})
			try:
				waveform_spectra = WaveformSpectra(periods,damping_val)
				Sa,Sd,channel = waveform_spectra.apply(wfs_stream)
			except Exception as e:
				if dictn.get('workflow')['verbose']: print('WaveformSpectra ', str(e))
				errors.update({'WaveformSpectra' : str(e)})
			try:
				waveform_integrate = WaveformIntegrate(dictn.get('settings')['WaveformIntegrate']['Method'])
				velocity = waveform_integrate.apply(wfs_stream)
			except Exception as e:
				if dictn.get('workflow')['verbose']: print('WaveformVelocity', str(e))
				errors.update({'WaveformSpectra' : str(e)})
			_velocity = deepcopy(velocity)
			try:
				waveform_integrate = WaveformIntegrate(dictn.get('settings')['WaveformIntegrate']['Method'])
				_displacement = waveform_integrate.apply(velocity)
			except Exception as e:
				if dictn.get('workflow')['verbose']: print('WaveformDisplacement', str(e))
				errors.update({'WaveformDisplacement' : str(e)})
	else:
		if dictn.get('workflow')['verbose']: print('A list of processing step must be created, admitted values are: WaveformCutting, WaveformDetrend, WaveformIntegrate, WaveformDifferentiate, WaveformTaper, WaveformFilter') 
		errors.update({'CustomWorkflow':'A list of processing step must be created, admitted values are: WaveformCutting, WaveformDetrend, WaveformIntegrate, WaveformDifferentiate, WaveformTaper, WaveformFilter'})

	#: manage the .h5 and .json output files
	try:
		out = ProcessingOutput(base_output_path,filename,dictn, errors, ptype, asdf_file, seconds_to_cut, _acceleration,_velocity, _displacement, periods, Sa, Sd, frequency, FFTamplitude)
		return out 
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('OUTPUT', str(e))
		errors.update({'OUTPUT' : str(e)})
		return [filename.replace('.h5','.processing.' + ptype + '.h5'),errors] 
				
#if __name__ == "__main__":
#  CUSTOMprocessing(dictn,filename)
