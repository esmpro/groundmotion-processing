#!/usr/bin/env python3
import numpy as np
from copy import deepcopy

from lib.utilities import SignalToNoiseWindows,FindNearest
from lib.spectra import WaveformFFT
from lib.konno_ohmachi import fast_konno_ohmachi

class WaveformQualityCheck:
	"""
	Performs the Quality Check
	"""
	def __init__(self,SW1,SW2, NW1, NW2,starttime,endtime):
		self.SW1 = SW1
		self.SW2 = SW2
		self.NW1 = NW1
		self.NW2 = NW2
		self.starttime = starttime
		self.endtime = endtime


	def signal_noise_windows_TD(self,stream, p_pick,s_pick):
		"""
		Finds the signal and noise windows for SNR in time domain(TD)

		Args:
			stream(StreamObject): stream to analyze
			p_pick(float): P-wave arrival (in seconds) derived from automatic picking
			s_pick(float): S-wave arrival (in seconds) derived from automatic picking
		Returns:
			a dictionary with the signal and noise windows, along with the automatic picking
		"""
		st=stream.copy()
		st.trim(starttime = self.starttime, endtime=self.endtime )
		st.detrend(type='demean').detrend(type='linear')
		st.filter('bandpass', freqmin=2.0, freqmax=8.0, corners=2,zerophase=True)
		SW1, SW2, NW1, NW2 = SignalToNoiseWindows(st,self.SW1,self.SW2,self.NW1,self.NW2,p_pick,s_pick)
		output_msg = {'signal_window':[SW1, SW2],'noise_window':[NW1, NW2],'ps_auto':{'P':p_pick,'S':s_pick}}
		return output_msg


	def signal_noise_stream_TD(self, stream, noisewindow, signalwindow):
		"""
		Return stream for signal and noise windows for SNR in time domain(TD)

		Args:
			stream(StreamObject): stream to analyze
			noisewindow(list): startime and endtime of the noise window (in seconds)
			signalwindow(list): startime and endtime of the signal window (in seconds)
		Returns:
			a dictionary with the signal and noise streams
		"""
		starttime = stream[0].stats['starttime']
		stream_tmp = deepcopy(stream)
		stream_tmp.detrend(type='demean').detrend(type='linear')
		NW_traces = deepcopy(stream_tmp)
		NW_traces.trim(starttime=starttime + noisewindow[0], endtime=starttime + noisewindow[1])
		NW_traces.detrend(type='demean').detrend(type='linear')

		SW_traces = deepcopy(stream_tmp)
		SW_traces.trim(starttime=starttime + signalwindow[0], endtime=starttime + signalwindow[1], pad=True, fill_value=None)
		SW_traces.detrend(type='demean').detrend(type='linear')
		return {'noise_window_stream':NW_traces, 'signal_window_stream':SW_traces}


	def signal_to_noise_TD(self,noise,signal):
		"""
		Compute SNR in time domain (TD) and provide a Quality Index (QI)

		Args:
			noise(StreamObject): noise stream
			signal(StreamObject): signal stream
		Returns:
			a dictionary with the results of the SNR in the time domain
		"""
		U_SW_RMS = np.sqrt(np.sum(np.square(signal[0].data))/signal[0].stats.npts)
		V_SW_RMS = np.sqrt(np.sum(np.square(signal[1].data))/signal[1].stats.npts)
		W_SW_RMS = np.sqrt(np.sum(np.square(signal[2].data))/signal[2].stats.npts)
		U_NW_RMS = np.sqrt(np.sum(np.square(noise[0].data))/noise[0].stats.npts)
		V_NW_RMS = np.sqrt(np.sum(np.square(noise[1].data))/noise[1].stats.npts)
		W_NW_RMS = np.sqrt(np.sum(np.square(noise[2].data))/noise[2].stats.npts)
		QI_U = 10*np.log10(U_SW_RMS/U_NW_RMS)
		QI_V = 10*np.log10(V_SW_RMS/V_NW_RMS)
		QI_W = 10*np.log10(W_SW_RMS/W_NW_RMS)
		QI_mean = np.mean([QI_U, QI_V, QI_W])
		return {'quality_index':{'average_value':QI_mean, 'component': {'U': QI_U, 'V': QI_V, 'W': QI_W}},'rms':{'signal':{'component':{'U': U_SW_RMS, 'V': V_SW_RMS, 'W': W_SW_RMS}},'noise':{'component':{'U': U_NW_RMS, 'V': V_NW_RMS, 'W': W_NW_RMS}}}}


	def waveform_class(self,rms_quality_index,warnings,parameters):
		"""
		Quality Class assignment.

		Args:
			rms_quality_index(float)
			warnings(dict): warnings
			parameters(dict): parameters of data processing and quality check
		Returns:
			a dictionary with the quality class and the reason
		"""
		dict_qi = {'A':None,'B':None,'C':None,'D':None,'reason':None}
		if warnings['the_number_of_traces_is_equal_to_three'] is False : dict_qi = {'A':False,'B':False,'C':False,'D':True,'reason':"missing one component"}
		elif warnings['empty_component'] is True: dict_qi = {'A':False,'B':False,'C':False,'D':True,'reason':"one empty component"}
		elif warnings['zero_component'] is True: dict_qi = {'A':False,'B':False,'C':False,'D':True,'reason':"one zero component"}
		elif warnings['trigger_detected'] is False: dict_qi = {'A':False,'B':False,'C':True,'D':False,'reason':"auto trigger failed"}
		elif warnings['picking_detected'] is False: dict_qi = {'A':False,'B':False,'C':True,'D':False,'reason':"auto PS phase picking failed"}
		elif np.isnan(rms_quality_index): dict_qi = {'A':False,'B':False,'C':False,'D':True,'reason':"Undefined Quality Index"}
		elif rms_quality_index <= 6: dict_qi = {'A':False,'B':False,'C':True,'D':False,'reason':"Low Signal to Noise Ratio"}
		elif rms_quality_index > 6 and rms_quality_index < 60: dict_qi = {'A':True,'B':False,'C':False,'D':False,'reason':"High Signal to Noise Ratio"}
		elif rms_quality_index >= 60: dict_qi = {'A':False,'B':False,'C':True,'D':False,'reason':"Anomalous Signal to Noise Ratio"}
		# Check Additional features when quality class is A or B.
		if dict_qi['A'] is True or dict_qi['B'] is True:
			# Amplitude ACC
			if warnings['unconsistent_horizontal_pga'] is True or warnings['unconsistent_horizontal_amplitude'] is True or \
			warnings['unconsistent_vertical_pga'] is True or warnings['unconsistent_vertical_amplitude'] is True: check_ACC = 1
			else: check_ACC = 0
			if warnings['extreme_pga_values'] is True: dict_qi = {'A':False,'B':True,'C':False,'D':False,'reason':"PGA > 2g"}
			elif check_ACC == 1: dict_qi = {'A':False,'B':True,'C':False,'D':False,'reason':"one component more than twice the other (ACC)"}
			elif warnings['unconsistent_picking'] is True: dict_qi = {'A':False,'B':True,'C':False,'D':False,'reason':"unreliable P-wave arrival"}
			elif warnings['multiple_events'] is True: dict_qi = {'A':False,'B':True,'C':False,'D':False,'reason':"more events detected"}
			if warnings['bad lc frequency'] is True or warnings['bad hc frequency'] is True: dict_qi = {'A':False,'B':True,'C':False,'D':False,'reason':"restricted frequency passband"}
		return dict_qi


	def horizontal_pgX_check(self,stream, flag):
		"""
		Check if PGA or PGD of one horizontal component is more than twice the other
		Args:
			stream(StreamObject)
			flag(int): 0 for acceleration, 1 for displacement
		Returns:
			a dictionary with the warnings
		"""
		warning_msg = {'unconsistent_horizontal_pga': False, 'unconsistent_horizontal_pgd': None}
		if flag == 1:
			st =stream.copy()
			st.detrend(type='demean').detrend(type='linear').taper(0.5,type='cosine').filter("highpass",freq=0.05)
		else: st = stream
		PGX_U = abs(st[0].max())
		PGX_V = abs(st[1].max())
		if PGX_U >=2*PGX_V or PGX_V >=2*PGX_U:
			if flag == 0:
				warning_msg = {'unconsistent_horizontal_pga': True}
			else:
				#print(PGX_U,PGX_V)
				warning_msg = {'unconsistent_horizontal_pgd': True}
		return warning_msg


	def vertical_pgX_check(self,stream, flag):
		"""
		Check if PGA or PGD of the vertical component is more than 3 times the horizontals
		Args:
			stream(StreamObject)
			flag(int): 0 for acceleration, 1 for displacement
		Returns:
			a dictionary with the warnings
		"""
		warning_msg = {'unconsistent_vertical_pga': False, 'unconsistent_vertical_pgd': None}
		if flag == 1:
			st =stream.copy()
			st.detrend(type='demean').detrend(type='linear').taper(0.5,type='cosine').filter("highpass",freq=0.05)
		else: st = stream
		PGX_U = abs(st[0].max())
		PGX_V = abs(st[1].max())
		PGX_W = abs(st[2].max())
		if (PGX_W >=3*PGX_V or PGX_V >=3*PGX_W) or (PGX_W >=3*PGX_U or PGX_U >=3*PGX_W):
			if flag == 0:
				warning_msg = {'unconsistent_vertical_pga': True}
			else:
				#print(PGX_U,PGX_V,PGX_W)
				warning_msg = {'unconsistent_vertical_pgd': True}
		return warning_msg


	def horizontal_amplitude_check(self,U_rms,V_rms):
		"""
		Check if the RMS on signal for one horizontal component is more than twice the other
		Args:
			U_rms(float): RMS of U component (horizontal 1)
			V_rms(float): RMS of V component (horizontal 2)
		Returns:
			a dictionary with the warnings
		"""	
		warning_msg = {'unconsistent_horizontal_amplitude': False}
		if U_rms >=2*V_rms or V_rms >=2*U_rms:
			warning_msg = {'unconsistent_horizontal_amplitude': True}
		return warning_msg


	def vertical_amplitude_check(self,U_rms,V_rms,W_rms):
		"""
		Check if the RMS on signal for the vertical component is more than 3 times the horizontals
		Args:
			U_rms(float): RMS of U component (horizontal 1)
			V_rms(float): RMS of V component (horizontal 2)
			W_rms(float): RMS of W component (vertical)
		Returns:
			a dictionary with the warnings
		"""	
		warning_msg = {'unconsistent_vertical_amplitude': False}
		if W_rms >=3*V_rms or W_rms >=3*U_rms:
			warning_msg = {'unconsistent_vertical_amplitude': True}
		return warning_msg


	def pga_check(self,stream):
		"""
		Check PGA > 2g for one of the 3 components
		Args:
			stream(StreamObject): stream to analyze
		Returns:
			a dictionary with the warnings
		"""	
		warning_msg = {'extreme_pga_values': False}
		PGA_U = abs(stream[0].max())
		PGA_V = abs(stream[1].max())
		PGA_W = abs(stream[2].max())
		if PGA_U>2*981 or PGA_V>2*981 or PGA_W>2*981:
			warning_msg = {'extreme_pga_values': True}
		return warning_msg


	def signal_noise_windows_FD(self,stream,starttime,parrival):
		"""
		Provide the signal and noise streams for SNR in the frequency domain (FD)

		Args:
			stream(StreamObject): stream to analyze
			starttime(UTCDateTime): startime of automatic waveform trimming
			parrival(float): P-wave arrival derived from automatic picking. In seconds from the startime above.
		Returns:
			a dictionary with the duration of the signal and noise windows, along with the corresponding streams
		"""
		maxstart_orig = max(stream[0].stats['starttime'], stream[1].stats['starttime'],stream[2].stats['starttime'])
		noise= deepcopy(stream)
		noise.trim(starttime=maxstart_orig, endtime=starttime+parrival, pad=False, fill_value=None)
		noise.detrend(type='demean').detrend(type='linear')
		noise_duration = starttime+parrival - maxstart_orig
		signal = deepcopy(stream)
		signal.trim(starttime=starttime+parrival, endtime=starttime+parrival+noise_duration, pad=False, fill_value=None)
		signal.detrend(type='demean').detrend(type='linear')
		signal_duration = noise_duration
		return {'noise':{'duration':noise_duration, 'amplitude':noise},'signal':{'duration':signal_duration, 'amplitude':signal}}


	def signal_noise_smooth_fft(self,signal,noise):
		"""
		Compute smoothed FFT on both signal and noise streams

		Args:
			signal(StreamObject): signal stream
			noise(StreamObject): noise stream
		Returns:
			a dictionary with the smoothed FFTs for the 3 components
		"""
		SWFFT = WaveformFFT()
		out_SW = SWFFT.apply(signal)
		#resample smoothed FFT on both signal and noise streams
		FFT_f = np.exp(np.linspace(np.log(0.01),np.log(100),200))
		SW=[]
		for i in range(3):
			tmp1=np.interp(FFT_f,out_SW[0][i][1:],out_SW[1][i][1:])
			#smoothed = faster_konno_ohmachi(tmp1, FFT_f, smooth_coeff=40, n_cores =8)
			smoothed = fast_konno_ohmachi(tmp1, FFT_f, smooth_coeff=40, progress_bar=False)
			#y = savgol_filter(out_SW[1][i], 1275, 3, mode='interp')
			SW.append(smoothed)
		#same for noise
		NWFFT = WaveformFFT()
		out_NW = NWFFT.apply(noise)
		NW=[]
		for i in range(3):
			tmp1=np.interp(FFT_f,out_NW[0][i][1:],out_NW[1][i][1:])
			#smoothed = faster_konno_ohmachi(tmp1, FFT_f, smooth_coeff=40, n_cores =8)
			smoothed = fast_konno_ohmachi(tmp1, FFT_f, smooth_coeff=40, progress_bar=False)
			#y = savgol_filter(out_NW[1][i], 175, 3, deriv=0, delta=1.0, axis=- 1, mode='interp', cval=0.0)
			NW.append(smoothed)
		#
		frequency_sw = [FFT_f,FFT_f,FFT_f]
		frequency_nw = [FFT_f,FFT_f,FFT_f]
		amplitude_sw = [SW[0], SW[1],SW[2]]
		amplitude_nw = [NW[0], NW[1],NW[2]]
		
		return {'noise':{'component':{'U':{'frequency':frequency_nw[0],'amplitude':amplitude_nw[0]},'V':{'frequency':frequency_nw[0],'amplitude':amplitude_nw[1]},'W':{'frequency':frequency_nw[0],'amplitude':amplitude_nw[2]}}}, \
		'signal':{'component':{'U':{'frequency':frequency_sw[0],'amplitude':amplitude_sw[0]},'V':{'frequency':frequency_sw[1],'amplitude':amplitude_sw[1]},'W':{'frequency':frequency_sw[2],'amplitude':amplitude_sw[2]}}}}


	def compute_lc_hc(self,noise_duration,signal_duration,fft_noise,fft_signal,linspcd_frequency,Nc,magnitude):
		"""
		Derive low-cut (lc) and high-cut (hc) frequencies for the bandpass filter.

		Args:
			noise_duration(float): duration of the noise window
			signal_duration(float): duration of the signal window
			fft_noise(list): list of fft amplitude for noise of the 3 components
			fft_signal(list): list of fft amplitude for signal of the 3 components
			linspcd_frequency(ndarray): frequencies of the Fourier Spectrum
			Nc(float): Nyquist Frequency
			magnitude(float): event magnitude
		Returns:
			two dictionaries, one for warnings and the other for the processing parameters.
		"""
		warning_msg = {'lc frequecies based on signal to noise ratio of the fft': True, 'lc frequecies based on magnitude ranges': False, 'bad lc frequency':False, 'hc frequecies based on signal to noise ratio of the fft': True,'hc frequencies based on fixed threshold': False,'bad hc frequency':False,'high cut threshold greater than Nyquist frequency': False,'high cut frequency lower than 10 Hz': False,'low cut frequency greater than 1 Hz': False,'low cut threshold less than lc limit': False}
		lc = np.empty(3);lc[:]=np.NaN
		hc = np.empty(3);hc[:]=np.NaN
		lc_limit = max(1/noise_duration, 1/signal_duration)
		for i in range(3):
			snr_tmp = (np.array(fft_signal)[i]-np.array(fft_noise)[i])/np.array(fft_noise)[i]                       
			idx_max=FindNearest(snr_tmp,max(snr_tmp))
			for ii in range(len(linspcd_frequency[:idx_max])):
				freq=linspcd_frequency[idx_max-ii]
				indx = [x for x,y in enumerate(linspcd_frequency) if y > freq/np.sqrt(2)/np.sqrt(2) and y <= freq]
				idx1 = min(indx)
				idx2 = max(indx)
				if len(indx) != 1:                                            
					tmp = np.mean(snr_tmp[idx1:idx2])
					if tmp <2:
						lc[i] = freq/np.sqrt(2)
						break
				else:
					break
			output_SNR_msg = {'lc':{'component':{'U':lc[0],'V':lc[1],'W':lc[2]}}}
			if lc[i] <= lc_limit: 
				lc[i]= lc_limit*1.25 #np.NaN
				warning_msg.update({'low cut threshold less than lc limit': True})
			if lc[i] > 1: #lc[i] = np.NaN 
				warning_msg.update({'low cut frequency greater than 1 Hz': True})
			
   
  

  		# hc da FFT
		for ii in range(len(linspcd_frequency[idx_max:-1])):
			freq=linspcd_frequency[idx_max+ii]
			#indx = [x for x,y in enumerate(linspcd_frequency) if y < freq+10 and y >= freq]
			indx = [x for x,y in enumerate(linspcd_frequency) if y < freq*np.sqrt(1.3)*np.sqrt(1.3) and y >= freq]
			idx1 = min(indx)
			idx2 = max(indx)
			tmp = np.mean(snr_tmp[idx1:idx2])
			if tmp <2:
				hc[i] = int(freq*np.sqrt(1.3))
				output_SNR_msg.update({'hc':{'component':{'U':hc[0],'V':hc[1],'W':hc[2]}}})
				if hc[i] >= Nc: 
					hc[i] = Nc*0.75
					warning_msg.update({'high cut threshold greater than Nyquist frequency': True})
				if hc[i] < 10: 
					#hc[i] = np.NaN # su quelli MP venivano valori insensati o per spike ecc
					warning_msg.update({'high cut frequency lower than 10 Hz': True})
				break
		output_SNR_msg.update({'hc':{'component':{'U':hc[0],'V':hc[1],'W':hc[2]}}})
		if np.isnan(lc).any():
			warning_msg = {'lc frequecies based on signal to noise ratio of the fft': False,'lc frequecies based on magnitude ranges': True,'bad lc frequency':False} 
			if magnitude <3.5: lc = np.array([0.25,0.25,0.25])
			elif magnitude>=3.5 and magnitude<4.5: lc = np.array([0.2,0.2,0.2])
			elif magnitude>=4.5 and magnitude<5.5: lc = np.array([0.1,0.1,0.1])
			elif magnitude>=5.5 and magnitude<6.0: lc = np.array([0.07,0.07,0.07])
			elif magnitude>=6.0 and magnitude<6.5: lc = np.array([0.05,0.05,0.05])
			elif magnitude>=6.5 and magnitude<7.0: lc = np.array([0.04,0.04,0.04])
			elif magnitude>=7.0 and magnitude<8.0: lc = np.array([0.03,0.03,0.03])
			elif magnitude>=8.0: lc = np.array([0.025,0.025,0.025])
			
		if np.isnan(hc).any():
			warning_msg.update({'hc frequecies based on signal to noise ratio of the fft': False,'hc frequencies based on fixed threshold': True,'bad hc frequency':False})
			hc = np.array([40,40,40])

		if max(lc) > 0.4: warning_msg['bad hc frequency']=True
		if np.mean(hc) < 20: warning_msg['bad hc frequency']=True
		output_msg = {'lc':{'component':{'U':lc[0],'V':lc[1],'W':lc[2]}}, 'hc':{'component':{'U':hc[0],'V':hc[1],'W':hc[2]}}}
		
		return  warning_msg, output_msg, output_SNR_msg