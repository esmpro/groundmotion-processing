import pyasdf
import argparse
from os.path import isfile as os_path_isfile
from os.path import exists as os_path_exists


def valid_asdf_r(ifile):
    """ 
    Check if the input file exist and it is an ASDF volume
    Args:
        ifile(str): path to input file
    Returns:
        ifile and ASDFDataSet 
    """
    err_str0 = "input file '" + ifile + "' does not exist!"
    if not os_path_isfile(ifile): raise argparse.ArgumentTypeError(err_str0)
 
    err_str2 = "input file '" + ifile + "' is not an ASDF volume"
    try:
        ds = pyasdf.ASDFDataSet(ifile, mode='r')
        return [ifile, ds]
    except: raise argparse.ArgumentTypeError(err_str2)


def valid_true_false(arg):
    """ 
    Check if the argument is true or false
    Args:
        arg(str)
    Returns:
        bool True or False
    """
    if arg.lower() == 'true': return True
    elif arg.lower() == 'false': return False
    else:
        raise argparse.ArgumentTypeError("invalid choice: '" + str(arg) + "' (choose from 'true' or 'false')")


def is_valid_file(arg):
    """ 
    Check if the argument is an input file
    Args:
        arg(str)
    Returns:
        the input argument or errors
    """
    if arg == "False": return arg # per quando si usa una funzione passando un oggetto al posto del file ;-)
    if not os_path_exists(arg) and arg != 'no-file':
        raise argparse.ArgumentTypeError("The file '%s' does not exist!" % arg)
    else: return arg
