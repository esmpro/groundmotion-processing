#!/usr/bin/env python3

from lib.CUSTOM import CUSTOMprocessing
from lib.PAO11 import PAO11processing
from lib.eBASCO import eBASCOprocessing

class Workflow:
	""" Processing Workflow """

	@staticmethod
	def createWorkflow(dictn):
		""" 
		Read the input dictionary and run the corresponding module for processing
		Args:
			dictn(dict): a dictionary with the processing info
		Returns:
			run the module for data processing and return the input dictionary
		"""
		if dictn.get('workflow')['modality'] == 'M' or dictn.get('workflow')['modality'] == 'A':
			pass
		else:
			raise Exception('Unknown Modality "' +  dictn.get('workflow')['modality'] + '" for standard processing, admitted values are : {M,A}; M:Manual - A:Automatic ') 		
		if dictn.get('workflow')['method'] == 'P' or dictn.get('workflow')['method'] == 'B' or dictn.get('workflow')['method'] == 'C':
			pass
		else:
			raise Exception('Unknown Method "' + dictn.get('workflow')['method'] + '" for data processing, admitted values are : {P,B,C}; P:Paolucci el al 2011 - B:eBASCO - C:Custom')
		if dictn.get('workflow')['trigger'] == 'NT' or dictn.get('workflow')['trigger'] == 'LT':
			pass
		else:
			raise Exception ('Unknown Trigger "' + dictn.get('workflow')['trigger'] + '" , admitted values are : {NT,LT}; NT:Normal Trigger - LT:Late Trigger')
		if dictn.get('workflow')['type'] == 'A' or dictn.get('workflow')['type'] == 'D':
			pass
		else:
			raise Exception ('Unknown Waveform "' + dictn.get('workflow')['type'] + '" , admitted values are : {A,D}; A:Analog - D:Digital')
		if len(dictn.get('data')['periods']) != 0:
			pass
		else:
			raise Exception ('Periods not available ')
		if type(dictn.get('settings')['AmplitudeScaling']['MultiplierFactor']) == float or   type(dictn.get('settings')['AmplitudeScaling']['MultiplierFactor']) == int: 
			pass
		else:
			raise Exception('Only float or integer data type can be used to change the waveform amplitude')
		
		if dictn.get('settings')['WaveformResampling']['ResamplingFactor'] in dictn.get('settings')['WaveformResampling'].values() and type(dictn.get('settings')['WaveformResampling']['ResamplingFactor']) is float:
			pass
		else:
			raise Exception('Only float data type can be used to resample a waveform')
		
		if dictn['settings']['WaveformCut']['StartTime'] in dictn['settings']['WaveformCut'].values() and dictn['settings']['WaveformCut']['StartTime'] is not None:
			pass
		else:
			raise Exception('Starttime non available: YYYY-MM-DDThh:mm:ss.sZ')
		if dictn['settings']['WaveformCut']['EndTime'] in dictn['settings']['WaveformCut'].values() and dictn['settings']['WaveformCut']['EndTime'] is not None:
			pass
		else:
			raise Exception('Endtime non available: YYYY-MM-DDThh:mm:ss.sZ')	
		if type(dictn.get('settings')['WaveformTaper']['side']['left']) is int:
			pass
		else:
			raise Exception('Only float data type can be used as taper percentage in the pre-event')
			
		if dictn.get('workflow')['method'] == 'P':
			if type(dictn.get('settings')['WaveformTaper']['side']['right']) is int or type(dictn.get('settings')['WaveformTaper']['side']['right']) is float:
				pass
			else:
				raise Exception('Only float or integer data type can be used as taper percentage in the post-event')

		if dictn.get('workflow')['method'] == 'B':
			dictn.get('settings')['WaveformTaper']['side']['right'] = None
		if dictn.get('workflow')['method'] == 'P':
			if dictn.get('workflow')['trigger'] == 'LT':
				if type(dictn.get('settings')['WaveformTaper']['EndLT']) is int or type(dictn.get('settings')['WaveformTaper']['EndLT']) is float:
					pass
				else:
					raise Exception('Only float or integer data type can be used as taper percentage in the post-event for late triggered records') 
		if type(dictn.get('settings')['FourierSpectrumFilter']['HighCut'][0]) is float or type(dictn.get('settings')['FourierSpectrumFilter']['HighCut'][0]) is int:
			pass
		else:
			raise Exception('Only float or integer data type can be used as higher cut-off frequency of the 1st component')
		if type(dictn.get('settings')['FourierSpectrumFilter']['HighCut'][1]) is float or type(dictn.get('settings')['FourierSpectrumFilter']['HighCut'][1]) is int:
			pass
		else:
			raise Exception('Only float or integer data type can be used as higher cut-off frequency of the 2nd component')
		if type(dictn.get('settings')['FourierSpectrumFilter']['HighCut'][2]) is float or type(dictn.get('settings')['FourierSpectrumFilter']['HighCut'][2]) is int:
			pass
		else:
			raise Exception('Only float or integer data type can be used as higher cut-off frequency of the 3th component')
		
		if dictn.get('workflow')['method'] == 'P':
			
			if type(dictn.get('settings')['FourierSpectrumFilter']['LowCut'][0]) is float or type(dictn.get('settings')['FourierSpectrumFilter']['LowCut'][0]) is int:
				pass
			else:
				raise Exception('Only float or integer data type can be used as lower cut-off frequency of the 1st component')
				
			if type(dictn.get('settings')['FourierSpectrumFilter']['LowCut'][1]) is float or type(dictn.get('settings')['FourierSpectrumFilter']['LowCut'][1]) is int:
				pass
			else:
				raise Exception('Only float or integer data type can be used as lower cut-off frequency of the 2nd component')
								
			if type(dictn.get('settings')['FourierSpectrumFilter']['LowCut'][2]) is float or type(dictn.get('settings')['FourierSpectrumFilter']['LowCut'][2]) is int:
				pass
			else:
				raise Exception('Only float or integer data type can be used as lower cut-off frequency of the 3th component')
		
		elif dictn.get('workflow')['method'] == 'B':

			dictn.get('settings')['FourierSpectrumFilter']['LowCut'][0] = None
			dictn.get('settings')['FourierSpectrumFilter']['LowCut'][1] = None
			dictn.get('settings')['FourierSpectrumFilter']['LowCut'][2] = None
		
		if type(dictn.get('settings')['FourierSpectrumFilter']['order']) is int:
			pass
		else:
			raise Exception('Only integer data type can be used as order of the Butterworth filter')
			
		if dictn.get('workflow')['method'] == 'P':
			if dictn.get('settings')['WaveformTaper']['side']['left'] >= 0 and dictn.get('settings')['WaveformTaper']['side']['right'] <= 50:
				pass
			else:
				raise Exception('Taper percentage both in pre- and post-event must be in the [0 50] interval')
			
		if dictn.get('workflow')['method'] == 'B':
			if type(dictn.get('settings')['WaveformDetrend']['T1Samples']) is int:
				pass
			else:
				raise Exception('Only integer data type can be used to sample the T1 time for eBASCO processing')
			if type(dictn.get('settings')['WaveformDetrend']['T2Samples']) is int:
				pass
			else:
				raise Exception('Only integer data type can be used to sample the T2 time for eBASCO processing')
			if type(dictn.get('settings')['WaveformDetrend']['T3Samples']) is int:
				pass
			else:
				raise Exception('Only integer data type can be used to sample the T3 time for eBASCO processing')			
			dictn.get('settings')['WaveformTaper']['side']['right'] = None
		if dictn.get('workflow')['modality'] == 'M' and dictn.get('workflow')['method'] == 'P':
			dictn.get('workflow')['steps'] = None
			dictn.get('settings')['WaveformDetrend']['T1Samples'] = None
			dictn.get('settings')['WaveformDetrend']['T2Samples'] = None
			dictn.get('settings')['WaveformDetrend']['T3Samples'] = None
			dictn.get('settings')['WaveformIntegrate']['Method'] = 'cumtrapz'
			dictn.get('settings')['WaveformDifferentiate']['Method'] = 'gradient'
			dictn.get('settings')['WaveformDetrend']['Type'] = 'linear'
			return PAO11('MP'), dictn
		elif dictn.get('workflow')['modality'] == 'A' and dictn.get('workflow')['method'] == 'P':
			dictn.get('workflow')['steps'] = None
			dictn.get('settings')['WaveformDetrend']['T1Samples'] = None
			dictn.get('settings')['WaveformDetrend']['T2Samples'] = None
			dictn.get('settings')['WaveformDetrend']['T3Samples'] = None
			dictn.get('settings')['WaveformIntegrate']['Method'] = 'cumtrapz'
			dictn.get('settings')['WaveformDifferentiate']['Method'] = 'gradient'
			dictn.get('settings')['WaveformDetrend']['Type'] = 'linear'
			return PAO11('AP'), dictn
		elif dictn.get('workflow')['modality'] == 'M' and dictn.get('workflow')['method'] == 'B' and dictn.get('workflow')['trigger'] == 'NT':
			dictn.get('workflow')['steps'] = None
			dictn.get('settings')['WaveformIntegrate']['Method'] = 'cumtrapz'
			dictn.get('settings')['WaveformDifferentiate']['Method'] = 'gradient'
			dictn.get('settings')['WaveformDetrend']['Type'] = 'linear'
			return eBASCO('MBNT'), dictn
		elif dictn.get('workflow')['modality'] == 'A' and dictn.get('workflow')['method'] == 'B' and dictn.get('workflow')['trigger'] == 'NT':
			dictn.get('workflow')['steps'] = None
			dictn.get('settings')['WaveformIntegrate']['Method'] = 'cumtrapz'
			dictn.get('settings')['WaveformDifferentiate']['Method'] = 'gradient'
			dictn.get('settings')['WaveformDetrend']['Type'] = 'linear'
			return eBASCO('ABNT'), dictn
		elif dictn.get('workflow')['method'] == 'B' and dictn.get('workflow')['trigger'] == 'LT':
			raise Exception('only NT waveforms can be processed by eBASCO')
		elif dictn.get('workflow')['method'] == 'C' and dictn.get('workflow')['trigger'] == 'LT':
			raise Exception('only NT waveforms can be processed by CUSTOM workflow')
		elif dictn.get('workflow')['method'] == 'B' and dictn.get('workflow')['type'] == 'A':
			raise Exception('only Digital waveforms can be processed by eBASCO')
		elif dictn.get('workflow')['method'] == 'C':
			try:
				len(dictn.get('workflow')['steps'])	 > 0
			except Exception as e:
				print('ProcessingStep',str(e))
				print('!A list of processing step must be created, admitted values are: WaveformCutting, WaveformDetrend, WaveformIntegrate, WaveformDifferentiate, WaveformTaper, WaveformFilter')
			dictn.get('settings')['WaveformDetrend']['T1Samples'] = None
			dictn.get('settings')['WaveformDetrend']['T2Samples'] = None
			dictn.get('settings')['WaveformDetrend']['T3Samples'] = None
			dictn.get('workflow')['modality'] = 'M'
			return CUSTOM('MC'), dictn

	def __init__(self, workflow):
		self.workflow = workflow
		self.stream = None
		self.dictn = None

class CUSTOM(Workflow):
	""" Run the Custom processing """
	def process(self,filename,dictn):
		"""
		Apply the Custom processing
			Args:
				filename(str): path to h5 file
				dictn(dict): a dictionary with the processing info
			Returns:
				processed stream
		"""
		try:
			wfs_stream = CUSTOMprocessing(dictn,filename)
			print('workflow type: ' + self.workflow + dictn.get('workflow')['type'] + dictn.get('workflow')['trigger'])
		except Exception as e:
			print('CUSTOM WF', str(e))
		return wfs_stream

class PAO11(Workflow):
	""" Run the PAO11 processing (standard of PAO11 and ESM)"""

	def process(self,filename,dictn):
		"""
		Apply the PAO11 processing
			Args:
				filename(str): path to h5 file
				dictn(dict): a dictionary with the processing info
			Returns:
				processed stream
		"""
		try:
			wfs_stream = PAO11processing(dictn,filename)
			print('workflow type: ' + self.workflow + dictn.get('workflow')['type'] + dictn.get('workflow')['trigger'])
		except Exception as e:
			print('PAO11 WF', str(e))
		return wfs_stream

class eBASCO(Workflow):
	""" Run the eBASCO processing """

	def process(self,filename,dictn):
		"""
		Apply the eBASCO processing
			Args:
				filename(str): path to h5 file
				dictn(dict): a dictionary with the processing info
			Returns:
				processed stream
		"""
		try:
			wfs_stream = eBASCOprocessing(dictn,filename)
			print('workflow type: ' + self.workflow)
		except Exception as e:
			print('eBASCO WF', e.message)
		return wfs_stream
