#!/usr/bin/env python3
from numpy.fft import fft as np_fft
from numpy import absolute as np_abs
from numpy import log2 as np_log2
from numpy import exp2 as np_exp2
from numpy import ceil as np_ceil
from numpy import arange as np_arange


def FFT_spectrum(acc):
    """
    Compute the Fourier Amplitude Spectrum

    Args:
        acc(StreamObject)
    Returns:
        freq_out(ndarray)
        outst(ndarray)
        delta_f(float)
        npts_fin(int)
    """
    npts = int(np_exp2(np_ceil(np_log2(acc[0].stats.npts))))
    fs = acc[0].stats.sampling_rate
    outst = np_fft(acc[0].data, n=int(npts))    
    outst = np_abs(outst[0:int(npts/2)]/fs)
    freq_out = fs*np_arange(0,npts/2)/npts
    delta_f = fs/npts; npts_fin = int(npts/2)
    return (freq_out, outst, delta_f, npts_fin)

