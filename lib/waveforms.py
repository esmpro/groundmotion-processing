from obspy.core import Stream, Trace
from obspy.signal.trigger import recursive_sta_lta, trigger_onset
from copy import deepcopy

import numpy as np

from lib.utilities import PS_TravelTimes,wf_Partition


class WaveformCuttingAuto:
    """ Automatic waveform trimming """
    def __init__(self):
 
        self.starttime = None
        self.endtime = None    
            
    def trigger(self, stream):
        """
        Application of the trigger algorythm
        Args:
            stream(StreamObject)
        Returns:
            array with triggers ON and OFF in UTCDateTime
        """
        warning_msg = {'trigger_detected': False}
        sampling_rate = stream[0].stats.sampling_rate
        st=stream.copy()
        st.detrend(type='demean').detrend(type='linear')
        st.filter('bandpass', freqmin=2.0, freqmax=8.0, corners=2,zerophase=True)
        
        t0 = st[0].stats['starttime']
        cft = recursive_sta_lta(st[0].data, int(1 * sampling_rate), int(8. * sampling_rate))
        on_of = trigger_onset(cft, 2.5, 0.3)/sampling_rate
        on_of = on_of.astype(type('float', (float,), {}))
        if len(on_of) != 0: warning_msg = {'trigger_detected': True}
        
        on_of_UTC = np.ones_like(on_of)
        for ii in range(len(on_of)):
            on_of_UTC[ii] = [t0 + on_of[ii][0], t0 + on_of[ii][1]]
        return on_of_UTC        

    def targetevent(self,stream,eve_depth,origin_time,epi_dist,triggerUTC, cur=None):
        """
        Identification of the target event
        
        Args:
            stream(list): contains the stream to be analyzed
            eve_depth(float): event depth
            origin_time(UTCDateTime): event origin time
            epi_dist(str): epicentral distance
            triggerUTC(ndarray): detected triggers on and off, ourput of the trigger algorythm 
            cur(NoneType):
        Returns:
            list with trigger ON and OFF for the target event, and dictionary with warnings
        """
        warning_msg = {'multiple_events': False, 'unconsistent_picking': False}
        if len(triggerUTC)>1:
            T1 = None
            T2 = None
            try:
                [timeP, timeS] = PS_TravelTimes(eve_depth, origin_time, epi_dist)
                idx=np.abs(triggerUTC[:,0] - timeP).argmin()            
                T1=triggerUTC[idx,0]
                T2=triggerUTC[idx,1] 
                st = stream[0]
                st=st.copy()
                st.detrend(type='demean').detrend(type='linear')
                dt = stream[0][0].stats['delta']       
                I1 = wf_Partition(stream[0], dt, 0.05, 0.95)[0,0]     
                I2 = wf_Partition(stream[0], dt, 0.05, 0.95)[0,1]
                # check travel times and Arias
                if abs(timeP-I1) > 20: warning_msg['unconsistent_picking']=True
                #
                idx=np.abs(triggerUTC[:,0] - I1).argmin()
                delta_t = timeS-timeP
                for (i,x) in enumerate(triggerUTC[:,0]):
                        if (i>idx and x<=I2):
                            warning_msg['multiple_events']=True
                           
                            break
                            
                        var_per = abs(delta_t -(triggerUTC[idx+1,0] - triggerUTC[idx,0]))/max(delta_t,(triggerUTC[idx+1,0] - triggerUTC[idx,0])) *100
                        if var_per <= 35:
                            warning_msg['multiple_events']=False
                            if triggerUTC[:,0][i+1] <= I2:
                                warning_msg['multiple_events']=True 
            except Exception as e:
                print(str(e))
                pass
        elif len(triggerUTC)==1:
            T1=triggerUTC[0,0]
            T2=triggerUTC[0,1]
        return [T1,T2], warning_msg
    
    def cut(self,stream,epi_dist,timewindow):
        """
        Application of waveform trimming
        
        Args:
            stream(StreamObject): contains the stream to be analyzed
            epi_dist(str): epicentral distance
            timewindow(list): with trigger ON and OFF for the target event
        Returns:
            two dictionaries, one with settings for automatic trimming, the other with warnings
        """
        settings_msg = {'start_time': None, 'end_time': None}
        warning_msg = {'epicentral_distance_detected': False}
        if epi_dist: warning_msg['epicentral_distance_detected'] = True
        tstart = max(stream[0].stats['starttime'],stream[1].stats['starttime'],stream[2].stats['starttime'])
        tend = min(stream[0].stats['endtime'],stream[1].stats['endtime'],stream[2].stats['endtime'])
        if timewindow[0]>tstart and timewindow[1]<tend and warning_msg['epicentral_distance_detected'] is True:
            
            if float(epi_dist)<= 20:
                T_start=(timewindow[0]-20 if tstart<=(timewindow[0]-20) else tstart)
                T_end=timewindow[1]+20
            elif float(epi_dist)> 20 and float(epi_dist)<= 100:
                T_start=(timewindow[0]-20 if tstart<=(timewindow[0]-20) else tstart)
                T_end=timewindow[1]+40
            elif float(epi_dist)>100 and float(epi_dist)<=200:             
                T_start=(timewindow[0]-20 if tstart<=(timewindow[0]-20) else tstart)
                T_end=timewindow[1]+60
            elif float(epi_dist) > 200:
                T_start=(timewindow[0]-20 if tstart<=(timewindow[0]-20) else tstart)
                T_end=timewindow[1]+80
        else:
            T_start = tstart
            T_end = tend

        settings_msg['start_time'] = T_start
        settings_msg['end_time'] = T_end
        return settings_msg, warning_msg


class WaveformCutting:
    """ Waveform trimming at specific startime and endtime """

    def __init__(self, starttime, endtime):
        self.starttime = starttime
        self.endtime = endtime

    def apply(self, stream):
        """ 
        Apply waveform trimming
        Args:
            stream(StreamObject)
        Returns:
            stream with trimmed waveforms
        """
        for i in range(len(stream)):
            stream[i].trim(starttime=self.starttime, endtime=self.endtime, pad=True, fill_value=stream[i][0])
        return stream


class WaveformResampling:
    """ Waveform resampling """

    def __init__(self):
        self.sampling_rate = 200.0

    def apply(self, stream):
        """ 
        Apply waveform resampling
            Args:
                stream(StreamObject)
            Returns:
                stream with resampled waveforms
        """
        if stream[0].stats.sampling_rate > 10e7 \
                or stream[1].stats.sampling_rate > 10e7 \
                or stream[2].stats.sampling_rate > 10e7:
            stream.resample(self.sampling_rate, strict_length=True)
        return stream


class WaveformDetrend:
    """ Waveform detrending """

    def __init__(self, dtrndtype):
        self.dtrndtype = dtrndtype

    def apply(self, stream):
        """ 
        Apply waveform detrending
            Args:
                stream(StreamObject)
            Returns:
                stream with detrended waveforms
        """
        for i in range(len(stream)):
            stream[i].detrend(type=self.dtrndtype)
        return stream


class WaveformIntegrate:
    """ Integration of continuous time series """
    def __init__(self, intmeth):
        self.intmeth = intmeth

    def apply(self, stream):
        """ 
        Apply integration
            Args:
                stream(StreamObject)
            Returns:
                stream with integrated time series
        """
        stream.integrate(method=self.intmeth)
        return stream


class WaveformDifferentiate:
    """ Differentiation of continuous time series """
    def __init__(self, diffmeth):
        self.diffmeth = diffmeth

    def apply(self, stream):
        """ 
        Apply differentiation
            Args:
                stream(StreamObject)
            Returns:
                stream with differentiated time series
        """
        stream.differentiate(method=self.diffmeth)
        return stream


class WaveformInstrCorr:
    """ Instrumental correction """

    def __init__(self, npts, delta, fnat, smor):
        self.npts = npts
        self.delta = delta
        self.fnat = fnat
        self.smor = smor

    def apply(self, stream):
        """ 
        Apply the instrumental correction
            Args:
                stream(StreamObject)
            Returns:
                stream with corrected waveforms
        """
        w_f_cor = []
        for j in range(len(stream)):
            fs = self.fnat[j];
            fs2 = (self.fnat[j]) * (self.fnat[j])
            ig = 0
            while 2 ** ig <= self.npts[j]:
                ig = ig + 1
            Nfft = 2 ** ig
            F = np.fft.rfft(stream[j], n=Nfft)
            f = np.fft.fftfreq(Nfft, d=self.delta[j])
            s = Fdec = np.zeros(len(F)) + 1j
            for i in range(0, len(F)):
                s[i] = complex((fs2 - f[i] ** 2) / fs2, 2.0 * self.smor[j] * f[i] / fs)
                Fdec[i] = F[i] * s[i]
            w_f_cor_tmp = np.fft.irfft(Fdec, n=Nfft)
            stream[j].data = w_f_cor_tmp[0:self.npts[j]]
        return stream


class WaveformTaperPAO11:
    """ Waveform Taper in PAO11 """

    def __init__(self, PercentageFromBeginning, PercentageFromEnd, SecondsFromBeginning, SecondsFromEnd, triggertype,
                 r1, D1, D2, D3):
        self.PercentageFromBeginning = PercentageFromBeginning
        self.PercentageFromEnd = PercentageFromEnd
        self.SecondsFromBeginning = SecondsFromBeginning
        self.SecondsFromEnd = SecondsFromEnd
        self.triggertype = triggertype
        self.r1 = r1
        self.D1 = D1
        self.D2 = D2
        self.D3 = D3

    def apply(self, stream):
        """ 
        Apply the taper according to the PAO11 (Paolucci et al., 2011) processing method, which is the standard in PAO11
            Args:
                stream(StreamObject)
            Returns:
                stream with tapered amplitudes
        """

        if self.r1 < 0.05 and not self.triggertype:
            self.triggertype = 'LT'
        elif self.r1 >= 0.05 and not self.triggertype:
            self.triggertype = 'NT'
        if self.triggertype == 'LT' and not self.PercentageFromBeginning:
            self.PercentageFromBeginning = 0
        elif self.triggertype == 'NT' and not self.PercentageFromBeginning:
            self.PercentageFromBeginning = min(float(self.D1) / float(self.D1 + self.D2 + self.D3) * 100, 5)

        if self.PercentageFromBeginning == None:
            self.PercentageFromBeginning = 2.0
        else:
            self.PercentageFromBeginning == self.PercentageFromBeginning / 2.0
        if self.PercentageFromEnd == None:
            self.PercentageFromEnd = 2.0
        else:
            self.PercentageFromEnd == self.PercentageFromEnd / 2.0

        for i in range(len(stream)):
            if self.SecondsFromBeginning < 0:
                stream[i].taper(type='cosine', side='left', max_percentage=self.PercentageFromBeginning*2.0*10**-2)
            if self.SecondsFromEnd < 0:
                stream[i].taper(type='cosine', side='right', max_percentage=self.PercentageFromEnd*2.0*10**-2)
        return stream


class WaveformTaper:
    """ Waveform Taper at the beginning and at the end of the trace """

    def __init__(self, PercentageFromBeginning, PercentageFromEnd,TaperType):
        self.PercentageFromEnd = PercentageFromEnd
        self.PercentageFromBeginning = PercentageFromBeginning
        self.TaperType = TaperType

    def apply(self, stream):
        """ 
        Apply the waveform taper
            Args:
                stream(StreamObject)
            Returns:
                stream with tapered amplitudes
        """
        stream.taper(type=self.TaperType, side='both', max_percentage=self.PercentageFromEnd*2.0*10**-2)
        #stream.taper(type=self.TaperType, side='left', max_percentage=self.PercentageFromBeginning*2*10**-2)

        return stream

class WaveformTaperLeft:
    """ Waveform Taper at the beginning of the trace """

    def __init__(self, PercentageFromBeginning, TaperType):
        self.PercentageFromBeginning = PercentageFromBeginning
        self.TaperType = TaperType

    def apply(self, stream):
        """ 
        Apply the waveform taper
            Args:
                stream(StreamObject)
            Returns:
                stream with tapered amplitudes
        """
        stream.taper(type=self.TaperType, side='left', max_percentage=self.PercentageFromBeginning*2.0*10**-2)
        stream.taper(type=self.TaperType, side='right', max_percentage=self.PercentageFromEndprint*2.0*10**-2)

        return stream


class WaveformPadding:
    """ Waveform Padding """

    def __init__(self, filterorder, lcfilter):
        self.filterorder = filterorder
        self.lcfilter = lcfilter

    def apply(self, stream):
        """ 
        Apply the waveform padding
            Args:
                stream(StreamObject)
            Returns:
                stream after waveform padding
        """
        Tpad = 1.5 * self.filterorder / min(self.lcfilter)
        time2add = int(Tpad / 2)
        stream.trim(starttime=stream[0].stats.starttime - time2add, endtime=stream[0].stats.endtime + time2add,
                    pad=True, fill_value=0)

        return stream


class WaveformFilter:
    """ Waveform filtering """

    def __init__(self, filterorder, filtertype, lcfilter, hcfilter):
        self.filterorder = filterorder
        self.filtertype = filtertype
        self.lcfilter = lcfilter
        self.hcfilter = hcfilter

    def apply(self, stream):
        """ 
        Apply a filter to the waveforms
            Args:
                stream(StreamObject)
            Returns:
                stream after waveform filtering
        """
        for i in range(len(stream)):
            stream[i].filter(type=self.filtertype, freqmin=self.lcfilter[i], freqmax=self.hcfilter[i],
                             corners=self.filterorder, zerophase=True)

        return stream


class WaveformMultiplierFactor:
    """ Multyply Waveform for a factor """

    def __init__(self, coeff):
        self.coeff = coeff

    def apply(self, stream):
        """ 
        Apply the multiplier factor to the waveform
            Args:
                stream(StreamObject)
            Returns:
                stream after the application of the multiplier factor
        """
        head1 = stream[0][0].stats
        head2 = stream[1][0].stats
        head3 = stream[2][0].stats
        trace1 = Trace(np.multiply(stream[0][0].data, self.coeff), head1)
        trace2 = Trace(np.multiply(stream[1][0].data, self.coeff), head2)
        trace3 = Trace(np.multiply(stream[2][0].data, self.coeff), head3)
        stream = Stream(traces=[trace1, trace2, trace3])

        return stream
