#!/usr/bin/env python3

def upgrade_asdf_wav_dict(asdf_wav_dict):
    """
    Update a dictionary with the auxiliary data
    Args:
        asdf_wav_dict(dict)
    Returns:
        the updated dictionary
    """  
    if not 'seconds_to_cut_from_beginning' in asdf_wav_dict.keys():
        
        user1_set = 0
        if asdf_wav_dict['user1']:
            user1_str = asdf_wav_dict['user1']
            user1_set = 1
        if asdf_wav_dict['processing_commandline']:
            user1_str = asdf_wav_dict['processing_commandline']
            user1_set = 1
        
        if user1_set == 1:
            pro_list = user1_str.replace("="," ").replace("  "," ")\
            .replace("--cut-a","--ca").replace("--cut-z","--cz")\
            .replace("--taper-a","--ta").replace("--taper-z","--tz")\
            .replace("--taper-z-vd","--tzvd").split(" ")
            
            try: ca = pro_list[pro_list.index("--ca") + 1]
            except: ca = None
            
            try: cz = pro_list[pro_list.index("--cz") + 1]
            except: cz = None
            
            try: ta = pro_list[pro_list.index("--ta") + 1]
            except: ta = None
            
            try: tz = pro_list[pro_list.index("--tz") + 1]
            except: tz = None
            
            if asdf_wav_dict['late_normal_triggered'] == 'LT':
                try: tzvd = pro_list[pro_list.index("--tzvd") + 1]
                except: tzvd = None
            else: tzvd = None
            
            asdf_wav_dict[u'seconds_to_cut_from_beginning'] = ca
            asdf_wav_dict[u'seconds_to_cut_from_end'] = cz
            asdf_wav_dict[u'taper_at_beginning'] = ta
            asdf_wav_dict[u'taper_at_end'] = tz
            asdf_wav_dict[u'taper_at_end_late_triggered_vel_dis'] = tzvd

            if "processing.py" in user1_str:
                asdf_wav_dict[u'processing_commandline'] = user1_str
                asdf_wav_dict[u'user1'] = ''
        else:
            asdf_wav_dict[u'seconds_to_cut_from_beginning'] = ''
            asdf_wav_dict[u'seconds_to_cut_from_end'] = ''
            asdf_wav_dict[u'taper_at_beginning'] = ''
            asdf_wav_dict[u'taper_at_end'] = ''
            asdf_wav_dict[u'taper_at_end_late_triggered_vel_dis'] = ''
            asdf_wav_dict[u'processing_commandline'] = ''
            
    
    return asdf_wav_dict
