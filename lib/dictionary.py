import json
        
class JSON:
    """ Manage JSON files. """
    
    def __init__(self,path,filename):

        self.path = path
        self.filename = filename

    def loadJSON(self):
        """ load JSON file """
        with open(self.path + '/' + self.filename) as f:
            dictn = json.load(f)
        return dictn        
        
    def get_settings(self):
        """ Get settings from JSON """
        with open(self.path + '/' + self.filename) as data_file:
            data = json.load(data_file)
            if data['settings']: 
                return data['settings']

    def get_data(self):
        """ Get attributes from JSON """
        with open(self.path + '/' + self.filename) as data_file:
            data = json.load(data_file)
            if data['data']['quality']:
              return data['data']['quality']       
    
    def merge_two_dicts(self,x, y):
        """ 
        Merge two dictionaries
            Args:
                x(dict):
                    first dictionary
                y(dict):
                    second dictionary
                
            Returns:
                merge x and y dictionaries as a shallow copy
        """
        z = {}
        z.update(x)
        z.update(y)
        return z           
    
    def makeJSON(self,dictn,val):
        """ 
        Generating json with processing info
        Args:
            dictn(dict): dictionary with processing info
            val(int)
        Returns:
            a JSON file
        """
        try:
            with open(self.path + '/' + self.filename, 'w') as fp:
                json.dump(dictn, fp, indent=val,sort_keys=False)
        except Exception as e:
            print('generating json with processing', str(e))             
