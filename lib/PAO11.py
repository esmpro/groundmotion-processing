#!/usr/bin/env python3
from obspy.core import UTCDateTime
import numpy as np
from copy import deepcopy
import time

from lib.waveforms import WaveformMultiplierFactor, WaveformResampling, WaveformDetrend, \
    WaveformInstrCorr, WaveformTaperPAO11, WaveformTaper, WaveformPadding, WaveformFilter, WaveformIntegrate, WaveformDifferentiate, \
    WaveformCutting
from lib.utilities import SecondToCut, WaveformNpts, WaveformsDelta, \
    IdentifyNormalLate, ProcessingOutput, create_output_dirs
from lib.spectra import WaveformSpectra, WaveformFFT
from lib.asdf_file import ASDFFile

#from lib.dictionary import JSON
#load_json = JSON('/PATH_INP', 'example_PAO11.json')
#dictn = load_json.loadJSON()
#filename = '/PATH_INP/example.h5'

base_output_path = './output/' + str(round(time.time()))
create_output_dirs(base_output_path)

def PAO11processing(dictn, filename):
    """
    Compute the PAO11 Processing (PAO11 - Paolucci et al., 2011).

	Args:
	    dictn(dict):
		    dictionary with the settings for processing.
		filename(str):
		    path to file with the unprocessed waveforms.
            	
	Returns:
	    a tuple. If the processing succeeds it contains the filename of the JSON file with processing info and None.
            If the processing does not succeed it contains the filename of h5 file and errors.
    """


    errors = {}

    ptype = dictn.get('workflow')['modality'] + dictn.get('workflow')['method']

    # read the uncorrected accelerogram stored in ESM ASDF volumes; the filename are identified by a sequence of tags: \
    # net_code + station_code + location_code + event_id

    try:
        asdf_file = ASDFFile(filename)
        if asdf_file.is_valid() is True:
            pass
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('get_asdf_volume', str(e))
        errors.update({'get_asdf_volume' : str(e)})
    try:
        wfs_stream = asdf_file.get_stream(asdf_file.get_station_ids()[0], asdf_file.get_event_ids()[0])
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('get_stream', str(e))
        errors.update({'get_stream' : str(e)})
    try:
        wfs_headers = asdf_file.get_header(asdf_file.get_station_ids()[0], asdf_file.get_event_ids()[0])
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('get_header', str(e))
        errors.update({'get_header' : str(e)})
    try:
        multiplier_factor = WaveformMultiplierFactor(dictn.get('settings')['AmplitudeScaling']['MultiplierFactor'])
        wfs_stream = multiplier_factor.apply(wfs_stream)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('WaveformMultiplierFactor', str(e))
        errors.update({'WaveformMultiplierFactor' : str(e)})
      
    # waveform cutting
    try:
        starttime = dictn['settings']['WaveformCut']['StartTime']
        endtime = dictn['settings']['WaveformCut']['EndTime']
        seconds_to_cut = SecondToCut(UTCDateTime(dictn.get('settings')['WaveformCut']['StartTime']),
                                 UTCDateTime(dictn.get('settings')['WaveformCut']['EndTime']), wfs_stream)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('WaveformWindowing', str(e))
        errors.update({'WaveformWindowing' : str(e)})                             
     
    try:
        waveform_cut = WaveformCutting(UTCDateTime(dictn.get('settings')['WaveformCut']['StartTime']), UTCDateTime(dictn.get('settings')['WaveformCut']['EndTime']))
        wfs_stream = waveform_cut.apply(wfs_stream)
        wfs_cut = deepcopy(wfs_stream)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('WaveformCutting', str(e))
        errors.update({'WaveformCutting' : str(e)})
    # baseline correction / linear detrend (1st order)
    try:
        waveform_detrend = WaveformDetrend('linear')
        wfs_stream = waveform_detrend.apply(wfs_stream)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('WaveformDetrendLinear', str(e))
        errors.update({'WaveformDetrendLinear' : str(e)})
        
    # downsampling to 0.005 secs if required
    try:
        waveform_resampling = WaveformResampling()
        wfs_stream = waveform_resampling.apply(wfs_stream)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('WaveformResampling', str(e))
        errors.update({'WaveformResampling' : str(e)})
        

    try:
        [r1, D1, D2, D3] = IdentifyNormalLate(wfs_stream)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('IdentifyNormalLate', str(e))
        errors.update({'IdentifyNormalLate' : str(e)})
    
    # check for instrumental correction of Analog data 
    try:
        if asdf_file.is_digital(asdf_file.get_station_ids()[0], asdf_file.get_event_ids()[0]) is True:
            dictn.get('workflow')['type'] = 'D'
            dictn['workflow']['trigger'] = 'NT'
            # taper asimmetrico accelerazione
            try:
                waveform_taper = WaveformTaperPAO11(dictn.get('settings')['WaveformTaper']['side']['left'],
                                                    dictn.get('settings')['WaveformTaper']['side']['right'],
                                                    seconds_to_cut[0][0], seconds_to_cut[1][0],
                                                    dictn.get('workflow')['trigger'], r1, D1, D2, D3)
                wfs_stream = waveform_taper.apply(wfs_stream)
            except Exception as e:
                if dictn.get('workflow')['verbose']: print('WaveformTaperPAO11 NT', str(e))
                errors.update({'WaveformTaperPAO11 NT' : str(e)})
                
        else:
            # instrumental correction
            try:
                instrumental_correction = WaveformInstrCorr(WaveformNpts(wfs_stream), WaveformsDelta(wfs_stream),
                                                            asdf_file.get_station_ids()[0], asdf_file.get_event_ids()[0])
                wfs_stream = instrumental_correction.apply(wfs_stream)
            except Exception as e:
                if dictn.get('workflow')['verbose']: print('WaveformInstrCorr', str(e))
                errors.update({'WaveformInstrCorr' : str(e)})
                
            try:
                waveform_taper = WaveformTaperPAO11(dictn.get('settings')['WaveformTaper']['side']['left'],
                                                    dictn.get('settings')['WaveformTaper']['side']['right'],
                                                    seconds_to_cut[0][0], seconds_to_cut[1][0],
                                                    dictn.get('workflow')['trigger'], r1, D1, D2, D3)
                wfs_stream = waveform_taper.apply(wfs_stream)
            except Exception as e:
                if dictn.get('workflow')['verbose']: print('IdentifyNormalLate LT', str(e))
                errors.update({'IdentifyNormalLate LT' : str(e)})
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('AnalogData', str(e))
        errors.update({'AnalogData' : str(e)})

    try:
        waveform_padding = WaveformPadding(dictn.get('settings')['FourierSpectrumFilter']['order'],
                                           dictn.get('settings')['FourierSpectrumFilter']['LowCut'])
        wfs_stream = waveform_padding.apply(wfs_stream)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('WaveformPadding', str(e))
        errors.update({'WaveformPadding' : str(e)})
    

    #: acausal passband Butterwoth filter
    try:
        waveform_filter = WaveformFilter(dictn.get('settings')['FourierSpectrumFilter']['order'],
                                         dictn.get('settings')['FourierSpectrumFilter']['Type'],
                                         dictn.get('settings')['FourierSpectrumFilter']['LowCut'],
                                         dictn.get('settings')['FourierSpectrumFilter']['HighCut'])
        wfs_stream = waveform_filter.apply(wfs_stream)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('WaveformFilter', str(e))
        errors.update({'WaveformFilter' : str(e)})
    

    #: for NT records re-establishes the original time scale
    if dictn.get('workflow')['trigger'] == 'NT':
        try:
            waveform_cut = WaveformCutting(UTCDateTime(dictn.get('settings')['WaveformCut']['StartTime']),
                                           UTCDateTime(dictn.get('settings')['WaveformCut']['EndTime']))
            wfs_stream = waveform_cut.apply(wfs_stream)
        except Exception as e:
            if dictn.get('workflow')['verbose']: print('WaveformCutting', str(e))
            errors.update({'WaveformCutting' : str(e)})

    #: computation of velocity
    try:
        displacement_integrate = WaveformIntegrate('cumtrapz')
        velocity = displacement_integrate.apply(wfs_stream)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('computation of velocity ', str(e))
        errors.update({'computation of velocity' : str(e)})

    try:
        waveform_detrend = WaveformDetrend('linear')
        velocity = waveform_detrend.apply(velocity)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('WaveformDetrendLinear', str(e))
        errors.update({'WaveformDetrendLinear' : str(e)})

    try:
        waveform_taper = WaveformTaper(dictn.get('settings')['WaveformTaper']['side']['left'],
                                            dictn.get('settings')['WaveformTaper']['side']['right'],dictn.get('settings')['WaveformTaper']['Type'])
        velocity = waveform_taper.apply(velocity)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('IdentifyNormalLate', str(e))
        errors.update({'IdentifyNormalLate' : str(e)})

    #: computation of displacement
    try:
        velocity_integrate = WaveformIntegrate('cumtrapz')
        displacement = velocity_integrate.apply(velocity)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('computation of displacement', str(e))
        errors.update({'computation of displacement' : str(e)})

    try:
        waveform_detrend = WaveformDetrend('linear')
        displacement = waveform_detrend.apply(displacement)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('WaveformDetrendLinear', str(e))
        errors.update({'WaveformDetrendLinear' : str(e)})

    try:
        waveform_taper = WaveformTaper(dictn.get('settings')['WaveformTaper']['side']['left'],
                                            dictn.get('settings')['WaveformTaper']['side']['right'],dictn.get('settings')['WaveformTaper']['Type'])
        displacement = waveform_taper.apply(displacement)
        #: make a copy of the displacement stream
        _displacement = deepcopy(displacement)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('computation of displacement', str(e))
        errors.update({'computation of displacement' : str(e)})

    #: computation of velocity
    try:
        displacement_differentiate = WaveformDifferentiate('gradient')
        velocity = displacement_differentiate.apply(displacement)
        #: make a copy of the velocity stream
        _velocity = deepcopy(velocity)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('computation of velocity', str(e))
        errors.update({'computation of velocity' : str(e)})

    #: computation of acceleration
    try:
        velocity_differentiate = WaveformDifferentiate('gradient')
        acceleration = velocity_differentiate.apply(velocity)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('computation of acceleration ', str(e))
        errors.update({'computation of acceleration' : str(e)})

    #: make a copy of the acceleration stream
    try:
        _acceleration = deepcopy(acceleration)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('copy acceleration',str(e))
        errors.update({'copy acceleration' : str(e)})
    # computation of response Spectra
    periods = np.array(dictn.get('data')['periods'])
    damping_val = np.array(dictn.get('data')['damping'])
    
    try:
        waveform_spectra = WaveformSpectra(periods, damping_val)
        Sa, Sd, channel = waveform_spectra.apply(wfs_stream)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('WaveformSpectra', str(e))
        errors.update({'WaveformSpectra' : str(e)})
       
    #: computation of FFT
    try:
        waveform_fft = WaveformFFT()
        frequency, FFTamplitude = waveform_fft.apply(acceleration)
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('WaveformFFT', str(e))
        errors.update({'WaveformFFT' : str(e)})
    try:
        out = ProcessingOutput(base_output_path,filename,dictn, errors, ptype, asdf_file, seconds_to_cut, _acceleration,_velocity, _displacement, periods, Sa, Sd, frequency, FFTamplitude)
        return out 
    except Exception as e:
        if dictn.get('workflow')['verbose']: print('OUTPUT', str(e))
        errors.update({'OUTPUT' : str(e)})
        return [filename.replace('.h5','_' + ptype + '.h5'),errors]     
       

#if __name__ == "__main__":
#    PAO11processing(dictn, filename)
