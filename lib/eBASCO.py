#!/usr/bin/env python3
from obspy.core import UTCDateTime
import numpy as np
from copy import deepcopy
import time

from lib.asdf_file import ASDFFile
from lib.waveforms import WaveformMultiplierFactor,WaveformCutting,WaveformDetrend,WaveformIntegrate
from lib.utilities import WaveformsDelta2, SampleCorrectionPoints, PreEvent, DetrendPreEvent, EndPoint, eBASCOvelocity, AccCorr, FindSolutions, \
	 FindBestSolution, ArrayToStream, SecondToCut, ProcessingOutput, create_output_dirs
from lib.spectra import WaveformSpectra,WaveformFFT

#from lib.dictionary import JSON
#load_json = JSON('/PATH_INP', 'example_eBASCO.json')
#dictn = load_json.loadJSON()
#filename = '/PATH_INP/example.h5'

base_output_path = './output/' + str(round(time.time()))
create_output_dirs(base_output_path)

def eBASCOprocessing(dictn, filename):
	"""
	Compute the eBASCO Processing (Schiappapietra et al., 2021).

	Args:
	    dictn(dict):
		    dictionary with the settings for processing.
		filename(str):
		    path to file with the unprocessed waveforms.
	
	Returns:
	    a tuple. If the processing succeeds it contains the filename of the JSON file with processing info and None.
	    If the processing does not succeed it contains the filename of h5 file and errors.

	"""

	errors = {}
    
	ptype = dictn.get('workflow')['modality'] + dictn.get('workflow')['method']
    
    # read the uncorrected accelerogram stored in ESM ASDF volumes; the filename are identified by a sequence of tags: \
    # net_code + station_code + location_code + event_id
	
	asdf_file = ASDFFile(filename)
	
	try:
		asdf_file = ASDFFile(filename)
		if asdf_file.is_valid() is True:
			pass
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('get_asdf_volume', str(e))
		errors.update({'get_asdf_volume' : str(e)})
    
	try:
		wfs_stream = asdf_file.get_stream(asdf_file.get_station_ids()[0], asdf_file.get_event_ids()[0])
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('get_stream', str(e))
		errors.update({'get_stream' : str(e)})

	try:
		wfs_headers = asdf_file.get_header(asdf_file.get_station_ids()[0], asdf_file.get_event_ids()[0])
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('get_header', str(e))
		errors.update({'get_header' : str(e)})
        
	try:
		multiplier_factor = WaveformMultiplierFactor(dictn.get('settings')['AmplitudeScaling']['MultiplierFactor'])
		wfs_stream = multiplier_factor.apply(wfs_stream)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('WaveformMultiplierFactor', str(e))
		errors.update({'WaveformMultiplierFactor' : str(e)})
        

    # waveform cutting
	starttime = dictn['settings']['WaveformCut']['StartTime']
	endtime = dictn['settings']['WaveformCut']['EndTime']
	seconds_to_cut = SecondToCut(UTCDateTime(dictn.get('settings')['WaveformCut']['StartTime']),
                                 UTCDateTime(dictn.get('settings')['WaveformCut']['EndTime']), wfs_stream)
	try:
		waveform_cut = WaveformCutting(UTCDateTime(dictn.get('settings')['WaveformCut']['StartTime']), UTCDateTime(dictn.get('settings')['WaveformCut']['EndTime']))
		wfs_stream_ = waveform_cut.apply(wfs_stream)
		wfs_cut = deepcopy(wfs_stream_)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('WaveformCutting', str(e))
		errors.update({'WaveformCutting' : str(e)})   

	#: baseline correction / linear detrend (1st order)
	try:
		waveform_detrend = WaveformDetrend('linear')
		wfs_stream = waveform_detrend.apply(wfs_stream)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('WaveformDetrendSimple', str(e))
		errors.update({'WaveformDetrendSimple' : str(e)})
	
	#: sample the T1 time before the 5% of the cumulated energy (ARIAS INTENSITY)
	try:
		t1_sample = SampleCorrectionPoints(wfs_stream,dictn.get('settings')['WaveformDetrend']['T1Samples'],0.0001, 0.05)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('T1 sampling',str(e))
		errors.update({'T1 sampling' : str(e)})
	
	#: sample T3 time after the 5% of the cumulated energy (ARIAS INTENSITY)	
	try:
		t3_sample = SampleCorrectionPoints(wfs_stream,dictn.get('settings')['WaveformDetrend']['T3Samples'],0.05, 0.95)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('SamplingT3',str(e))
		errors.update({'SamplingT3' : str(e)})
	
	#: make a copy of the uncorrected acceleration
	uncorrected_acceleration = deepcopy(wfs_stream)
	
	# computation of velocity 
	try:
		acceleration_integrate = WaveformIntegrate('cumtrapz')
		velocity = acceleration_integrate.apply(wfs_stream)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('computation of velocity ', str(e))
		errors.update({'computation of velocity' : str(e)})
	
	uncorrected_velocity = deepcopy(velocity)
	
	#: calculate the sampling rate
	try:
		delta = WaveformsDelta2(velocity)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('sampling rate',str(e))
		errors.update({'sampling rate' : str(e)})
		
	#: define the "pre-event" between the first sample T0 and T1  (velocity)
	try:
		time_pre,vel_pre = PreEvent(velocity,t1_sample)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('PreEvent',str(e))
		errors.update({'PreEvent' : str(e)})
	
	#: linear de-trend in the "pre-event"
	try:
		pre_eve_vel_det, last_value_lin_fit, lin_fit = DetrendPreEvent(time_pre,vel_pre)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('PreEvent Detrend',str(e))
		errors.update({'PreEvent Detrend' : str(e)})
	
	#: search the end points for each component
	try:
		end_point = EndPoint(velocity)		
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('search the end points',str(e))
		errors.update({'search the end points' : str(e)})
	
	#: sample T2 time from each T3 and the end of the signal and detrend the velocity trace in the time windows start-T1, T1-T2, T3-Tend
	#print(len(pre_eve_vel_det))
	try:
		T1, T2, T3, time, vel_corr = eBASCOvelocity(uncorrected_velocity,t1_sample,dictn.get('settings')['WaveformDetrend']['T2Samples'],t3_sample,end_point,last_value_lin_fit,time_pre,pre_eve_vel_det)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('eBASCOvelocity',str(e))
		errors.update({'eBASCOvelocity' : str(e)})
	
	try:
		acceleration_corrected,time_ok,t3_ok,index_ok,delta = AccCorr(uncorrected_acceleration,vel_corr,time,T1,T2,T3,0.25)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('Corrected Acceleration',str(e))
		errors.update({'Corrected Acceleration' : str(e)})
	
	#: calculate VEL and DISP for each acceptable ACC
	try:
		vel_corr_good,disp_corr_good = FindSolutions(acceleration_corrected,delta)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('FindSolution',str(e))
		errors.update({'FindSolution' : str(e)})
				
					
	#: identify the best solution througth the flatness indicator
	try:		
		st_acc,st_vel,st_dis,T1_good,T2_good,T3_good = FindBestSolution(acceleration_corrected,time_ok,disp_corr_good,t3_ok,delta,dictn.get("settings")["WaveformTaper"]["side"]["left"],dictn.get("settings")["FourierSpectrumFilter"]["order"],dictn.get("settings")["FourierSpectrumFilter"]["HighCut"],T1,T2,T3)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('FindBestSolution',str(e))
		errors.update({'FindBestSolution' : str(e)})
	
	#: make a stream for eBASCO acceleration	
	try:
		acceleration_eb = ArrayToStream(st_acc,velocity)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('eBASCO ACC',str(e))
		errors.update({'eBASCO ACC' : str(e)})
	
	#: make a stream for eBASCO velocity	
	try:
		velocity_eb = ArrayToStream(st_vel,velocity)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('eBASCO VEL',str(e))
		errors.update({'eBASCO VEL' : str(e)})
		
	#: make a stream for eBASCO displacement	
	try:
		displacement_eb = ArrayToStream(st_dis,velocity)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('eBASCO DIS',str(e))
		errors.update({'eBASCO DIS' : str(e)})
		
	#: computation of response Spectra
	periods = np.array(dictn.get('data')['periods'])
	damping_val =  np.array(dictn.get('data')['damping'])
	try:
		waveform_spectra = WaveformSpectra(periods,damping_val)
		Sa,Sd,channel = waveform_spectra.apply(acceleration_eb)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('WaveformSpectra', str(e))
		errors.update({'WaveformSpectra' : str(e)})
	
	#: computation of FFT
	try:
		waveform_fft = WaveformFFT()
		frequency,FFTamplitude = waveform_fft.apply(acceleration_eb)
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('WaveformFFT', str(e))
		errors.update({'WaveformFFT' : str(e)})
		
	if dictn['settings']['WaveformTaper']['side'] != None:
		new_entry = {'left': 5, 'right': None}
		dictn['settings']['WaveformTaper']['side'] = new_entry
		
		#: manage the .h5 and .json output files
	try:
		out = ProcessingOutput(base_output_path,filename,dictn, errors, ptype, asdf_file, seconds_to_cut, acceleration_eb, velocity_eb, displacement_eb, periods, Sa, Sd, frequency, FFTamplitude)
		return out 
	except Exception as e:
		if dictn.get('workflow')['verbose']: print('OUTPUT', str(e))
		errors.update({'OUTPUT' : str(e)})
		return [filename.replace('.h5','.processing.' + ptype + '.h5'),errors] 

			
#if __name__ == "__main__":
#  eBASCOprocessing(dictn,filename)
