#!/usr/bin/env python3
import numpy as np
import os
import platform
if str(platform.system()) == 'Linux':
	os.system('cp ./utils/resp_spectra_f64_py3_unix.so   ./utils/resp_spectra_f64_py3.so')
	import utils.resp_spectra_f64_py3 as resp_spectra_f64
elif str(platform.system()) == 'Darwin':
	print(str(platform.system()))
	os.system('cp ./utils/resp_spectra_f64_py3_mac.so   ./utils/resp_spectra_f64_py3.so')
	import utils.resp_spectra_f64_py3 as resp_spectra_f64

class WaveformSpectra:
	"""
	Compute Response Spectra
	"""
	def __init__(self,periods,damping):
		self.periods = periods
		self.damping = damping
	
	def apply(self,stream):
		"""
		Application of WaveformSpectra
		Args:
			stream(StreamObject)
		Returns:
			list of response spectra in acceleration and displacement, along with the corresponding channel
		"""
		Sd = []
		Sa = []
		npts = [stream[0].stats.npts,stream[1].stats.npts,stream[2].stats.npts]
		delta = [stream[0].stats.delta,stream[1].stats.delta,stream[2].stats.delta]
		channel = {"CH1":None,"CH2":None,"CH3":None}
		for i in range(len(stream)):
			xsd,xsa,std,sta = resp_spectra_f64.pcn05(npts[i],len(self.periods),delta[i],self.damping,self.periods,stream[i].data)
			Sa.append(xsa)
			Sd.append(xsd)
			channel[list(channel.keys())[i]]=stream[i].stats.channel

		return Sa,Sd,channel


class WaveformFFT:
	"""
	Compute Fourier Spectra
	"""
	def apply(self,stream):
		"""
		Application of WaveformFFT
		Args:
			stream(StreamObject)
		Returns:
			two lists, one for the frequencies, the other for the FFT amplitudes
		"""
		FFT = []
		freq = []		
		channel = {"CH1":None,"CH2":None,"CH3":None}
		for i in range(len(stream)):
			npts = int(np.exp2(np.ceil(np.log2(stream[i].stats.npts))))
			fs = stream[i].stats.sampling_rate
			outst = np.fft.fft(stream[i].data, n=int(npts))
			outst = np.abs(outst[0:int(npts/2)]/fs)
			freq_out = fs*np.arange(0,npts/2)/npts
			delta_f = fs/npts; npts_fin = int(npts/2)
			FFT.append(outst)
			freq.append(freq_out)
	
		return freq,FFT
