#!/usr/bin/env python3
from obspy.core import UTCDateTime
import unicodedata
import re
from math import trunc as math_trunc
from numpy import log10 as np_log10
try:
    long
except NameError:
    long = int

import sys
if sys.version_info[0] >= 3:
    unicode = str


def dict_fmt(exit_status, exit_message=None, in_file=None, out_string=None, out_object=None, out_file=None, out_format=None, out_file_content=None):
    """
    Return a feedback message 
    Args:
        exit_status(int)
        exit_message(str)
        in_file(str)
        out_string(str)
        out_object(PythonObject): can be used to return a list, dict or any other python object
        out_file(str)
        out_format(str)
        out_file_content(str)
    Returns:
        python dictionary with feedback message
    """
    return { \
    'exit_status': exit_status, \
    'exit_message': exit_message, \
    'in_file': in_file, \
    'out_string': out_string, \
    'out_object': out_object, \
    'out_file': out_file, \
    'out_format': out_format, \
    'out_file_content': out_file_content
    }


def u_val(val):
    """ Convert val from int/float/long to str """
    if isinstance(val, int) or isinstance(val, float) or isinstance(val, long):
        val = str(val)
    else:
        if val is None: val = ''
    return val


def return_decimals(f_value):
    """ Return decimals for graphical plot """
    pg_int = math_trunc(np_log10(f_value)); pg_decimals = '0'
    if pg_int < 0: pg_decimals = str(abs(pg_int))
    return pg_decimals


def fromUTCDateTime(dt, withZ=False, withMS=True):
    """
    Convert from UTCDateTime to string
    Args:
        dt(UTCDateTime): UTCDateTime to be converted
        withZ(bool): format type
        withMS(bool): format type for seconds 
    Returns:
        string 
    """
    
    if withZ and withMS:
        pattern = "%4d-%02d-%02dT%02d:%02d:%02d.%03dZ"
    elif withZ and not withMS:
        pattern = "%4d-%02d-%02dT%02d:%02d:%02dZ"
    elif not withZ and not withMS:
        pattern = "%4d%02d%02d_%02d%02d%02d"
    elif not withZ and withMS:
        pattern = "%4d%02d%02d_%02d%02d%02d.%03d"
        
    if withMS: out_str = pattern % (dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, dt.microsecond / 1000)
    else: out_str = pattern % (dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second)
    
    return out_str


def fromUTCDateTime2time(dt):
    """
    Get time from UTCDateTime
    Args:
        dt(UTCDateTime): UTCDateTime to be converted
    Returns:
        string formatted with a specific pattern
    """
    pattern = "%02d:%02d:%02d"
    return pattern % (dt.hour, dt.minute, dt.second)


def toUTCDateTime(value):
    """
    Convert time stamp from string to UTCDateTime
    Args:
        value(str): time stamp
    Returns:
        UTCDateTime 
    """
    year = int(value[0:4]); month = int(value[5:7]); day = int(value[8:10])
    hour = int(value[11:13]); mins = int(value[14:16]); secs = int(value[17:19])
    return UTCDateTime(year, month, day, hour, mins) + secs


#-------------------------------------------------------
try:
    # Python 2
    xrange
except NameError:
    # Python 3, xrange is now named range
    xrange = range
try:
    unichr
except NameError:
    unichr = chr
    
all_chars = (unichr(i) for i in xrange(0x110000))
control_chars = ''.join(c for c in all_chars if unicodedata.category(c) == 'Cc')
# or equivalently and much more efficiently
#control_chars = ''.join(map(unichr, range(0,32) + range(127,160)))
control_char_re = re.compile('[%s]' % re.escape(control_chars))
