#!/usr/bin/env python3

import sys
import argparse
import os.path
import json

from lib.format_utils import dict_fmt
from lib.asdf_utils import valid_true_false, is_valid_file, valid_asdf_r

from lib.workflow import Workflow
from lib.dictionary import JSON

class ArgumentParserError(Exception): pass
class CustomArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise ArgumentParserError(message)

def processing(opts_str):
    """
    Processes the input data with three alternative methods: PAO11 (Paolucci et al., 2011), eBASCO (Schiappapietra et al., 2021) or CUSTOM.
    This is performed based on the configuration file provided by preprocessing.py. 
        Args:
        input_file (str):
            path to the file with the unprocessed waveforms.
        configuration_file (str):
            path to configuration file. It is the output of preprocessing steps.
        verbose (bool):
            prints summary and other details (or None). Default is True.

    Returns:
        An ASDF volume with the processed data and a JSON with the exit message.
    """
    
    synopsis = 'Process waveforms in asdf file with Paolucci et al. 2011 (PAO11), Schiappapietra et al. 2021 (eBASCO), or User defined processing workflow'
    usage = "example: %prog --input=[INPUT_DIR]/example.h5 --config=[INPUT_DIR]/config.json"
    
    p = argparse.ArgumentParser(description=synopsis + '\n' + usage)
    
    p.add_argument("--input", "--input_file", action="store", dest="ifile", type=valid_asdf_r, required=True, help="input h5 file")

    p.add_argument("--config", "--configuration_file", action="store", dest="config", required=True,   help="configuration file in JSON format containing metadata to identify the waveform to be processed within the ASDF volume and additional information.")
    
    p.add_argument("--v", "--verbose", action="store", dest="verb", type=valid_true_false, default='True', help="Verbose mode (prints summary and other details). Default: True")
    
    opts_str_lst = opts_str.replace("'", "").replace('=',' ').split(' ')
        
    try: opts = p.parse_args(opts_str_lst) 
    except ArgumentParserError as ap_err: return dict_fmt(exit_status=2, exit_message=str(ap_err))    

    path = os.path.dirname(opts.ifile[0]) 
    if opts.verb: print ("path: ", path)
    
    filename_h5 = os.path.basename(opts.ifile[0])
    if opts.verb: print ("filename_h5: ", filename_h5)
    
    filename_json = opts.config       
    if opts.verb: print ("filename_json: ", filename_json)
    
    try:
        check_file = is_valid_file(filename_json)
        filename_json = opts.config.split('/')[-1]
    except Exception as e:
        if opts.verb: print('check_json_file', str(e))
        return dict_fmt(exit_status=6, exit_message=str(e))
        
    try:
        if opts.verb:
            print("")
            print('about to process h5 file ' + path + '/' + filename_h5 + ' with the configuration file ' + path + '/' + filename_json)
        load_json = JSON(path, filename_json)
        dictn = load_json.loadJSON()

        dictn.get('workflow')['verbose'] = opts.verb
    except Exception as e:
        if opts.verb: print('load json', str(e))
        return dict_fmt(exit_status=6, exit_message=json.dumps(str(e)))
    
    try:
        w, dictn = Workflow.createWorkflow(dictn)
        proc_out = w.process(path + '/' + filename_h5, dictn)
        return dict_fmt(exit_status=0, in_file=opts.ifile[0], out_file=proc_out[0], out_format='hdf5')
    except Exception as e:
        if opts.verb: print('Specific Processing', str(e))
        return dict_fmt(exit_status=6, exit_message=json.dumps(str(e)))


def main():
    
    ret_code = processing(' '.join(sys.argv[1:]))
    if ret_code['exit_status'] == 0 and ret_code['out_string']: print(ret_code['out_string'])
    if ret_code['exit_message']: print(ret_code['exit_message'])
    sys.exit(ret_code['exit_status'])

if __name__ == "__main__": main()
