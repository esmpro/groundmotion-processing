# ISTITUTO NAZIONALE DI GEOFISICA E VULCANOLOGIA
<img src="logo/ESMpro.jpg"  width="800" height="300">

ESMpro is a stand-alone python software for analyzing and processing ground-motion waveforms from the Engineering Strong Motion (ESM) database.

## Engine versions
Version 1.0.0 (26/07/2022) - Beta version, the first release of code and documentation.

## Documentation
- <h3 style="font-size:16px;">[Overview](/docs/Overview.md)
- <h3 style="font-size:20px;">[Installation](/docs/Installation.md)
- <h3 style="font-size:20px;">[Getting Started](/docs/Getting-Started.md)
- <h3 style="font-size:20px;">[Workflow](/docs/Workflow.md)

## Licence
ESMpro is licensed under the GNU Lesser General Public License (LGPL) v3.0.

## Citation
Permission to use, copy or reproduce parts of the ESMpro code and/or related outputs is granted, provided that ESMpro is properly referenced as:

_D’Amico M., Mascandola C., Russo E., and Pavesi F. (2022). Engineering Strong Motion data PROcessing (ESMpro), Istituto Nazionale di Geofisica e Vulcanologia Software Release. INGV Software Release._

## Getting Help
If you need help, please browse and search this documentation. Additionally, you can submit questions, bug reports, and feature requests on the issue tracker on GitLab (https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/issues). All feedback and suggestions at **esmdb@ingv.it** are welcome.

## Disclaimer
This software is preliminary or provisional and is subject to revision.
ESMpro is provided on an As Is, with all faults, and As Available basis, without any warranties of any kind. We specifically do not warrant that ESMpro is complete, accurate, reliable, or secure in any way, suitable for, or compatible with any of your contemplated activities, devices, operating systems, browsers, software and tools, or that their operation will be free of any viruses, bugs or program limitations.
The strong motion records processed by ESMpro are devoted to qualified users. No warranty, implicit or explicit, is attached to ESMpro data and metadata. Every risk due to the improper use of data or related information is assumed by the User.

## Acknowledgements
ESMpro has been developed by the ESM Working Group of the INGV with support from JRU EPOS Italia (Engineering Strong Motion DB (ESM) - Access to waveforms & products OS1) and ORFEUS Software Development Grants - 2022. The ESMpro has benefited also from funding provided by the Italian Presidenza del Consiglio dei Ministri – Dipartimento della Protezione Civile (DPC) in the framework of the Agreement A DPC - INGV 2020-2021 (WP7.2 Banche dati sismologiche strumentali). The eBASCO processing scheme has been developed in the framework of the Research Project RS2-DPC-ReLUIS 2017 Numerical simulations of earthquakes and near-source effects.

<img src="logo/orfeus.png"  width="150" height="70"> <img src="logo/epos.png"  width="100" height="70"> <img src="logo/reluis.png"  width="150" height="70"> <img src="logo/DPC.png"  width="150" height="70">

