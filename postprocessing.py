#!/usr/bin/env python3
import argparse
import base64
import os.path
from distutils import errors
from sys import argv as sys_argv
from sys import exit as sys_exit
from os import remove as os_remove

from lib.upgrade_asdf_wav_dict import upgrade_asdf_wav_dict

from lib.plot_utils import S_plot
from lib.plot_utils import WF_plot
from lib.plot_utils import FFT_plot
from lib.plot_utils import PRE_plot

from lib.asdf_file import ASDFFile
from lib.asdf_utils import valid_true_false, valid_asdf_r
from lib.format_utils import dict_fmt
from lib.utilities import create_input_dir

class ArgumentParserError(Exception): pass
class CustomArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise ArgumentParserError(message)


def ws_plot_pro(opts_str, cur=None):
    """
    Provides different plots of the relative waveforms, Fourier and response spectra.
    
    Args:
        input_file (str):
            path to h5 file with raw and processed data.
        processing-type (str):
            type of processing. Available types are: 'MP' (manually processed using Paolucci et al., 2011), 'AP' (automatically
            processed using Paolucci et al., 2011), 'MB' (manually processed using eBASCO by Schiappapietra et al 2021), 'AB' (automatically
            processed using eBASCO by Schiappapietra et al 2021). Default: 'MP'.
        output-format (str):
            output file format. Available formats are 'PNG'and 'base64'. Default: 'PNG'.
        plot-type (str):
            type of plot. Available types are: 'PRE' (uncorrected acceleration, in cm/s^2), 'FFT' (Fourier spectra, in cm*s/s^2), 'ACC 
            (acceleration, in cm/s^2), 'VEL' (velocity, in cm/s), 'DIS' (displacement, in cm), 'SA' (acceleration response spectra, in cm/s^2
            and 'SD' (displacement response spectra, in cm). Default: 'PRE'.
        include-transparency (bool):
            add Transparency to Graphics Objects . Default: False
        plot-psarrivals (bool):
            add P and S waves arrivals to acceleration, velocity, or displacement taces. Default: False
        plot-channels (bool):
            add ground motion orientation to Graphic Objects. Default: False
        plot-annotations (bool):
            add annotations to Graphic Objects . Default: False
        plot-axes (bool):
            add axes to Graphic Objects. Default: False
        plot-grid (bool):
            add grid and axes to Graphic Objects. Default: False
        plot-peakvalues (bool):
            add ground motion peak values to the Graphic Objects. Default: False
        plot-frequencies (bool):
            add corner frequencies of the Fourier Specra filter. Default: False
        unprocessed-acceleration (bool):
            add only unprocessed wavefroms to Graphic Objects. Default: False
        outfile (str):
            output filename.
        verbose (bool):
            prints summary and other details. Default is False.
    
    Returns:
        plot in png files.

    """

    synopsis = 'Generate plots for ground motion data processing.'
    usage = "example: %prog --input=[OUTPUT_DIR][TIME_STAMP]/h5/example_processing_type.h5"
    p = argparse.ArgumentParser(description=synopsis + '\n' + usage)
        
    errors = {}

    p.add_argument("--input", "--input_file", action="store", dest="ifile", type=valid_asdf_r, required=True, help="input h5 file")

    p.add_argument("--ptype", "--processing-type", action="store", dest="processingtype", type=str, choices=['MP', 'AP','MB','AB','MC'], default='AP', help="Specify processing type for data selection; available types are: 'MP' (manually processed using Paolucci et al., 2011), 'AP' (automatically processed using Paolucci et al., 2011), 'MB' (manually processed using eBASCO by Schiappapietra et al 2021), 'AB' (automatically processed using eBASCO by Schiappapietra et al 2021). Default: 'MP'")

    p.add_argument("--format", "--output-format", action="store", dest="oformat", type=str, default='PNG', choices=['PNG', 'base64'], help="Specify format for result. Default: 'PNG'")

    p.add_argument("--plot", "--plot-type", action="store", dest="plottype", type=str, default='PRE', choices=['PRE', 'FFT', 'ACC', 'VEL', 'DIS', 'SA', 'SD'],  help="Specify the type of plot for data selection; available types are: 'PRE' (preview of acceleration, in cm/s^2), 'FFT' (Fourier spectra, in cm*s/s^2), 'ACC' (acceleration, in cm/s^2), 'VEL' (velocity, in cm/s), 'DIS' (displacement, in cm), 'SA' (acceleration response spectra, in cm/s^2) and 'SD' (displacement response spectra, in cm). Default: 'PRE'")
    
    p.add_argument('--transparency', '--include-transparency', dest='transpflag', default=False, action='store', type=valid_true_false, help='Add Transparency to Graphics Objects . Default: False')

    p.add_argument('--psarrivals', '--plot-psarrivals', dest='psflag', default=False, action='store', type=valid_true_false,  help='Add P and S waves arrivals to acceleration, velocity, or displacement taces: False')
    
    p.add_argument('--channels', '--plot-channels', dest='chflag', default=False, action='store', type=valid_true_false, help='Add ground motion orientation to Graphic Objects. Default: False')
    
    p.add_argument('--annotations', '--plot-annotations', dest='txtflag', default=False, action='store', type=valid_true_false, help='Add annotations to Graphic Objects . Default: False')
    
    p.add_argument('--axes', '--plot-axes', dest='axesflag', default=False, action='store', type=valid_true_false, help='Add axes to Graphic Objects. Default: False')
    
    p.add_argument('--grid', '--plot-grid', dest='gridflag', default=False, action='store', type=valid_true_false, help='Add grid and axes to Graphic Objects. Default: False')

    p.add_argument('--peakvalues', '--plot-peakvalues', dest='peakflag', default=False, action='store', type=valid_true_false, help='Add ground motion peak values to the Graphic Objects. Default: False')
    
    p.add_argument('--frequencies', '--plot-frequencies', dest='cornerfreq', default=False, action='store', type=valid_true_false, help='Add corner frequencies of the Fourier Specra filter. Default: False')

    p.add_argument('--unprocessed', '--unprocessed-acceleration', dest='rawflag', default=False, action='store', type=valid_true_false, help='Add only unprocessed wavefroms to Graphic Objects. Default: False')

    p.add_argument("--outfile", "--outfile", action="store", dest="outfile", type=str, default='output', help="Specify the output filename. Default: 'output'")

    p.add_argument("--v", "--verbose", action="store", dest="ver_d", type=valid_true_false, default='False', help="Verbose mode (also print selected items)")
    

    opts_str_lst = opts_str.replace("'", "").replace('=',' ').split(' ')
    
    try:
        opts = p.parse_args(opts_str_lst) 
        if opts.ver_d: print('options: ' + str(opts))
    except ArgumentParserError as ap_err:
        return dict_fmt(exit_status=2, exit_message=str(ap_err))
    
    if opts.gridflag == True and opts.axesflag == False:
        opts.axesflag = True
    
    proctype = opts.processingtype.lower()
    
    time_offset = 4
    curr_h5 = opts.ifile[0]
    
    try:
        asdf_file = ASDFFile(curr_h5)
        if asdf_file.is_valid() is True:
            pass
    except Exception as e:
        if opts.ver_d: print('get_asdf_volume', str(e))
        errors.update({'get_asdf_volume' : str(e)})    
    # tag / path definition for stream
    
    origindepth = asdf_file.get_depth()
    origintime = asdf_file.get_origin_time()
    distance = asdf_file.get_epi_dist()
    
    tag_path = asdf_file.get_tag(str(asdf_file.get_station_ids()[0]),str(asdf_file.get_event_ids()[0]))
    net_sta = asdf_file.get_station_ids()[0].replace('.','_')
    
    U_tag_path_CV = tag_path[0] 
    V_tag_path_CV = tag_path[1] 
    W_tag_path_CV = tag_path[2] 
    
    U_tag_path_XP = tag_path[0].replace('cv',proctype).replace('_acc_','_' + opts.plottype.lower() + '_')      
    V_tag_path_XP = tag_path[1].replace('cv',proctype).replace('_acc_','_' + opts.plottype.lower() + '_')      
    W_tag_path_XP = tag_path[2].replace('cv',proctype).replace('_acc_','_' + opts.plottype.lower() + '_')      

    path_out = os.path.dirname(opts.ifile[0]).replace('/h5','/fig/')
    opts.outfile = path_out

    create_input_dir(opts.outfile)
    if opts.plottype == 'PRE':
        U_data_CV = opts.ifile[1].waveforms[net_sta][U_tag_path_CV]
        V_data_CV = opts.ifile[1].waveforms[net_sta][V_tag_path_CV]
        W_data_CV = opts.ifile[1].waveforms[net_sta][W_tag_path_CV]

        PRE_plot(U_data_CV,V_data_CV,W_data_CV,opts.outfile + 'PRE',opts.transpflag)

    elif opts.plottype == 'ACC' or opts.plottype == 'VEL' or opts.plottype == 'DIS':

        U_data_CV = opts.ifile[1].waveforms[net_sta][U_tag_path_CV]
        V_data_CV = opts.ifile[1].waveforms[net_sta][V_tag_path_CV]
        W_data_CV = opts.ifile[1].waveforms[net_sta][W_tag_path_CV]
        
        U_data_XP = opts.ifile[1].waveforms[net_sta][U_tag_path_XP]
        V_data_XP = opts.ifile[1].waveforms[net_sta][V_tag_path_XP]
        W_data_XP = opts.ifile[1].waveforms[net_sta][W_tag_path_XP]
        
        try:
            WF_plot(U_data_CV,V_data_CV,W_data_CV,U_data_XP,V_data_XP,W_data_XP,opts,time_offset, cur, opts.processingtype,opts.transpflag,proctype,origindepth, origintime, distance)
        except Exception as e:
            errors.update({'plot time series',str(e)})
                    
    elif opts.plottype == 'FFT':
            try:
                U_aux_XP = upgrade_asdf_wav_dict(opts.ifile[1].auxiliary_data.Headers[net_sta][U_tag_path_XP.replace('_' + opts.plottype.lower() + '_','_acc_')].parameters)
                V_aux_XP = upgrade_asdf_wav_dict(opts.ifile[1].auxiliary_data.Headers[net_sta][V_tag_path_XP.replace('_' + opts.plottype.lower() + '_','_acc_')].parameters)
                W_aux_XP = upgrade_asdf_wav_dict(opts.ifile[1].auxiliary_data.Headers[net_sta][W_tag_path_XP.replace('_' + opts.plottype.lower() + '_','_acc_')].parameters)
            except Exception as e:
                errors.update({'WaveformFFT info' : str(e)})        
            U_data_CV = opts.ifile[1].waveforms[net_sta][U_tag_path_CV]
            V_data_CV = opts.ifile[1].waveforms[net_sta][V_tag_path_CV]
            W_data_CV = opts.ifile[1].waveforms[net_sta][W_tag_path_CV]
            U_data_XP = opts.ifile[1].waveforms[net_sta][U_tag_path_XP.replace('_' + opts.plottype.lower() + '_','_acc_')]
            V_data_XP = opts.ifile[1].waveforms[net_sta][V_tag_path_XP.replace('_' + opts.plottype.lower() + '_','_acc_')]
            W_data_XP = opts.ifile[1].waveforms[net_sta][W_tag_path_XP.replace('_' + opts.plottype.lower() + '_','_acc_')]
            try:
                FFT_plot(U_data_CV,V_data_CV,W_data_CV,U_data_XP,V_data_XP,W_data_XP,U_aux_XP,V_aux_XP,W_aux_XP,opts)
            except Exception as e:
                errors.update({'WaveformFFT' : str(e)})
                
    elif opts.plottype == 'SA' or opts.plottype == 'SD':
        if opts.plottype == 'SA':
            tg = '_acc_'
        else:
            tg = '_dis_'
            
        try:
            U_data_XP = opts.ifile[1].auxiliary_data.Spectra[net_sta][U_tag_path_XP.replace('_' + opts.plottype.lower() + '_',tg)]
            V_data_XP = opts.ifile[1].auxiliary_data.Spectra[net_sta][V_tag_path_XP.replace('_' + opts.plottype.lower() + '_',tg)]
            W_data_XP = opts.ifile[1].auxiliary_data.Spectra[net_sta][W_tag_path_XP.replace('_' + opts.plottype.lower() + '_',tg)]

        except Exception as e:
            errors.update({'plot response spectra', str(e)})
        try:
            S_plot(U_data_XP,V_data_XP,W_data_XP,opts,cur, opts.transpflag)
        except Exception as e:
            errors.update({'plot response spectra',str(e)})

    if len(errors) == 0:
        if opts.oformat == 'PNG':
            return dict_fmt(exit_status=0, out_file=opts.outfile, out_format=opts.oformat.lower())
        elif opts.oformat == 'base64':
            with open(opts.outfile, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
            os_remove(opts.outfile)        
            return dict_fmt(exit_status=0, out_string=encoded_string, out_format=opts.oformat.lower())
    else:
        return dict_fmt(exit_status=4, exit_message=errors)
        
def main():

    ret_code = ws_plot_pro(' '.join(sys_argv[1:]))

    if ret_code['exit_status'] == 0 and ret_code['out_string']: print(ret_code['out_string'])
    if ret_code['exit_message']: 
        print(ret_code['exit_message'])
        print(errors)
    sys_exit(ret_code['exit_status'])

if __name__ == "__main__": main()
