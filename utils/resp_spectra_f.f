      SUBROUTINE PCN05(N,NP,DT,DAMP,P,ACC,XSD,XSA,STD,STA,NMX,NPX)
c-----------------------------------------------------------------------
c      N:     number of points of acceleration trace
c      NP:    number of frequencies of SDOF oscillator
c      DT:    delta t of acceleration trace
c      DAMP:  damping
c      P:     period
c      ACC:   acceleration trace
c      output:
c      XSD:   spectral displacement
c      XSA:   spectral acceleration
c      STD:   SDOF time history displacement
c      STA:   SDOF time history acceleration
c      optional:
c      NMX:   max dimension of array containing acceleration trace
c      NPX:  max dimension of array containing acceleration trace
C-----------------------------------------------------------------------
      DIMENSION A(2,2),B(2,2),ACC(NMX),P(NPX)
      DIMENSION XSD(NPX),XSV(NPX),XSA(NPX),STD(NMX,NPX),STA(NMX,NPX)
C-----------------------------------------------------------------------
Cf2py intent(out) XSD(NPX)
Cf2py intent(out) XSA(NPX)
Cf2py intent(out) STD(NMX)
Cf2py intent(out) STA(NMX)
      DO 10 IP=1,NP
         W=    6.2832/P(IP)
         DELT= P(IP)/10.
         L=    DT/DELT+1.0-1.E-05
         VERTL=1.0/FLOAT(L)
         DELT= DT*VERTL
         CALL PCN04(DAMP,W,DELT,A,B)
         XIP=    0.0
         XIPD=   0.0
         XSA(IP)=0.0
         XSV(IP)=0.0
         XSD(IP)=0.0
         NN=N-1
         STD(1,IP)=0.0
         STA(1,IP)=0.0
         DO 1 J=1,NN
            AI=ACC(J)
            SL=(ACC(J+1)-ACC(J))*VERTL
            XIPT =0.0
            XIPAT=0.0
            DO 2 JJ=1,L
               AF=   AI+SL
               XIP1= XIP
               XIPD1=XIPD
               XIP=  A(1,1)*XIP1+A(1,2)*XIPD1+B(1,1)*AI+B(1,2)*AF
               XIPD= A(2,1)*XIP1+A(2,2)*XIPD1+B(2,1)*AI+B(2,2)*AF
               AI=   AF
               XIPA= -2.*DAMP*W*XIPD-W*W*XIP
               IF(ABS(XIP).GT.XSD(IP))  XSD(IP)=ABS(XIP)
               IF(ABS(XIPA).GT.XSA(IP)) XSA(IP)=ABS(XIPA)
               IF(ABS(XIP).GT.ABS(XIPT))  XIPT=XIP
               IF(ABS(XIPA).GT.ABS(XIPAT)) XIPAT=XIPA
2              CONTINUE
            STD(J+1,IP)=XIPT
            STA(J+1,IP)=XIPAT
1           CONTINUE
10       CONTINUE
      RETURN
      END
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      SUBROUTINE PCN04(D,W,DELT,A,B)
C-----------------------------------------------------------------------
c     called by pcn05
c     D:     damping
c     W:     frequency omega
c     DELT:  something like integration step (?)
c     A:     2x2 matrix, returned
c     B:     2x2 matrix, returned
C-----------------------------------------------------------------------
      DIMENSION A(2,2),B(2,2)
C-----------------------------------------------------------------------
      DW=D*W
      D2=D*D
      A0=EXP(-DW*DELT)
      A1=W*SQRT(1.-D2)
      AD1=A1*DELT
      A2=SIN(AD1)
      A3=COS(AD1)
      A7=1.0/(W*W)
      A4=(2.0*D2-1.0)*A7
      A5=D/W
      A6= 2.0*A5*A7
      A8=1.0/A1
      A9=-(A1*A2+DW*A3)*A0
      A10=(A3-DW*A2*A8)*A0
      A11=A2*A8
      A12=A11*A0
      A13=A0*A3
      A14=A10*A4
      A15=A12*A4
      A16=A6*A13
      A17=A9*A6
      A(1,1)=A0*(DW*A11+A3)
      A(1,2)=A12
      A(2,1)=A10*DW+A9
      A(2,2)=A10
      DINV=1.0/DELT
      B(1,1)=(-A15-A16+A6)*DINV-A12*A5-A7*A13
      B(1,2)=(A15+A16-A6)*DINV+A7
      B(2,1)=(-A14-A17-A7)*DINV-A10*A5-A9*A7
      B(2,2)=(A14+A17+A7)*DINV
      RETURN
      END
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
 
