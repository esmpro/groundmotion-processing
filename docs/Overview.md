Strong-motion records and open access to strong-motion data repositories are fundamental inputs to seismology, earthquake engineering science and practice. However, to ensure a proper use of these records, a reliable strong-motion data processing is necessary to disseminate good-quality waveforms free from signal distortion (e.g., noisy band frequency, flatline channels, spurious spikes, early termination during coda, multiple baseline). The strong-motion data quality control is a challenging task, which becomes more important in case of large strong-motion data sets characterized by high levels of complexity. In addition, given the high growth rate of earthquake records from global, regional, and national seismic networks, it becomes more important to manage the data storage of strong-motion databases which potentially could contain  tens of millions of seismic waveforms.
In this regard, ESMpro was designed to provide automated data quality checks and processing of strong-motion data archived in ESM. The waveform quality check helps the system maintenance, avoiding the data processing of low quality records, thus preserving the storage capacity. Moreover, the automatic quality check introduced in ESMpro, along with the improvement of automatic settings for waveform trimming and filtering, will reduce time for manual revision, giving priority to some selected data and providing record-specific automatic settings. The current version of ESMpro enables the User to process three component waveforms stored in ASDF volumes by means of two alternative algorithms. The former (PAO1 - Paolucci et al., 2011) applies a band-pass filter to the signal and forces the velocity and displacement to oscillate around zero at the end of the signal. Conversely, the latter (eBASCO - Schiappapietra et al., 2021) applies only a piecewise baseline correction of the velocity trace, thus preserving the long-period component of the ground-motion which could be featured by near-source effects (i.e. fling-step, impulsive behavior).



More information on ESMpro project can be found in:

_Mascandola C., D’Amico M., Russo E. Luzi L. (2022); ESMpro: an improved data management for the Engineering Strong Motion database (ESM), in preparing, doi: https://doi.org/xxxxxxxx_

Motivations behind ESMpro:
- to reduce human intervention for manual processing of strong motion data;
- to handle different processing techniques;
- to improve the data management strategy of ESM.

Main features of ESMpro:
- is written in Python and relies on [**ObsPy**](https://docs.obspy.org/) and [**PyASDF**](https://seismicdata.github.io/pyasdf/) libraries for processing routines and access to seismic data stored in ASDF format;
- can be used through a command line interface; webservices for processing data present in the ESM database will be also available;
- adopts the ASDF format to store in a single file earthquake and station metadata, raw and processed ground-motion time histories, acceleration and displacement response spectra, waveform and station metrics;
- has a modular and flexible software architecture that facilitates the implementation of new or alternative algorithms.

