This page contains some code snippets that show you several common scenarios.

<br></br>
## **Download ESM data**

The User may download ASDF files useful for the ground motion processing from the [ESM web site](https://esm-db.eu/) or by using the [Event-Data Web-Service](https://esm-db.eu/esmws/eventdata/1/) which provides machine-friendly access to event-based waveforms and response spectra included in the ESM Database.

<span dir="">Data can be obtained using any program supporting the HTTP-GET method, e.g. your favorite web browser</span> as follows:

<pre>

`https://esm-db.eu/esmws/eventdata/1/query?eventid=EMSC-20161030_0000029&station=CLO&processing-type=CV&data-type=ACC&add-xml=True&add-auxiliary-data=True`

</pre>in this case the User will obtain an ASDF volume (e.g. query.h5) with uncorrected acceleration recorded at the station `IT.CLO` during the `EMSC-20161030_0000029` seismic event

Move the file downloaded from ESM to the groundmotion-processing input directory. The User may change the file name if necessary:

`mv /path/query.h5 /path/to/input/example.h5`

<br></br>
# prepocessing.py

Run the **preprocessing.py** module indicating the configuration file and the ASDF volume to be analised:

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./preprocessing.py --input=/path/to/input/example.h5 --config=./input/defaults.json
```

You will get the following output with a summary of the main features of the uncorrected waveforms such as:

* _warnings related to the analised waveform_
* _quality class_
* _signal-to-noise parameters_
* _list of errors raised during the code execution_

```1c
path:  /path/to/input
filename_h5:  example.h5
filename_json:  defaults.json
SUMMARY:

warnings:  {'the_number_of_traces_is_equal_to_three': True, 'empty_component': False, 'zero_component': False, 'trigger_detected': True, 'multiple_events': False, 'unconsistent_picking': False, 'epicentral_distance_detected': True, 'picking_detected': True, 'unconsistent_horizontal_pga': False, 'unconsistent_horizontal_p': None, 'unconsistent_vertical_pga': False, 'unconsistent_vertical_pgd': None, 'unconsistent_horizontal_amplitude': False, 'unconsistent_vertical_amplitude': False, 'extreme_pga_values': False, 'lc frequecies based on signal to noise ratio of the fft': False, 'lc frequecies based on magnitude ranges': True, 'bad lc frequency': False, 'hc frequecies based on signal to noise ratio of the fft': False, 'hc frequencies based on fixed threshold': True, 'bad hc frequency': False}
classes:  {'A': True, 'B': False, 'C': False, 'D': False, 'reason': 'High Signal to Noise Ratio'}
parameters:  {'start_time': UTCDateTime(2016, 10, 30, 6, 40, 13, 769000), 'end_time': UTCDateTime(2016, 10, 30, 6, 40, 54, 109000), 'signal_window': [8.635000228881836, 12.635000228881836], 'noise_window': [1.9800000190734863, 5.980000019073486], 'ps_auto': {'P': 5.980000019073486, 'S': 8.635000228881836}, 'quality_index': {'average_value': 45.018074356433466, 'component': {'U': 43.828011646282235, 'V': 45.542661003937155, 'W': 45.68355041908103}}, 'rms': {'signal': {'component': {'U': 144.95451953658568, 'V': 211.04048756284075, 'W': 209.93880566095223}}, 'noise': {'component': {'U': 0.006003860545703843, 'V': 0.005889788248156076, 'W': 0.0056720190431354435}}}, 'lc': {'component': {'U': 0.04, 'V': 0.04, 'W': 0.04}}, 'hc': {'component': {'U': 40, 'V': 40, 'W': 40}}}
errors: []
```

---

The User may keep a copy of [processing parameters](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example.parameters.json) and [quality check metadata](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example.quality_check.json) in json files stored in the same input directory of the ASDF volume, by adding to the preprocessing.py command line the option **`--copy=True`**. 
This option also saves the configuration files ([example_AP.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example_AP.json), [example_AB.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example_AB.json), or [example_MC.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example_MC.json)) necessary for the further **processing.py** module. 

<br></br>
# processing.py

Run the [**processing.py**](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/processing.py) module indicating the configuration file, which contains the processing workflow and setting parameters, besides the ASDF volume to be processed.

The waveforms contained in the [example.h5](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example.h5) ASDF volume can be processed with three different configuration files ([example_AP.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example_AP.json), [example_AB.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example_AB.json), or [example_MC.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example_MC.json)) depending on the workflow ([PAO11](./processing.md#pao11), [eBASCO](./processing.md#ebasco) or CUSTOM). 
Some examples of use are reported below:

_PAO11 example_

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./processing.py --input=./input/example.h5 --config=/path/to/input/example_AP.json
```

_eBASCO example_

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./processing.py --input=./input/example.h5 --config=/path/to/input/example_AB.json
```

_CUSTOM example_

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./processing.py --input=./input/example.h5 --config=/path/to/input/example_MC.json
```

**NOTE:** for sake of simplicity the customized processing workflow applies the following numerical operations <span dir="">`["WaveformDetrend","WaveformCutting","WaveformTaper","WaveformFilter"]`</span>, the **_User must edit with care_** this method list to apply a reliable data processing.

The outputs of the [processing.py](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/processing.py) module are described in the [output section](./Output.md) of the ESMpro documentation.

<br></br>
# postprocessing.py

Run the **postprocessing.py** module indicating the ASDF volume that contains the processed waveforms.

To generate a preview of the uncorrected waveforms run:

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787358/h5/example_AP.h5
```

the User will get the following .png file

![](/images/PRE.png)
<img src="images/PRE.png"  width="400" height="300">

Other examples are here provided to obtain:

**Uncorrected and Corrected Acceleration**

_PAO11 example_

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787358/h5/example_AP.h5 --plot=ACC --unprocessed=True --axes=True --psarrivals=True --channels=True --annotations=True --grid=True --peakvalues=True
```
![](/../output/1665787358/fig/ACC.png)
<img src="../output/1665787358/fig/ACC.png"  width="500" height="400">

_eBASCO example_

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787690/h5/example_AB.h5 --plot=ACC --unprocessed=True --axes=True --psarrivals=True --channels=True --annotations=True --grid=True --peakvalues=True --ptype=AB
```

![](/../output/1665787690/fig/ACC.png)
<img src="../output/1665787690/fig/ACC.png"  width="500" height="400">


**Corrected Velocity**

PAO11 example

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787358/h5/example_AP.h5 --plot=VEL --unprocessed=True --axes=True --psarrivals=False --channels=True --annotations=True --grid=True --peakvalues=True
```

![](/../output/1665787358/fig/VEL.png)
<img src="../output/1665787358/fig/VEL.png"  width="500" height="400">


eBASCO example

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787690/h5/example_AB.h5 --plot=VEL --unprocessed=True --axes=True --psarrivals=False --channels=True --annotations=True --grid=True --peakvalues=True --ptype=AB
```

![](/../output/1665787690/fig/VEL.png)
<img src="../output/1665787690/fig/VEL.png"  width="500" height="400">


**Corrected Displacement**

PAO11 example

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787358/h5/example_AP.h5 --plot=DIS --unprocessed=True --axes=True --psarrivals=False --channels=True --annotations=True --grid=True --peakvalues=True
```

![](/../output/1665787358/fig/DIS.png)
<img src="../output/1665787358/fig/DIS.png"  width="500" height="400">


eBASCO example

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787690/h5/example_AB.h5 --plot=DIS --unprocessed=True --axes=True --psarrivals=False --channels=True --annotations=True --grid=True --peakvalues=True --ptype=AB
```

![](/../output/1665787690/fig/DIS.png)
<img src="../output/1665787690/fig/DIS.png"  width="500" height="400">


**Uncorrected and Corrected Fourier Spectra**

PAO11 example

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787358/h5/example_AP.h5 --plot=FFT --unprocessed=True --axes=True --channels=True --annotations=True --grid=True
```

![](/../output/1665787358/fig/FFT.png)
<img src="../output/1665787358/fig/FFT.png"  width="500" height="400">


eBASCO example

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787690/h5/example_AB.h5 --plot=FFT --unprocessed=True --axes=True --channels=True --annotations=True --grid=True --ptype=AB
```

![](/../output/1665787690/fig/FFT.png)
<img src="../output/1665787690/fig/FFT.png"  width="500" height="400">


**Acceleration Response Spectra**

PAO11 example

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787358/h5/example_AP.h5 --plot=SA --unprocessed=True --axes=True --channels=True --grid=True
```

![](/../output/1665787358/fig/SA.png)
<img src="../output/1665787358/fig/SA.png"  width="500" height="400">


eBASCO example

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787690/h5/example_AB.h5 --plot=SA --unprocessed=True --axes=True --channels=True --grid=True --ptype=AB
```

![](/../output/1665787690/fig/SA.png)
<img src="../output/1665787690/fig/SA.png"  width="500" height="400">


**Displacement Response Spectra**

PAO11 example

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787358/h5/example_AP.h5 --plot=SD --axes=True --channels=True --grid=True
```

![](/../output/1665787358/fig/SD.png)
<img src="../output/1665787358/fig/SD.png"  width="500" height="400">


eBASCO example

```
(groundmotion-processing) user@MyComputer:~/groundmotion-processing$ ./postprocessing.py --input=/path/to/output/1665787690/h5/example_AB.h5 --plot=SD --axes=True --channels=True --grid=True --ptype=AB
```

![](/../output/1665787690/fig/SD.png)
<img src="../output/1665787690/fig/SD.png"  width="500" height="400">
