Example of plots for the **time series** obtained with the [PAO11](./processing.md#pao11) and [eBASCO](./processing.md#ebasco) processing methods. 

| | PAO11 | eBASCO |
|--| -- | -- |
| acceleration | [ACC.png](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/output/1665787358/fig/ACC.png) | [ACC.png](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/output/1665787690/fig/ACC.png) |
| velocity | [VEL.png](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/output/1665787358/fig/VEL.png) | [VEL.png](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/output/1665787690/fig/VEL.png) |
| displacement | [DIS.png](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/output/1665787358/fig/DIS.png) | [DIS.png](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/output/1665787690/fig/DIS.png) |


Example of plots for the **Fourier Spectra** of the Accelerometric time series obtained with the PAO11 and eBASCO processing methods. 

| PAO11 | eBASCO |
| -- | -- |
| [FFT.png](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/output/1665787358/fig/FFT.png) | [FFT.png](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/output/1665787690/fig/FFT.png) |

Example of plots for the **Response Spectra** obtained with the PAO11 and eBASCO processing methods. 

| | PAO11 | eBASCO |
|--| -- | -- |
| acceleration | [SA.png](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/output/1665787358/fig/SA.png) | [SA.png](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/output/1665787690/fig/SA.png) |
| displacement | [SD.png](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/output/1665787358/fig/SD.png) | [SD.png](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/output/1665787690/fig/SD.png) |
