The workflow of the ESMpro code (Figure 1) consists of three main python modules: 
1. [**preprocessing.py**](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/preprocessing.py), which analyzes the unprocessed waveforms providing information about the data quality and the configuration files for data processing - [more details HERE](./preprocessing.md); 
1. [**processing.py**](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/processing.py), which processes raw data with PAO11 (Paolucci et al., 2011), eBASCO (Schiappapietra et al., 2021), or user-defined (CUSTOM) workflows - [more details HERE](./processing.md). 
2. [**postprocessing.py**](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/postprocessing.py), which provides different plots of the relative waveforms and spectra - [more details HERE](./postprocessing.md).

![](/images/Workflow.jpg)
<img src="images/Workflow.jpg"  width="700" height="800">

**Figure 1:** workflow for data processing in ESMpro. 
