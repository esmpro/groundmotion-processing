This code performs a quality check on the unprocessed waveforms and provides the settings for automatic processing, saving the outputs in two different JSONs: the former with the quality class, the latter with processing workflow and settings (Figure 1). The preprocessing module also provides the configuration file for processing.

![](/images/preprocessing.jpg)
<img src="images/preprocessing.jpg"  width="600" height="270">

**Figure 1**

<br/><br/> 

**<h1 style="font-size:40px;">``Quality check``**

The quality check, in the preprocessing phase, classifies the waveforms according to: 

<details><summary>1) input data requirements</summary>

Some preliminary checks ensure that all mandatory requirements to run ESMpro are fulfilled (e.g., wrong format, noncompliant mandatory metadata, dead or empty channels). The input file must be in Adaptable Seismic Data Format (ASDF, Krischer et al., 2016), where data and metadata related to waveforms, events and stations are organized in a specific hierarchical structure inside a HDF5 ([Hierarchical Data Format version 5; The HDF Group 1997–2015](https://www.hdfgroup.org/HDF5/)) container. After that, noise and signal windows should be identified to compute the SNR<sub>T</sub>. However, in some cases, the reprocessing algorithm could fail during waveform trimming (Figure 2a) or picking the P and S wave arrivals (Figure 2b). In these cases, the SNR<sub>T</sub> cannot be computed; otherwise, we compute the SNR<sub>T</sub> as described in the following section.

![](/images/sub1.jpg)
<img src="images/sub1.jpg"  width="600" height="200">

**Figure 2:** Example of records that cannot be automatically a) trimmed or b) picked.

</details>


<details><summary>2) signal-to-noise ratio in time domain (SNR<sub>T</sub>)</summary>

We compute the SNR<sub>T</sub> value as follows:

- 2th order acausal Butterworth bandpass filter between 2-8 Hz to allow a better application of the picking algorithm;
- picking of P- and S- waves with the [ar_pick algorithm in Obspy](https://docs.obspy.org/packages/autogen/obspy.signal.trigger.ar_pick.html), which includes a combination of AR-AIC and STA/LTA algorithms (Akazawa, 2004); 
- computation of the Root Mean Square (RMS<sub>S</sub>) on a strong-motion time window of 4 s, starting from the S- wave picking;
- computation of the Root Mean Square (RMS<sub>N</sub>) on a pre-event noise window of 4 s, before P-wave picking. If the length of the pre-event noise window is shorter, it takes 4s from the end of the trace;
- computation of SNR<sub>T</sub> as the ratio between RMS<sub>S</sub> and RMS<sub>N</sub>, for each ground-motion component;
- computation of the mean SNR<sub>T</sub> for the three-component seismic records.
 

The distribution of the SNR<sub>T</sub> values of the ESM waveforms (Figure 3) shows that most data have a high Signal-to-Noise ratio, with an average of 14.3 dB (Figure 4a) and a standard deviation of 12.5 dB. Based on this distribution, we decided to set an empirical threshold of 6 dB (Figure 4b) to identify the noisy traces. A few data show extremely high SNR<sub>T</sub> (≥ 60 dB; red circle in Figure 3) that are often artifacts related to instrumental issues or bad signal acquisitions (Figure 4c). 

![](/images/SNRt.jpg)
<img src="images/SNRt.jpg"  width="350" height="250">

**Figure 3:** distribution of SNR<sub>T</sub> values on the ESM data. The black line indicates the mean value and the gray shadow the 68% confidence interval. The red dashed line and the red circle mark the threshold of 6 dB and the anomalous SNR<sub>T</sub> distribution, respectively.

![](/images/sub2.jpg)
<img src="images/sub2.jpg"  width="500" height="400">

**Figure 4:** The green and red boxes indicate the noise and signal windows, respectively. The vertical green and red lines indicate the P- and S- wave arrivals, respectively. Example of a)good quality data; b) low quality data; c) record with SNR<sub>T</sub> > 60 dB;  and d) record with PGA > 2 g.

</details>

<details><summary>3) additional features</summary>

When SNR<sub>T</sub> ranges between 6-60 dB, the record goes to the additional checks reported in Table 1. Since these are not mutually exclusive, they are addressed in sequential order. Firstly, we check for extreme Peak Ground Acceleration (PGA) values. The PGA is considered suspected when it is greater than 2g on at least one of the three components (Figure 4d). Secondly, we check for suspected acceleration components: if one horizontal component is more than twice the other, based on PGA or Root Mean Square (RMS) values (Figure 5a), the data should be manually revised because one component may be biased. The same check is performed on the vertical component, considering a threshold of three times the horizontal ones. Indeed, the horizontal components have an average PGA ratio of 0.9 and they are about 1.6 times the vertical one (Figure 6): the thresholds for the amplitude checking are twice these ratios. 

![](/images/sub3.jpg)
<img src="images/sub3.jpg"  width="500" height="400">

**Figure 5:** The green and red boxes indicate the noise and signal windows, respectively. The vertical green and red lines indicate the P- and S- wave arrivals, respectively. Solid lines: automatic picking; dashed lines:  theoretical arrivals. Example of records with a) unrealistic horizontal component; b) unreliable P-wave arrival; c) multiple events; and d) restricted frequency band.


![](/images/PGA_PGV.jpg)
<img src="images/PGA_PGV.jpg"  width="700" height="200">

**Figure 6:** PGA distribution on the ESM data. Left: relation between PGA of horizontal components (HNN, HNE). Central: relation between East and vertical components. Right: relation between North and vertical components. The horizontal components have an average PGA ratio of 0.9 and they are about 1.6 times the vertical one. 

Subsequently, we check the reliability of the theoretical P wave arrival considering the lag with the 5% of the Normalized Arias Intensity (Arias, 1970). A significant lag (Figure 5b) may cause an improper trimming around the target event. After that, we check the occurrence of multiple events by applying a trigger algorithm ([Recursive STA/LTA in Obspy](https://docs.obspy.org/packages/autogen/obspy.signal.trigger.recursive_sta_lta_py.html)). If more than one trigger occurs inside the significant duration (i.e., D<sub>5-95</sub>: time span between 5% and 95% of the Normalized Arias Intensity; Arias 1970) of the ground motion, the waveform is flagged as multiple events (Figure 5c). The target event is identified by the trigger closest to the theoretical P wave arrival. Finally, we check for records characterized by a usable frequency bandwidth of the Fourier spectrum in the interval 0.4–20 Hz (i.e., a low-cut frequency of the bandpass filter greater than 0.4 Hz or a high-cut frequency of the bandpass filter lower than 20 Hz). Indeed, the good-quality waveforms in ESM always preserve the Fourier Amplitude Spectrum in this frequency range (Lanzano et al., 2019). A restricted frequency passband may be critical for the usability of the response spectral values (Ancheta et al., 2014; Douglas and Boore, 2011) and it is usually related to a low signal-to-noise ratio or to disturbances on one ground-motion component. Figure 5d shows an example due to a bad signal acquisition on one ground-motion component. In this case, the disturbances on the HNN component affect the automatic picking. When the 4s noise window cannot be selected before the P-wave arrival, the noise window is taken starting from the end of the trace (Figure 5d).

**Table 1:** additional checks considered for the quality class assignment. lc and hc are the low-cut and high-cut frequency, respectively, of the Butterworth bandpass filter. PGA: Peak Ground Acceleration; PGD: Peak Ground Displacement; RMS: Root Mean Square (quadratic mean).


| # | Warnings | Quality Class | Issues |
| ------ | ------ | ------ | ------ |
| 1 | Extreme PGA | B |The PGA is greater than 2g on at least one of the three components.|
| 2 | Suspected amplitude | B |One horizontal component is more than twice the other or the vertical component is more than 3 times the horizontals (on PGA or RMS). Data should be manually revised.|
| 3 | Unreliable P-wave arrival | B |The waveform may not be trimmed properly.|
| 4 | Multiple events | B |More events are detected on the same trace, suggested manual revision.|
| 5 | Restricted frequency passband | B |<ul><li>If lc > 0.4 Hz</li> The low-cut frequency of the bandpass filter is too high; try to extend the bandwidth by manual revision. <li>If hc < 20 Hz</li> The high-cut frequency of the bandpass filter is too low; try to extend the bandwidth by manual revision.</ul>|

</details>


Based on the applied quality checks, we propose the following waveform classification (Figure 7):
- A - records with SNR<sub>T</sub>  in the range 6-60 dB; 
- B - records with SNR<sub>T</sub> in the range 6-60 dB affected by some additional features (Table 1);
- C - records with SNR<sub>T</sub> ≤ 6dB, or SNR<sub>T</sub> ≥ 60dB affected by signal distortions;
- D - records that caused unexpected errors while computing SNR<sub>T</sub> or featured by improper input data/metadata.   

![](/images/Quality_Class.jpg)
<img src="images/Quality_Class.jpg"  width="400" height="400">

**Figure 7:** decision matrix in ESMpro.

These four quality classes can support decisions on automatic and manual processing in ESM. Only records in A and B classes should be automatically processed and a higher priority for manual revision is suggested for B and D classes (Figure 7). 

<br/><br/>

**<h1 style="font-size:40px;">``Settings for automatic processing``**

The processing of accelerometric records can be performed through two alternative methods: [PAO11](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/wikis/processing#pao11) ([Paolucci et al., 2011](https://link.springer.com/chapter/10.1007/978-94-007-0152-6_8)) and [eBASCO](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/wikis/processing#ebasco) ([Schiappapietra et al., 2021](https://www.mdpi.com/2076-3263/11/2/67)). 
Among the processing settings required by both methods, the waveform trimming and the Butterworth bandpass filter often require the visual inspection of time series and Fourier spectra. 

<details><summary>Waveform trimming</summary>

The raw data retrieved from continuous streams (considering the event metadata) are often featured by long noise windows before or after the event, which may be annoying for engineering and practitioners that adopt these waveforms in several applications. The refinement of the waveform trimming around the target event can be automatically performed by applying a trigger algorithm ([Recursive STA/LTA in Obspy](https://docs.obspy.org/packages/autogen/obspy.signal.trigger.recursive_sta_lta_py.html)), along with the theoretical P wave arrival to discard false triggers and select the correct one in case of multiple events. The procedure for the automatic waveform trimming is the following:

- 2th order acausal Butterworth bandpass filter between 2-8 Hz to allow a better application of the trigger algorithm;
- the recursive short-time average/long-time average (STA/LTA) algorithm in Obspy is applied with STA=1s and LTA=8s;
- Trigger On (T1) at the STA/LTA threshold of 2.5 (Figure 8);
- Trigger Off (T2) at the STA/LTA threshold of 0.3 (Figure 8);
- selection of the trigger closer to the theoretical P wave arrival:  the theoretical P wave arrival is adopted to discard false triggers and select the correct trigger in case of multiple events (Figure 8);
- first sample: T1-20s. If the 3-component waveform does not have enough pre-event seconds, a zero-padding is applied; 
- last Sample: T2+dt, where dt is a time interval, which depends on the source-to-site distance (D) based on empirical observations (Table 2).

**Table 2**
| dt [s] | Condition on D [km] | 
| ------ | ------ |
| 20 | D < 20 |
| 40 | 20 < D < 100 |
| 60 | 100 < D < 200 |
| 80 | D > 200  |

![](/images/Multiple_events.jpg)
<img src="images/Multiple_events.jpg"  width="400" height="400">

**Figure 8:** example of application of the trigger algorithm.

</details>

<details><summary>Waveform filtering</summary>

A further improvement for the automatic settings regards the cutoff frequencies of the bandpass filter: the low-cut (lc) and high-cut (hc) frequencies (Paolucci et al. 2011; Puglia et al. 2018) are the most important parameters to set. An improper bandpass filtering can alter the frequency content of the signal with a significant impact on the representativeness of the recorded earthquake and, consequently, on the calibration of ground motion models. To set the cutoff frequencies of the bandpass filter we apply the classical signal-to-noise ratio in the frequency domain (SNR<sub>F</sub>). For the SNRF computation, we consider the entire traces uploaded on ESM before waveform trimming, to preserve all the available pre-event noise. The method adopted to automatically set the cutoff frequencies of the bandpass filter is described by the following steps:
- selection of the noise window before the P wave arrival (all available pre-event noise; Figure 9a);
- selection of the signal window after the P wave arrival. The noise and signal windows have the same length, constrained by the available pre-event noise (Figure 9a);
- computation of the Fast Fourier Transform on the signal (FFT<sub>S</sub>) and noise (FFT<sub>N</sub>) windows;
- resampling of the FFT and application of a Konno-Ohmachi smoothing (b=40; Figure 9b);
- computation of the of the Signal-to-Noise Ratio in the frequency domain, SNR<sub>F</sub>, as the ratio of (FFT<sub>S</sub> - FFT<sub>N</sub>) and FFT<sub>N</sub> for each ground motion component (Figure 9c);
- the SNR<sub>F</sub> threshold of 2 is adopted to select the low-cut frequency (lc) and the high-cut frequency (hc);
- selection of lc: we start from the central peak of the SNR<sub>F</sub> curve and we move leftwards with a mobile window where the average SNR<sub>F</sub> is computed. The mobile window is centered on lc and ranges from lc/2 to lc∙2. The lc frequency is selected when the SNR<sub>F</sub>, computed in the mobile window, is lower than 2. The minimum value of lc is the inverse of the time-length of the pre-event noise window; 
- selection of hc: same method adopted for lc, but moving rightwards with a mobile window that ranges from hc/1.3 to hc∙1.3. The maximum value of hc is the Nyquist frequency (f<sub>N</sub>).
- If the SNR<sub>F</sub> curve is always above the predefined threshold of 2, the lc frequency depends on magnitude ranges (Puglia et al., 2018). On the contrary, the hc frequency is set at 40 Hz to avoid anthropic and instrumental noise often observed at higher frequencies (Trnkoczy et al., 2012). 

![](/images/SNRf.jpg)
<img src="images/SNRf.jpg"  width="700" height="500">

**Figure 9:** automatic setting of the cutoff frequencies for the bandpass filter. As an example, the records of the event EMSC-20150808_0000064 at the IT.TOR station are reported. a) Accelerometric time series. The red dashed line marks the automatic P-wave arrival; the blue and red boxes indicate the noise and signal windows respectively. b) FFT of signal (blue curves) and noise (red curves). c) Signal-to-noise ratio in the frequency domain (SNR<sub>F</sub>). The selected SNR threshold is indicated by a dashed red line and the cutoff frequencies of the bandpass filter are given by the vertical gray lines.

</details>

<br/><br/> 
**<h1 style="font-size:40px;">``Testing``**

The ESMpro improvements related to quality check and automatic settings are tested on ⁓70,000 records in ESM. These records, already revised by ESM operators as good (⁓45,000) or bad (⁓22,000) quality waveforms, were adopted to test the effectiveness of ESMpro in replacing manual processing and reducing time for human intervention. Interested readers may refer to [**Mascandola et al. (2022)**]() for the testing phase on the comparison between automatic and manual settings for data processing. 
