# **`Example for preprocessing.py`**

```bash
./preprocessing.py [options]
./preprocessing.py --input=[INPUT_DIR]/example.h5 --config=[INPUT_DIR]/defaults.json
```
| options | description | default | format |
|---------|-------------|---------|--------|
| input | ASDF volume containing the raw data to be analyzed. | \* | str |
| config | configuration file in JSON format containing placeholders for metadata defined by preprocessing.py and default parameters. | [defaults.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/defaults.json) | str |
| copy | Keep a copy of json files with processing parameters and quality check metadata. 'Guess' will keep copies only for waveforms classified as quality 'A' (Best) or 'B' (Good). | False | str |
| verbose | verbose mode, prints summary and other details | True | str |

\* mandatory

# **`Example for processing.py`**

```bash
./processing.py [options]
./processing.py --input=[INPUT_DIR]/example.h5 --config=[INPUT_DIR]/config.json
```
| options | description | default | format |
|---------|-------------|---------|--------|
| input | ASDF volume containing the raw data to be processed | \* | str |
| config | configuration file in JSON format containing metadata to identify the waveform to be processed within the ASDF volume, the processing workflow, the setting parameters, and additional information to calculate response spectra. | \* | str |
| verbose | verbose mode, prints summary and other details | True | str |

\* mandatory

# **`Example for postprocessing.py`**

```bash
./postprocessing.py [options]
./postprocessing.py --input=[OUTPUT_DIR][TIME_STAMP]/h5/example_processing_type.h5 
```
| options | description | default | format |
|---------|-------------|---------|--------|
| input | ASDF volume containing both raw and processed data | \* | str |
| ptype | Specify processing type for data selection; available types are: 'MP' (manually processed using Paolucci et al., 2011), 'AP' (automatically processed using Paolucci et al., 2011), 'MB' (manually processed using| MP | str |
|format  | Specify format for result | PNG | str |
| plot |Specify the type of plot for data selection; available types are: 'PRE' (preview of acceleration, in cm/s^2, 'FFT' (Fourter spectra, in cm*s/s^2), 'ACC' (acceleration, in cm/s^2), 'VEL' (velocity, in cm/s), 'DIS' (displacement, in cm), 'SA' (acceleration response spectra, in cm/s^2) and 'SD' (displacement response spectra, in cm)  | PRE | str |
| transparency | Add Transparency to Graphics Objects | False | str |
| psarrivals | Add P and S waves arrivals to acceleration, velocity, or displacement taces| False | str |
| channels | Add ground motion orientation to Graphic Objects | False | str |
| annotations | Add annotations to Graphic Objects | False | str |
| axes | Add axes to Graphic Objects | False | str |
| grid | Add grid and axes to Graphic Objects | False | str |
| peakvalues | Add ground motion peak values to the Graphic Objects | False | str |
| frequencies | Add corner frequencies of the Fourier Specra filter to the Graphic Objects | False | str |
| unprocessed | Add only unprocessed wavefroms to the Graphic Objects | False | str |
| outfile | Specify the output filename | output | str |
| verbose | verbose mode, prints summary and other details | True | str |

\* mandatory