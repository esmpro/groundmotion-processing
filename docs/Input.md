To run ESMpro you need two inputs:

1. configuration file
2. input data

# 1. CONFIGURATION FILE

A [configuration file](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/defaults.json) is required by ESMpro to help Users to customize the workflow and set the processing parameters. The configuration file is a JSON dictionary wich includes three keys:

**_workflow_**

A dictionary containing metadata to define the processing workflow
| name | description |
|------|-------------|
| modality | tag to identify whether the data processing will apply setting parameters defined by the User or estimated by ESMpro, admitted values are: {M,A}; M:Manual - A:Automatic |
| method | tag to define the processing scheme, admitted values are : {P,B,C}; P: PAO11 (Paolucci et al., 2011) - B:eBASCO (Schiappapietra et al., 2021)- C:Custom |
| trigger | tag to define the branch of the PAO11 processing scheme, admitted values are : {NT,LT}; NT:Normal Trigger - LT:Late Trigger |
| type | tag to define the branch of the PAO11 processing scheme, admitted values are : {A,D}; A:Analog - D:Digital |
| steps | list of processing steps executed by the customized workflow, admitted methods are: WaveformCutting, WaveformDetrend, WaveformIntegrate, WaveformDifferentiate, WaveformTaper, WaveformFilter |

**_settings_**

A set of dictionaries related to the processing workflow. The key of each dictionary represents a single processing step, while the value is a dictionary of arguments for the applied numerical method
| name | description |
|------|-------------|
| AmplitudeScaling | to apply an integer multiplier factor to the uncorrected acceleration. Only float or integer data type can be used to change the waveform amplitude. |
| WaveformResamplig | to resample the uncorrected acceleration. Only float data type can be used to resample a waveform. |
| WaveformCut | to isolate only the time window where the target event was recorded. Start- and End-Time must have the following format yyyy-mm-ddThh:mm:ss.sZ. |
| WaveformIntegrate | waveform integration; supported obspy method: cumtrapz. |
| WaveformDifferentiate | waveform differentiation; supported obspy method: gradient. |
| WaveformDetrend | waveform detrending; supported obspy method: linear. Set the number of samples to automatically define the T1, T2, and T3 correction points that will be used to define the eBASCO piecewise baseline correction. The eBASCO correction points T1 and T2 can be set by the User with the following format yyyy-mm-ddThh:mm:ss.sZ. |
| WaveformTaper | waveform tapering; supported obspy method: cosine. Only float or integer data type can be used as taper percentage in pre- or post-event. |
| FourierSpectrumFilter | waveform filtering: supported obspy method: bandpass, lowpass |

**_data_**

An additional dictionary containing a list of periods and the damping percentage to calculate the response spectra.
| name | description |
|------|-------------|
| Periods | list of periods to calculate response spectra. |
| Damping | percentage of damping to calculate response spectra. |

# **`Example`**

[AB.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example_AB.json) configuration file to process waveforms with [eBASCO](./processing.md#ebasco)

[AP.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example_AP.json) configuration file to process waveforms with [PAO11](./processing.md#pao11)

[MC.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example_MC.json) configuration file to process waveforms with user defined workflow

# 2. INPUT DATA

The input file is in the Adaptable Seismic Data Format ([ASDF](http://seismic-data.org/); Krischer et al., 2016). The ASDF volume can be read by dedicated software (e.g. [HDFView](http://portal.hdfgroup.org/display/support); [ASDF SEXTANT](https://github.com/SeismicData/asdf_sextant) or programming languages such as [Python](https://seismicdata.github.io/pyasdf/) or Matlab. The ASDF format organizes data in a hierarchical structure inside a HDF5 (Hierarchical Data Format version 5; The HDF Group 1997–2015) container. The ESM HDF5 container is organized in three sections as reported in figure 1:

* **QuakeML** - includes event metadata such ad localization, origin time, magnitude, and focal mechanism
* **Waveforms** - waveform data and metadata, with the [FDSN StationXML](https://www.fdsn.org/xml/station/)
* **Auxiliary Data** - includes metadata related to event, station, waveform, source-distance metrics, data processing, license of use, and citation (Header). The 5% damped acceleration and displacement response spectra (Spectra) are also reported.

Each trace is named according to the following scheme (hereinafter waveform_tag):

**location_code.channel_code.event_id.gm_par.processing_type**

According to the [Standard for the Exchange of Earthquake Data](http://www.fdsn.org/pdf/SEEDManual_V2.4.pdf) (SEED) the first two codes (location_code, channel_code) identify the recording instrument, while the last ones relate to the seismic event (event_id), the ground motion parameter (gm_par), and the processing workflow (processing_type).

![](/images/Input.jpg)
<img src="images/Input.jpg"  width="800" height="700">

**Figure 1:** Structure of the ESM ASDF file in the HDF5 container (modified after Krisher et al., 2016). In blue, the information about an arbitrary number of seismic events stored in a single QuakeML. In red, the seismic waveforms are stored per station together with the necessary meta information in the form of an FDSN StationXML document. In yellow, the response spectra and metadata related to stations, events, waveforms, processing, credits, and license are hierarchically stored in the auxiliary data section.

# **`Example`**

[IT.CLO..HG.EMSC-20161030_0000029.h5](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example.h5) input file to be processed with PAO11, eBASCO, or User defined workflows.

# **`Download ESM data`**

The User may download ASDF files useful for the ground motion processing from the [ESM web site](https://esm-db.eu/) or by using the [Event-Data Web-Service](https://esm-db.eu/esmws/eventdata/1/) which provides machine-friendly access to event-based waveforms and response spectra included in the ESM Database.

Example

<span dir="">Data can be obtained using any program supporting the HTTP-GET method, e.g. your favorite web browser</span> as follows:

```
https://esm-db.eu/esmws/eventdata/1/query?eventid=EMSC-20161030_0000029&station=CLO&processing-type=CV&data-type=ACC&add-xml=True&add-auxiliary-data=True
```

in this case the User will obtain an ASDF volume (es. query.h5) with uncorrected acceleration recorded at the station `IT.CLO` during the `EMSC-20161030_0000029` seismic event
