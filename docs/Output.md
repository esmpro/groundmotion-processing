The output of **preprocessing.py** is a directory with the following path (Figure 1):


/ESMpro/input/

that can contain different JSON files:
<ul><li>[AB.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example_AB.json) - configuration file to Automatically (A) process uncorrected waveforms with [PAO11](./processing.md#pao11)(P);</li><li>[AP.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example_AP.json) - configuration file to Automatically (A) process uncorrected waveforms with [eBASCO](./processing.md#ebasco)(B);</li><li>[MC.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example_MC.json) - configuration file to Manually (M) process uncorrected waveforms with a custom (C) processing;</li><li>[parameters.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example.parameters.json) - dictionary with some features of the uncorrected waveform such as signal to noise ratio, arrival times of P- and S-waves, time window for waveform trimming, band frequency for Butterrwhorth filtering of the signal;</li><li>[quality_check.json](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/input/example.quality_check.json) - dictionary for quality classification and detected features.</li></ul>

<br/><br/>
The output of **processing.py** is a directory with the following path (Figure 1):


/ESMpro/output/time_stamp/

that contains two folders: _h5_ with the processed data and _json_ with the relative settings and metadata.


<br/><br/>
The output of **postprocessing.py** is a directory with the following path (Figure 1):


/ESMpro/output/time_stamp/fig/

that contains the plots of the accelerometric (ACC.png), velocimetric (VEL.png) and displacement (DISP.png) time series, along with the unprocessed waveforms (PRE.png). The plots of the Fourier Spectra (FFT.png), besides the 5% damped Acceleration (SA.png) and Displacement (SD.png) Response Spectra are also provided.
![](/images/Output.jpg)
<img src="images/Output.jpg"  width="1300" height="550">

**Figure 1**
