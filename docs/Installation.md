## Development software

This tutorial will walk you through installing the [groundmotion-processing development software](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/tree/main) and its basic requirements, and managing its specific dependencies by using a Python3 [virtual environment](https://docs.python.org/3/tutorial/venv.html).

**OS support**

* GNU/Linux

_The instructions in this tutorial assume that you are using a GNU/Linux distribution based on Ubuntu._

For Debian and other distributions based on the `apt` package manager you may have to login as (or `su` to) root instead of using `sudo`.

If you're using a different GNU/Linux distro (eg. based on `rpm` packages) please refer to your package manager documentation for instructions on how to install the required packages.

* Microsoft Windows

If you're running Microsoft Windows 10 (or newer) it is recommended to install everything using [Windows Subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/install) aka `wsl`.

Since wsl is based on Ubuntu Linux, you will be able to follow the instructions in this howto unaltered using the wsl command prompt exactly as if you were running an Ubuntu based distribution.

* Apple OSX

If you are running Apple OSX the steps described in the last paragraph of this howto ("Creating a Virtual Environment") should work as long as you have the Python3 programming language installed together with the Python modules `pip` and `venv`. Please note that while it is reasonable that it will work, it has not been tested.

**Installing Python3 and required Python3 libraries (pip and venv)**

* Python3

Before you go any further, make sure you have `Python3` installed and that it is available from your commandline. As it is usually already included by default on many GNU/Linux distributions, you can check this by running the following command at a shell prompt:

`python3 --version`

You should get some output with the exact version installed (eg. 3.8.2). If you do not have Python3, you can install it using the following commands:

1. Update the package list using the following command:

`sudo apt update`

2. Use the following command to install Python3:

`sudo apt install python3`

* pip3

Next, make sure you have `pip3` available. Chances are it is already installed, you can check this by running the following command at a shell prompt:

`pip3 --version`

and you should get an output text with the exact version of the pip3 package installed on your system. If not, you can install pip3 as follows:

1. Update the package list using the following command (not needed if you just did it in the previous step):

`sudo apt update`

2. Use the following command to install pip3:

`sudo apt install python3-pip`

* venv

Now, make sure you have the Python3 `venv` module available, you can check this by running the following command at a shell prompt:

`python3 -m venv`

and you should get an output text with a brief usage synopsis. If not, you can install the venv module as follows:

1. Update the package list using the following command (not needed if you just did it in one of the previous steps):

`sudo apt update`

2. Use the following command to install the venv module for Python3:

`sudo apt install python3-venv`

**Download the Ground Motion Processing repository**

You can create a local copy of the latest development version of groundmotion-processing by cloning the git repository or download the package from the project main web page and extract it to any directory you like.

If you choose to use git, you can check if it is already installed:

`git --version`

or install it the usual way:

`sudo apt install git`

From a terminal window, change to the local directory where you want to clone your repository and run:

`git clone https://gitlab.rm.ingv.it/esmpro/groundmotion-processing.git`

or

`git clone git@gitlab.rm.ingv.it:esmpro/groundmotion-processing.git`

If the clone was successful, a new sub-directory appears on your local drive in the directory where you cloned your repository. This directory has the same name as the GitLab repository that you cloned.

**Creating a Virtual Environment**

groundmotion-processing was developed using python libaries that are not included in the standard installation of Python 3.x such as [maplotlib](https://matplotlib.org/), [numpy](https://numpy.org/), [obspy](https://docs.obspy.org/), [scipy](https://scipy.org/), and [pyasdf](https://seismicdata.github.io/pyasdf/#).

To manage dependencies avoiding conflict due to different version installed on own Operating System you can create a "Python virtual environment" specifying the target directory (absolute or relative to the current directory) with the ground motion processing repository:

`python3 -m venv /path/to/new/virtual/environment`

For example:

`python3 -m venv /path/to/groundmotion-processing/`

To activate the Virtual Environment

`cd /path/to/groundmotion-processing/`

and run

`source bin/activate`

You should get a command line like this:

`(groundmotion-processing) user@MyComputer:/groundmotion-processing$`

At this point you are ready to install the groundmotion-processing requirements with `pip3`, and they will be "confined" only in the virtual environment:

`pip3 install --upgrade pip`

`pip3 install obspy` (this will also install numpy, maplotlib and scipy)

`pip3 install pyasdf`

You should be now ready to use the grounmotion-processing software!

When you're done processing waveforms, you can "exit" the virtual environment by deactivating it wit the following command:

`deactivate`

**Fortran subroutine**

Response Spectra are calculated by using a Fortran subroutine ([resp_spectra_f.f](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/blob/main/utils/resp_spectra_f.f)) for performance reasons.

The groundmotion-processing repository ships two [binary versions of the extension modules](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/tree/main/utils), compiled as a 64-bit Linux and macOS object and ready to use within ESMpro under GNU/Linux.

Source code for this Fortran routine is also provided; If you'd like to compile it yourself, you will need the `f2py` "Fortran to Python interface generator" utility (already included with the Python3 numpy library, which should already be installed) and a Fortran 95 compiler such as `gfortran`. As usual, you can install it with:

`sudo apt install gfortran`

To compile the Fortran routine and build the extension module:

`f2py -c -m resp_spectra_f64_py3 resp_spectra_f.f` 

please remember to rename the output .so file (it is created as resp_spectra_f64_py3.cpython-$YOUR_PYTHON3_VERSION-x86_64-linux-gnu.so by the compiler) to resp_spectra_f64_py3_unix or resp_spectra_f64_py3_mac after compilation is successful.



## Source Distribution

The User may also download the source code of a specific release of ESMpro groundmotion-processing following the same instruction of the development software for its installation:

[v.1.0.0-groundmotion-processing](https://gitlab.rm.ingv.it/esmpro/groundmotion-processing/-/releases/v1.0.0-beta "beta version of ESMpro groundmotion-processing software")