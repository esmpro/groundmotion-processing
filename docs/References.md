Akazawa, T. (2004). A technique for automatic detection of onset time of P-and S-Phases in strong motion records, 13th World Conference on Earthquake Engineering.

Ancheta, T.D., Darragh R.B., Stewart J.P., Seyhan E., Silva W.J., Chiou B.S.J., Wooddell K.E., Graves R.W., Kottke A.R., Boore D.M., and Kishida T. (2014). NGA-West2 database. Earthq Spectra 30 (3): 989–1005.

Arias, A. (1970). A measure of earthquake intensity, in Seismic Design of Nuclear Power Plants, R. Hansen (Editor), MIT press, Cambridge, Massachusetts.

D’Amico, M., C. Felicetta, E. Schiappapietra, F. Pacor, F. Galloviˇc, R. Paolucci, R. Puglia, G. Lanzano, S. Sgobba, and L. Luzi (2018). Fling Effects from Near-Source Strong-Motion Records: Insights from the 2016 Mw 6.5 Norcia, Central Italy, Earthquake.Seismological Research Letters ; 90 (2A): 659–671. doi: 10.1785/0220180169

Douglas, J and D.M. Boore (2011). High-frequency filtering of strong-motion records. Bull Earthq Eng 9 (2): 395–409.

Krischer L., Smith J., Lei W., Lefebvre M., Ruan Y., Sales de Andrade E., Podhorszki N., Bozdağ E., Tromp J., (2016). An Adaptable Seismic Data Format, Geophysical Journal International, Volume 207(2), 1003–1011, doi : 10.1093/gji/ggw319

Lanzano G., Sgobba S., Luzi L., Puglia R., Pacor F., Felicetta C., D’Amico M., Cotton F., Bindi D. (2019). The pan-European engineering strong motion (ESM) flatfile: Compilation criteria and data statistics, Bull. Earthq. Eng. 17, 561–582.

Mascandola C., D’Amico M., Russo E. Luzi L. (2022). ESMpro: an improved data management for the Engineering Strong Motion database (ESM), **doi: XXXX**

Pacor, F., Felicetta, C., Lanzano, G., Sgobba, S., Puglia, R., D'Amico, M., ... & Iervolino, I. (2018). NESS1: A worldwide collection of strong‐motion data to investigate near‐source effects. Seismological Research Letters, 89(6), 2299-2313, doi : 10.1785/0220180149

Paolucci, R., F. Pacor, R. Puglia, G. Ameri, C. Cauzzi, and M. Massa (2011). Record processing in ITACA, the new Italian strong motion database, in Earthquake Data in Engineering Seismology Predictive Models, Data Management and Networks, S. Akkar, P. Gülkan, and T. van Eck (Editors), Springer, Dordrecht, The Netherlands, 99–113, ISBN: 978-94- 007-0151-9 (printed version) 978-94-007-0152-6 (e-book version)

Puglia, R., E. Russo, L. Luzi, M. D’Amico, C. Felicetta, F. Pacor, and G. Lanzano (2018). Strong motion processing service: A tool to access and analyze earthquakes strong motion waveforms, Bull. Earthq. Eng. 16, 2641–2651

Schiappapietra, E., C. Felicetta, and M. D’Amico (2021). Fling-Step Recovering from Near-Source Waveforms Database. Geosciences, 11, 67., doi: 10.3390/geosciences11020067

Sgobba, S., Felicetta, C., Lanzano, G., Ramadan, F., D’Amico, M., & Pacor, F. (2021). NESS2. 0: An updated version of the worldwide dataset for calibrating and adjusting ground‐motion models in near source. Bulletin of the Seismological Society of America, 111(5), 2358-2378, doi : 10.1785/0120210080

Trnkoczy, A., P. Bormann, W. Hanka, L.G. Holcomb, and R.L., Nigbor (2012). Site selection, preparation and installation of seismic stations. In New Manual of Seismological Observatory Practice 2 (NMSOP-2) (pp. 1-139). Deutsches GeoForschungsZentrum GFZ.