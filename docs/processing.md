This code processes the input data with three alternatives: PAO11 [(Paolucci et al., 2011)](https://link.springer.com/chapter/10.1007/978-94-007-0152-6_8), eBASCO [(Schiappapietra et al., 2021)](https://www.mdpi.com/2076-3263/11/2/67) or CUSTOM. This is performed based on the configuration file provided by preprocessing.py. In output, it provides the ASDF volume with the processed data and a JSON with the exit message (Figure 1).

![](/images/processing.jpg)
<img src="images/processing.jpg"  width="650" height="200">


**Figure 1**


# ``PAO11``

PAO11 (Paolucci et al., 2011) is the default scheme and it is always applied to ensure full compatibility between acceleration and velocity or displacement, obtained by single and double integration of processed accelerations, respectively (Puglia et al., 2018). 
The workflow of PAO11 is summarized below (Figure 2):

- Linear detrending of the uncorrected acceleration signal (subtraction of a first order polynomial);
- Application of a cosine taper, with percentage fixed to 5% of the signal length, with the possibility of being modified by the user; at this stage records identified as late triggered are not tapered;
- Application of a 2nd order acausal time-domain Butterworth filter to the acceleration time-series; zero-pads are added at the beginning and at the end of the signal before the acausal filter is applied;
- Removal of zero pads from the acceleration trace of the normal triggered records; zero pads are kept for late triggered records. The taper of analog late-triggered records is fixed to 5% of the signal length, although it can be modified;
- (start/end) taper of the acceleration signal, with percentage fixed to 5% (same threshold as digital and analog normal-triggered);
- Computation of the velocity signal and linear detrending;
- (start/end) taper of the velocity signal, with percentage fixed to 5% (same threshold as digital and analog normal-triggered);
- Computation of displacement signal and linear detrending;
- (start/end) taper of the displacement signal, with percentage fixed to 5% (same threshold as digital and analog normal-triggered);
- Recursive differentiation to obtain the velocity and the acceleration signal, respectively.

![](/images/pao11.jpg)
<details><summary>

**Click for Figure 2**

</summary>
<img src="images/pao11.jpg"  width="600" height="1200">

**Figure 2**: Data processing flowchart of PAO11.
</details>

# ``eBASCO``

eBASCO (extended BASeline COrrection; Schiappapietra et al., 2021), is specifically tailored to process near-source data affected by fling-step featured by one-side pulse in the velocity trace, and an offset at the end of the displacement (D’Amico et al., 2018). Differently from PAO11, eBASCO is applied only to near-source records (Pacor et al., 2018; Sgobba et al., 2021). In the current version, eBASCO adjusts the long-period distortion of the signal (due to instrumental effects, ground rotation or tilting) by means of a trilinear detrending of the velocity time series. 
The eBASCO procedure is very sensitive to the preliminary cut of the accelerometric waveforms needed to isolate the uncorrected earthquake records; a bad cutting may cause unwanted distortion of the signal. 

The workflow of eBASCO is summarized below (Figure 3):
- trimming of the uncorrected acceleration
- sampling of time correction points to define a set of piecewise linear trends to correct the velocity trace;
- piecewise linear detrending of the velocity trace in three time-windows: 1) pre-event window between the time of the first sample T<sub>0</sub> and the time T<sub>1</sub> from which the ground moves toward the final displacement; 2) transient window containing the strong phase of the motion between T<sub>1</sub> and the time T<sub>2</sub> in which the ground has already reached the permanent displacement; 3) post-event window from T<sub>2</sub> and the end of the signal (T<sub>end</sub>);
- calculation of the flatness indicator (f-value) of the displacement trace for each set of correction points;
- selection of the displacement with maximum f-value (best solution).

![](/images/ebasco.jpg)
<details><summary>

**Click for Figure 3**

</summary>
<img src="images/ebasco.jpg"  width="500" height="900">

**Figure 3**: Flowchart of the eBASCO procedure, modified after D’Amico et al. (2019).
</details>
